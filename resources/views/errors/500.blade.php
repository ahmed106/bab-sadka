<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
    @import url('https://fonts.googleapis.com/css2?family=Noto+Kufi+Arabic&display=swap');

    :root {
        --button: #b3b3b3;
        --button-color: #0a0a0a;
        --shadow: #000;
        --bg: #737373;
        --header: #7a7a7a;
        --color: #fafafa;
        --lit-header: #e6e6e6;
        --speed: 2s;
    }

    * {
        box-sizing: border-box;
    }

    @property --swing-x {
        initial-value: 0;
        inherits: false;
        syntax: '<integer>';
    }

    @property --swing-y {
        initial-value: 0;
        inherits: false;
        syntax: '<integer>';
    }

    body {
        min-height: 100vh;
        display: flex;
        font-family: 'Noto Kufi Arabic', sans-serif;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        background: var(--bg);
        color: var(--color);
        perspective: 1200px;
        margin: 0;
        padding: 15px;
    }

    a {
        text-transform: uppercase;
        text-decoration: none;
        background: #b26834;
        color: #ffffff;
        padding: 13px 40px;
        border-radius: 4rem;
        font-size: 20px;
        display: inline-block;
    }

    p {
        font-weight: 100;
    }

    h1 {
        -webkit-animation: swing var(--speed) infinite alternate ease-in-out;
        animation: swing var(--speed) infinite alternate ease-in-out;
        font-size: clamp(5rem, 40vmin, 20rem);
        font-family: 'Open Sans', sans-serif;
        margin: 0;
        margin-bottom: 0;
        letter-spacing: 1rem;
        color: #2c685a;
        position: relative;
        z-index: 9;
        margin-top: -2rem;
    }

    h1:after {
        -webkit-animation: swing var(--speed) infinite alternate ease-in-out;
        animation: swing var(--speed) infinite alternate ease-in-out;
        content: "404";
        position: absolute;
        top: 0;
        left: 0;
        color: var(--shadow);
        filter: blur(1.5vmin);
        transform: scale(1.05) translate3d(0, 12%, -10vmin) translate(calc((var(--swing-x, 0) * 0.05) * 1%), calc((var(--swing-y) * 0.05) * 1%));
        z-index: -1;
    }

    .cloak {
        animation: swing var(--speed) infinite alternate-reverse ease-in-out;
        height: 100%;
        width: 100%;
        transform-origin: 50% 30%;
        transform: rotate(calc(var(--swing-x) * -0.25deg));
        background: radial-gradient(40% 40% at 50% 42%, transparent, #000 35%);
    }

    .cloak__wrapper {
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        overflow: hidden;
    }

    .cloak__container {
        height: 250vmax;
        width: 250vmax;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .info {
        text-align: center;
        position: relative;
    }

    .info h2 {
        font-weight: normal;
        line-height: 32px;
        margin-top: 0;
        margin-bottom: 40px;
    }

    @media (max-width: 450px) {
        a {
            padding: 5px 30px;
            font-size: 16px;
        }

        .info h2 {
            line-height: 30px;
            margin-bottom: 25px;
            font-size: 20px;
        }

        h1 {
            margin-bottom: 10px;
        }
    }


    @-webkit-keyframes swing {
        0% {
            --swing-x: -100;
            --swing-y: -100;
        }

        50% {
            --swing-y: 0;
        }

        100% {
            --swing-y: -100;
            --swing-x: 100;
        }
    }

    @keyframes swing {
        0% {
            --swing-x: -100;
            --swing-y: -100;
        }

        50% {
            --swing-y: 0;
        }

        100% {
            --swing-y: -100;
            --swing-x: 100;
        }
    }

</style>


<body>


<h1>404</h1>
<div class="cloak__wrapper">
    <div class="cloak__container">
        <div class="cloak"></div>
    </div>
</div>
<div class="info">
    <h2>نأسف لك هذه الصفحة غير موجودة</h2>
    <a href="{{url()->previous()}}">رجوع</a>
</div>

</body>

</html>
