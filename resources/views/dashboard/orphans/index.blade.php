@extends('dashboard.layouts.master')
@push('style')
    <style>

    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">الأيتام</h4>
                    </div>
                    @if(auth('admin')->user()->hasPermission('create_orphans'))
                        <a href="{{route('orphans.create')}}" class="btn btn-secondary add-list"><i class="las la-plus"></i>إضافه يتيم</a>
                    @endif
                </div>
                <div class="card-body">

                    @if(auth('admin')->user()->hasPermission('delete_orphans'))
                        <button type="submit" class="btn  btn-danger mb-3" id="deleteAllBtn"><i class="las la-trash"></i> حذف المحدد</button>
                    @endif
                    <div class="table-responsive">
                        <form id="deleteAllForm" action="{{route('orphans.bulkDelete')}}" method="post">
                            @csrf

                            <table id="datatable" class="table data-table table-bordered table-striped">
                                <thead>
                                <tr class="ligth">
                                    <th>
                                        <div class="checkbox d-inline-block">
                                            <input type="checkbox" class="checkbox-input" id="check_all">
                                            <label for="checkbox1" class="mb-0"></label>
                                        </div>
                                    </th>

                                    <th>الاسم</th>
                                    <th>معتمد</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('javascript')


    <script>
        $('#datatable').DataTable({

            ajax: {
                url: "{{route('orphans.data')}}"
            },
            columns: [
                {data: 'check_all', name: 'check_all'},
                {data: 'name', name: 'name'},
                {data: 'approval', name: 'approval'},

                {data: 'actions', name: 'actions'},
            ]
        })

        $(document).on('click', '.active_btn', function () {
            let id = $(this).data('id'),
                active = $(this).data('active');

            $.ajax({
                type: 'post',
                url: '{{route('orphans.approval')}}',
                data: {
                    '_token': '{{csrf_token()}}',
                    'id': id,
                    'active': active
                },
                success: function (response) {
                    Toast.fire({
                        title: response.data,
                        icon: 'success',
                    })
                }
            })

        })


    </script>
@endpush
