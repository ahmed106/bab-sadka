@extends('dashboard.layouts.master')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">اضافة يتيم</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="step-app" id="demo-steps">
                        <ul class="step-steps">
                            <li data-step-target="step1"><span>1</span> بيانات اليتيم</li>
                            <li data-step-target="step2"><span>2</span> بيانات أهل اليتيم</li>
                        </ul>
                        <form id="form" action="{{route('orphans.update',$orphan->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="step-content checkout-form">

                                <input type="hidden" value="{{$orphan->id}}" name="id">

                                <div class="step-tab-panel" data-step="step1">

                                    <div class="flex-divs">
                                        <div class="input-block">
                                            <label>الإسم رباعي</label>
                                            <input value="{{$orphan->name}}" name="name" type="text" placeholder="">
                                        </div>
                                        <div class="input-block">
                                            <label>رقم الهوية</label>
                                            <input name="id_number" value="{{$orphan->id_number}}" type="text" placeholder="">
                                        </div>
                                        <div class="input-select">
                                            <div class="select-div">
                                                <label>النوع</label>
                                                <select name="gender">
                                                    <option {{$orphan->gender == 'male' ?'selected':''}} value="male"> ذكر</option>
                                                    <option {{$orphan->gender == 'female' ?'selected':''}} value="female"> انثى</option>
                                                </select>
                                            </div>
                                            <div class="input-block">
                                                <label>تاريخ الميلاد</label>
                                                <input value="{{$orphan->birthdate}}" name="birthdate" type="date" placeholder="">
                                            </div>
                                        </div>
                                        <div class="select-div">
                                            <label>الدولة</label>

                                            <select class="select-2" name="country_id">

                                                <option value="" selected disabled>إختر دوله</option>
                                                @foreach($countries as $country)

                                                    <option {{$orphan->country_id == $country->id ? 'selected':''}} value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="select-div">
                                            <label>المحافظة</label>
                                            <select class="select-2" name="city_id">
                                                <option value="" selected disabled>إختر محافظه</option>
                                            </select>
                                        </div>
                                        <div class="input-block">
                                            <label>العنوان</label>

                                            <input value="{{$orphan->address}} " name="address" type="text" placeholder="">
                                        </div>
                                        <div class="input-select">
                                            <div class="select-div">
                                                <label>اليتيم فقد</label>
                                                <select name="lost">
                                                    <option {{$orphan->lost =='father' ?'selected':''}} value="father"> الاب</option>
                                                    <option {{$orphan->lost =='mother' ?'selected':''}} value="mother">الام</option>
                                                    <option {{$orphan->lost =='both' ?'selected':''}} value="both"> كلاهما</option>
                                                </select>
                                            </div>
                                            <div class="input-block">
                                                <label>مع من يعيش اليتيم</label>
                                                <input value="{{$orphan->live_with}}" name="live_with" type="text" placeholder="">
                                            </div>
                                        </div>
                                        <div class="input-select">
                                            <div class="select-div">
                                                <label>كود الدولة</label>
                                                <select name="country_code" class="code">
                                                    <option {{$orphan->country_code == '+966' ? 'selected':''}} value="+966">+966</option>
                                                    <option {{$orphan->country_code == '+20' ? 'selected':''}} value="+20">+20</option>
                                                </select>
                                            </div>
                                            <div class="input-block">
                                                <label>رقم الجوال</label>
                                                <input name="phone" value="{{$orphan->phone}}" type="number" placeholder="">
                                            </div>
                                        </div>
                                        <div class="input-block">
                                            <label>صاحب الرقم</label>
                                            <input name="phone_owner" value="{{$orphan->phone_owner}}" type="text" placeholder="">
                                        </div>
                                    </div>
                                    <div class="btm-part">
                                        <h3 class="title">الحالة الصحية لليتيم</h3>
                                        <p class="text">هل اليتيم يعاني من اي امراض ؟</p>
                                        <div class="flex-radio">
                                            <label for="yes">
                                                <input {{$orphan->has_disease ? 'checked':''}} type="radio" name="has_disease" value="1" id="yes">
                                                <strong>نعم</strong>
                                            </label>
                                            <label for="no">
                                                <input {{!$orphan->has_disease ? 'checked':''}} type="radio" name="has_disease" value="0" id="no">
                                                <strong>لا</strong>
                                            </label>
                                        </div>
                                        <div class="flex-divs disease_details {{$orphan->has_disease ?'':'d-none' }}">
                                            <div class="input-block">
                                                <label>نوع المرض</label>
                                                <input value="{{$orphan->disease_type}}" name="disease_type" type="text" placeholder="">
                                            </div>
                                            <div class="input-block">
                                                <label>تكلفة العلاج شهريا</label>
                                                <input value="{{$orphan->disease_cost}}" name="disease_cost" type="number" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="input-file">
                                        <label>اضافة صورة هوية اليتيم</label>
                                        <input class="_image" name="photo" type="file" placeholder="">
                                        <div class="mt-2 flex-divs">
                                            @if($orphan->photo)
                                                <img class="image_preview" src="{{asset('images/orphans/'.$orphan->photo)}}" alt="">
                                            @else
                                                <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="step-tab-panel" data-step="step2">

                                    <h3 class="tilte-head">بيانات والد اليتيم</h3>


                                    <div class="flex-divs">
                                        <div class="input-block">
                                            <label>الإسم رباعي</label>
                                            <input value="{{$orphan->father_name}}" name="father_name" type="text" placeholder="">
                                        </div>
                                        <div class="input-block">
                                            <label>رقم الهوية</label>
                                            <input name="father_id" value="{{$orphan->father_id}}" type="text" placeholder="">
                                        </div>


                                        <div class="input-block father_death {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'':'d-none'}}">
                                            <label>تاريخ الوفاة</label>
                                            <input value="{{$orphan->father_death_date}}" name="father_death_date" type="date" placeholder="">
                                        </div>
                                        <div class="input-block father_death {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'':'d-none'}}">
                                            <label>سبب الوفاة</label>
                                            <input value="{{$orphan->father_death_reason}}" name="father_death_reason" type="text" placeholder="">
                                        </div>
                                        <div class="input-block father_death {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'':'d-none'}}">
                                            <label>مكان الوفاة</label>
                                            <input value="{{$orphan->father_death_place}}" name="father_death_place" type="text" placeholder="">
                                        </div>


                                        <div class="input-block father_details   {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'d-none':''}}">
                                            <label>المستوي التعليمي</label>
                                            <input value="{{$orphan->father_education_level}}" name="father_education_level" type="text" placeholder="">
                                        </div>
                                        <div class="input-block father_details {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'d-none':''}}">
                                            <label>المهنة</label>
                                            <input value="{{$orphan->father_job}}" name="father_job" type="text" placeholder="">
                                        </div>
                                        <div class="input-block father_details {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'d-none':''}}">
                                            <label>الراتب</label>
                                            <input name="father_salary" value="{{$orphan->father_salary}}" placeholder="">
                                        </div>


                                    </div>

                                    <div class="input-file father_death_application {{$orphan->lost == 'father' ||$orphan->lost == 'both' ?'':'d-none'}} ">
                                        <label>اضافة صورة شهادة الوفاة</label>
                                        <input name="father_death_application" type="file" placeholder="">
                                        @if($orphan->father_death_application)
                                            <img class="image_preview" src="{{asset('images/orphans/'.$orphan->father_death_application)}}" alt="">
                                        @else
                                            <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                        @endif
                                    </div>
                                    <div class="input-file father_photo {{$orphan->lost == 'father' ||$orphan->lost=='both' ?'d-none':''}} ">
                                        <label>اضافة صورة الهوية</label>
                                        <input name="father_photo" type="file" placeholder="">
                                        @if($orphan->father_photo)
                                            <img class="image_preview" src="{{asset('images/orphans/'.$orphan->father_photo)}}" alt="">
                                        @else
                                            <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                        @endif
                                    </div>

                                    <hr>

                                    <h3 class="tilte-head">بيانات والدة اليتيم</h3>
                                    <div class="flex-divs">
                                        <div class="input-block">
                                            <label>الإسم رباعي</label>
                                            <input value="{{$orphan->mother_name}}" name="mother_name" type="text" placeholder="">
                                        </div>
                                        <div class="input-block">
                                            <label>رقم الهوية</label>
                                            <input value="{{$orphan->mother_id}}" name="mother_id" type="text" placeholder="">
                                        </div>
                                        <div class="input-block mother_death {{$orphan->lost == 'mother' || $orphan->lost=='both'?'':'d-none'}}">
                                            <label>تاريخ الوفاه</label>
                                            <input value="{{$orphan->mother_death_date}}" name="mother_death_date" type="date" placeholder="">
                                        </div>


                                        <div class="input-block mother_death {{$orphan->lost == 'mother' || $orphan->lost=='both'?'':'d-none'}}">
                                            <label>سبب الوفاة</label>
                                            <input value="{{$orphan->mother_death_reason}}" name="mother_death_reason" type="text" placeholder="">
                                        </div>
                                        <div class="input-block mother_death {{$orphan->lost == 'mother' || $orphan->lost =='both'?'':'d-none'}}">
                                            <label>مكان الوفاة</label>
                                            <input value="{{$orphan->mother_death_place}}" name="mother_death_place" type="text" placeholder="">
                                        </div>
                                        <div class="input-block mother_details {{$orphan->lost == 'mother' || $orphan->lost=='both'?'d-none':''}} ">
                                            <label>المستوي التعليمي</label>
                                            <input value="{{$orphan->mother_education_level}}" name="mother_education_level" type="text" placeholder="">
                                        </div>
                                        <div class="input-block mother_details {{$orphan->lost == 'mother' || $orphan->lost=='both'?'d-none':''}}  ">
                                            <label>المهنة</label>
                                            <input value="{{$orphan->mother_job}}" name="mother_job" type="text" placeholder="">
                                        </div>
                                        <div class="input-block mother_details {{$orphan->lost == 'mother' || $orphan->lost=='both'?'d-none':''}}">
                                            <label>الراتب</label>
                                            <input value="{{$orphan->mother_salary}}" name="mother_salary" placeholder="">
                                        </div>
                                    </div>
                                    <div class="input-file mother_death_application {{$orphan->lost == 'mother' || $orphan->lost == 'both' ?'':'d-none'}}">
                                        <label>اضافة صورة شهادة الوفاة</label>
                                        <input name="mother_death_application" type="file" placeholder="">
                                        @if($orphan->mother_death_application)
                                            <img class="image_preview" src="{{asset('images/orphans/'.$orphan->mother_death_application)}}" alt="">
                                        @else
                                            <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                        @endif
                                    </div>
                                    <div class="input-file mother_photo {{$orphan->lost == 'mother' || $orphan->lost=='both'?'d-none':''}}">
                                        <label>اضافة صورة الهوية</label>
                                        <input name="mother_photo" type="file" placeholder="">
                                        @if($orphan->mother_photo)
                                            <img class="image_preview" src="{{asset('images/orphans/'.$orphan->mother_photo)}}" alt="">
                                        @else
                                            <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                        @endif
                                    </div>

                                </div>
                            </div>

                        </form>
                        <div class="step-footer">
                            <button data-step-action="prev" class="button btn-prev">السابق</button>
                            <button data-step-action="next" class="button btn-next">التالي</button>
                            <button data-step-action="finish" class="button btn-add">ارسال</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('javascript')
    <script>
        $(document).ready(function () {
            $('#demo-steps').steps({
                onFinish: function () {
                    $('#form').submit();
                }
            });
            let country_id = $('select[name="country_id"]').val();
            $.ajax({
                type: "get",
                url: "{{route('getCountryCities')}}",
                data: {
                    country_id: country_id,
                },
                success: function (res) {
                    res.data.map((res) => {
                        let active = res.id == '{{$orphan->city_id}}' ? 'selected' : '';
                        $('select[name="city_id"]').append(`<option ${active} value="${res.id}">${res.name}</option>`)
                    })
                }
            });
        });
        $('input[name="has_disease"]').on('change', function () {
            $(this).val() == 1 ? $('.disease_details').removeClass('d-none') : $('.disease_details').addClass('d-none')
        });

        $('select[name="lost"]').on('change', function () {

            if ($(this).val() == 'father') {
                $('.mother_death,.father_details,.father_photo,.mother_death_application').addClass('d-none');
                $('.father_death,.mother_details,.father_death_application,.mother_photo').removeClass('d-none');

            } else if ($(this).val() == 'mother') {
                $('.father_death,.mother_details,.mother_photo,.father_death_application').addClass('d-none');
                $('.mother_death,.father_details,.mother_death_application,.father_photo').removeClass('d-none');

            } else {
                $('.father_death ,.mother_death,.father_death_application,.mother_death_application').removeClass('d-none');
                $('.father_details ,.mother_details,.mother_photo,.father_photo').addClass('d-none');
            }

        });


        $('select[name="country_id"]').on('change', function () {
            let country_id = $(this).val();
            $('select[name="city_id"]').empty();
            $.ajax({
                type: "get",
                url: '{{route('getCountryCities')}}',
                data: {
                    country_id: country_id
                },
                success: function (res) {
                    res.data.map((res) => {
                        $('select[name="city_id"]').append(`<option value="${res.id}">${res.name}</option>`)
                    })
                }
            })
        });

    </script>
@endpush
