<div class="d-flex align-items-center list-action">
    @if(auth('admin')->user()->hasPermission('update_roles'))
        <a class="btn btn-success" href="{{route('roles.edit',encrypt($row->id))}}"><i class="ri-pencil-line mr-0"></i> تعديل</a>

    @endif
    @if(auth('admin')->user()->hasPermission('delete_roles'))
        <form method="post" action="{{route('roles.destroy',$row->id)}}">
            @csrf
            @method('delete')


            <button class="btn btn-danger delete_btn " type="submit">حذف</button>


        </form>
    @endif

</div>

<script>
    $('.delete_btn').on('click', function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'هل أنت  متأكد',
            text: "يمكنك التراجع عن هذا الأمر!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'نعم, حذف!',
            cancelButtonText: 'ألغاء'
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).parent('form').submit();
            }
        })

    })
</script>
