@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div class="header-title">
                    <h4 class="card-title">اضافة صلاحيات</h4>
                </div>
                <a href="{{route('roles.index')}}" class="btn btn-outline-secondary add-list">
                    رجوع
                    <i class="las la-long-arrow-alt-left"></i></a>
            </div>

            @if($errors->any())
            <div class="alert alert-danger">

                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach


                </ul>
            </div>

            @endif


            <div class="card-body">

                <form class="form " action="{{route('roles.store')}}" method="post">
                    @csrf
                    <div class="flex-divs mb-3">
                        <div class="input-block">
                            <label>اسم الصلاحية</label>
                            <input type="text" required name="name">
                        </div>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="datatable" class="table data-table table-bordered table-striped">
                            <thead>
                                <tr class="ligth">
                                    <th>#</th>
                                    <th>الاسم</th>
                                    <th>الصلاحيات</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach(Models() as $index => $model)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>{{ucfirst($model)}}</td>
                                    <td>
                                        <div class="list-action-roles list-perm">
                                            <div class="custom-control custom-checkbox custom-control-inline check-all">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="customCheck-{{$model}}">
                                                <label class="custom-control-label" for="customCheck-{{$model}}">تحديد
                                                    الكل</label>
                                            </div>
                                            @foreach(maps() as $index_map => $map)
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input name="permissions[]" value="{{$map.'_'.$model}}" type="checkbox"
                                                    class="custom-control-input checkbox-{{$index}}"
                                                    id="customCheck-{{$map.'_'.$model}}">
                                                <label class="custom-control-label"
                                                    for="customCheck-{{$map.'_'.$model}}">{{$map}}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </form>
            </div>


        </div>
    </div>
</div>
@endsection

@push('javascript')
<script>
    @foreach(Models() as $index => $model)

    $('input[id="customCheck-{{$model}}"]').on('change', function () {
        let checkbox = $(this).parent().parent().find($('input[type="checkbox"]'));
        if ($(this).is(':checked')) {
            checkbox.prop('checked', true)
        } else {
            checkbox.prop('checked', false)
        }
    })

    @endforeach

</script>

@endpush
