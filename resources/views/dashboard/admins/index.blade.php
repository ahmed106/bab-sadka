@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">المشرفين</h4>
                    </div>
                    @if(auth('admin')->user()->hasPermission('create_admins'))
                        <a href="{{route('admins.create')}}" class="btn btn-secondary add-list"><i class="las la-plus"></i>اضافة صلاحية</a>
                    @endif
                </div>
                <div class="card-body">

                    @if(auth('admin')->user()->hasPermission('delete_admins'))
                        <button type="submit" class="btn disabled btn-danger mb-3" id="deleteAllBtn"><i class="las la-trash"></i> حذف المحدد</button>
                    @endif
                    <div class="table-responsive">
                        <form id="deleteAllForm" action="{{route('admins.bulkDelete')}}" method="post">
                            @csrf

                            <table id="datatable" class="table data-table table-bordered table-striped">
                                <thead>
                                <tr class="ligth">
                                    <th>
                                        <div class="checkbox d-inline-block">
                                            <input type="checkbox" class="checkbox-input" id="check_all">
                                            <label for="checkbox1" class="mb-0"></label>
                                        </div>
                                    </th>

                                    <th>الاسم</th>
                                    <th>البريد الالكتروني</th>
                                    <th>الصوره</th>
                                    <th>العمليات</th>


                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('dashboard.includes.swa')
    <script>

        $('#datatable').DataTable({
            "lengthMenu": [[20, 50, -1], [20, 50, "All"]],
            "processing": true,
            "serverSide": true,
            responsive: true,
            "ajax": {
                "url": "{{route('admins.data')}}",
                "type": "GET",

            },
            "columns": [
                {data: 'check_all', name: 'check_all', sortable: false},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'photo', name: 'photo'},

                {data: 'actions', name: 'actions'},


            ]

        })
    </script>

    <script>
        $('#check_all').on('click', function () {

            $.each($('.checkedBtn[type="checkbox"]'), function () {

                if ($('#check_all[type="checkbox"]:checked').length === 0) {
                    $('#deleteAllBtn').addClass('disabled');
                    $(this).prop('checked', false)

                } else {
                    $('#deleteAllBtn').removeClass('disabled');
                    $(this).prop('checked', true)
                }


            })
        });

        $(document).on('click', '.checkedBtn', function () {
            if ($('.checkedBtn[type="checkbox"]:checked').length > 0) {
                $('#deleteAllBtn').removeClass('disabled');
            } else {
                $('#deleteAllBtn').addClass('disabled');
            }
        })


        $('#deleteAllBtn').on('click', function () {
            if ($('.checkedBtn[type="checkbox"]:checked').length > 0) {
                Swal.fire({
                    title: 'هل أنت  متأكد',
                    text: "يمكنك التراجع عن هذا الأمر!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'نعم, حذف!',
                    cancelButtonText: 'ألغاء'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#deleteAllForm').submit();
                    }
                })
            } else {
                Swal.fire({
                    title: 'من فضلك  قم بإختيار بعض العتاصر!',
                    icon: 'warning',
                })
            }

        })
    </script>

@endpush
