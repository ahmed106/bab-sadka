@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">اضافة مشرفين</h4>
                    </div>
                    <a href="{{route('admins.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>

                <form method="post" action="{{route('admins.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="checkout-form checkout-form-custom">
                            <div class="flex-divs">
                                <div class="input-block">
                                    <label>الاسم</label>
                                    <input type="text" name="name" required>
                                </div>
                                <div class="input-block">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" name="email" required>
                                </div>
                                <div class="input-block">
                                    <label>كلمة المرور</label>
                                    <input type="password" name="password" required>
                                </div>
                                <div class="input-block">
                                    <label>إعاده كلمة المرور</label>
                                    <input type="password" name="password_confirmation" required>
                                </div>
                                <div class="select-div">
                                    <label>الصلاحيات</label>
                                    <select class="select-2" name="role" data-placeholder="اختر">
                                        @foreach($roles as $role)
                                            <option value="{{$role->id}}">{{$role->display_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الصورة</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" class="_image" name="photo"/>
                                        <div class="mt-2 flex-divs">
                                            <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('javascript')

@endpush
