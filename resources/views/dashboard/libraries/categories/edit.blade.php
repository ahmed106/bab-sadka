@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
<div class="row">
    <div class="col-sm-12">
        <form action="{{route('library-categories.update',$category->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')

            <input type="hidden" value="{{$category->id}}" name="id">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">تعديل قسم</h4>
                    </div>
                    <a href="{{route('library-categories.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                        @foreach(activeLanguages() as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                aria-selected="true">{{$lang->name}}</a>
                        </li>
                        @endforeach


                    </ul>
                    <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                        @foreach(activeLanguages() as $lang)
                        <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                            role="tabpanel" aria-labelledby="one-tab">
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label>@lang('site.name') ( {{$lang->name}} )</label>
                                    <input value="{{$category->translate($lang->locale)['name']}}" type="text"
                                        name="{{$lang->locale}}[name]">

                                    @error($lang->locale.'.name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            @include('dashboard.includes.seo_inputs',['lang'=>$lang,'model'=>$category])
                        </div>

                        @endforeach

                        <div class="btm-part">
                            <h4 class="title mb-3">الصورة</h4>
                            <div class="flex-divs">
                                <div class="input-block">
                                    <input type="file" id="image" class="" name="photo" />
                                </div>
                            </div>
                            <div class="mt-2 flex-divs">
                                @if($category->photo !='')
                                <img class="image_preview"
                                    src="{{asset('images/library_categories/'.$category->photo)}}" alt="">

                                @else

                                <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                @endif
                            </div>
                        </div>


                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('javascript')

@endpush
