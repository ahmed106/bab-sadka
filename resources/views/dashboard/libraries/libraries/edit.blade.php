@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
<div class="row">
    <div class="col-sm-12">
        <form action="{{route('libraries.update',$library->id)}}" method="post" enctype="multipart/form-data">
            @csrf

            @method('put')

            <input type="hidden" name="id" value="{{$library->id}}">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">تعديل </h4>
                    </div>
                    <a href="{{route('libraries.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>
                <div class="card-body">

                    <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                        @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                aria-selected="true">{{$lang->name}}</a>
                        </li>
                        @endforeach


                    </ul>
                    <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                        @foreach($languages as $lang)
                        <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                            role="tabpanel" aria-labelledby="one-tab">
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label>@lang('site.name') ( {{$lang->name}} )</label>
                                    <input value="{{$library->translate($lang->locale)['name']}}" type="text"
                                        name="{{$lang->locale}}[name]">

                                    @error($lang->locale.'.name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            @include('dashboard.includes.seo_inputs',['lang'=>$lang,'model'=>$library])

                        </div>

                        @endforeach

                        <div class="flex-divs">
                            <div class="select-div">
                                <label for="">القسم</label>
                                <select class="select-2" name="category_id" id="">
                                    <option value="" disabled selected>إختر قسماً</option>
                                    @foreach($categories as $cat)
                                    <option {{$library->category_id == $cat->id ?'selected':''}} value="{{$cat->id}}">
                                        {{$cat->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="btm-part">
                            <h4 class="title mb-3">ملف pdf</h4>
                            <div class="flex-divs">
                                <div class="input-block">
                                    <input type="file" accept="application/pdf" id="pdf" class="" name="pdf" />
                                    @if($library->pdf)
                                    {{-- <a class="btn btn-secondary" target="_blank" href="{{asset('uploads/libraries/pdf/'.$library->pdf)}}"><i
                                        class="fa fa-eye"></i> عرض</a>--}}
                                    <a class="btn btn-secondary" id="preview_pdf" target="_blank"
                                        href="{{route('libraries.viewPdf',$library->id)}}"><i class="fa fa-eye"></i> عرض
                                    </a>
                                    @else
                                    <a class="btn btn-secondary d-none" id="preview_pdf" target="_blank"
                                        href=""><i class="fa fa-eye"></i> عرض </a>

                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="btm-part">
                            <h4 class="title mb-3">الصورة</h4>
                            <div class="flex-divs">
                                <div class="input-block">
                                    <input accept="image/*" type="file" id="image" class="" name="photo" />
                                </div>
                            </div>
                            <div class="mt-2 flex-divs">
                                @if($library->photo)

                                <img class="image_preview" src="{{asset('images/libraries/'.$library->photo)}}" alt="">
                                @else
                                <img class="image_preview" src="{{asset('default.png')}}" alt="">
                                @endif
                            </div>
                        </div>

                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('javascript')
{{--    --}}
@endpush
