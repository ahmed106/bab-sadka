<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$library->name}}-pdf</title>

    <link rel="shortcut icon" href="{{asset('assets/front')}}/assets/images/logo/logo.svg">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/front')}}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/front')}}/assets/css/plugins.css">
    <link href="{{asset('assets/front')}}/assets/js/flip-book/css/flipbook.style.css" rel="stylesheet" type="text/css">

</head>
<body>

<div class="content">

    <div class="solid-container"></div>
</div>
<script src="{{asset('assets/front')}}/assets/js/jquery-3.6.0.min.js"></script>
<script src="{{asset('assets/front')}}/assets/js/jquery-migrate-3.3.2.min.js"></script>
<script src="{{asset('assets/front')}}/assets/js/modernizr-3.11.2.min.js"></script>
<script src="{{asset('assets/front')}}/assets/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('assets/front')}}/assets/js/plugins.js"></script>
<script src="{{asset('assets/front')}}/assets/js/active.js"></script>
<script src="{{asset('assets/front')}}/assets/js/scripts.js"></script>

<script src="{{asset('assets/front')}}/assets/js/flip-book/js/flipbook.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".solid-container").flipBook({
            pdfUrl: "{{asset('uploads/libraries/pdf/'.$library->pdf)}}",
            rightToLeft: true,
            btnSize: 18,
            backgroundColor: "#0a0c06",
            locale: 'en',
        });

    })
</script>
</body>
</html>
