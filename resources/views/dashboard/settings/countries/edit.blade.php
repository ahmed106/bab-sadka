@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
<div class="row">
    <div class="col-sm-12">
        <form action="{{route('countries.update',$country->id)}}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')

            <input type="hidden" name="id" value="{{$country->id}}">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">تعديل دوله</h4>
                    </div>
                    <a href="{{route('prays.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                        @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                aria-selected="true">{{$lang->name}}</a>
                        </li>
                        @endforeach


                    </ul>
                    <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                        @foreach($languages as $lang)
                        <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                            role="tabpanel" aria-labelledby="one-tab">
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label>الاسم ( {{$lang->name}} )</label>
                                    <input value="{{$country->translate($lang->locale)['name']}}"
                                        name="{{$lang->locale}}[name]">
                                    @error($lang->locale.'.name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('javascript')

@endpush
