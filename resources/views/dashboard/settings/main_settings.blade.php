@extends('dashboard.layouts.master')
@section('content')

<div class="row">
    <div class="col-sm-12">
        <form action="{{route('settings.store')}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">الإعدادات</h4>
                    </div>
                    <a href="{{route('settings.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                        @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                aria-selected="true">{{$lang->name}}</a>
                        </li>
                        @endforeach


                    </ul>
                    <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                        @foreach($languages as $lang)
                        <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                            role="tabpanel" aria-labelledby="one-tab">
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label for="">اسم الموقع</label>
                                    <input type="text"
                                        value="{{isset($setting) ? $setting->translate($lang->locale)['website_name']:''}}"
                                        name="{{$lang->locale}}[website_name]">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>وصف الموقع ( {{$lang->name}} )</label>
                                    <textarea class="summernote" name="{{$lang->locale}}[website_description]" id=""
                                        cols="30"
                                        rows="10">{{isset($setting) ? $setting->translate($lang->locale)['website_description']:''}}</textarea>
                                    @error($lang->locale.'.pray')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-12">
                                    <label>وصف عنوان الميتا ( {{$lang->name}} )</label>

                                    <textarea class="summernote" name="{{$lang->locale}}[meta_title]" id="" cols="30"
                                        rows="10">{{isset($setting) ? $setting->translate($lang->locale)['meta_title']:''}}</textarea>
                                    @error($lang->locale.'.pray')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror

                                </div>
                                <div class="form-group col-md-12">
                                    <label>وصف الميتا ( {{$lang->name}} )</label>
                                    <textarea class="summernote" name="{{$lang->locale}}[meta_description]" id=""
                                        cols="30"
                                        rows="10">{{isset($setting) ? $setting->translate($lang->locale)['meta_description']:''}}</textarea>
                                    @error($lang->locale.'.pray')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror

                                </div>

                                <div class="form-group col-md-12">
                                    <label>الكلمات الدلاليه ( {{$lang->name}} )</label>
                                    <textarea class="summernote" name="{{$lang->locale}}[meta_keywords]" id="" cols="30"
                                        rows="10">{{isset($setting) ? $setting->translate($lang->locale)['meta_keywords']:''}}</textarea>
                                    @error($lang->locale.'.pray')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror

                                </div>

                            </div>

                        </div>

                        @endforeach

                        <div class="btm-part">
                            <h4 class="title mb-3">شعار الموقع</h4>
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <input type="file" class="_image" name="logo" />
                                    <div class="mt-2 flex-divs">
                                        @if(isset($setting) && $setting->logo)

                                        <img class="image_preview" src="{{asset('images/settings/'.$setting->logo)}}"
                                            alt="">
                                        @else
                                        <img class="image_preview" src="{{asset('default.png')}}" alt="">

                                        @endif
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </div>
        </form>
    </div>
</div>

@endsection
@push('js')
@endpush
