@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('surahs.store')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">@lang('site.add_surah')</h4>
                        </div>
                        <a href="{{route('surahs.index')}}" class="btn btn-outline-secondary add-list">
                            رجوع
                            <i class="las la-long-arrow-alt-left"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                            @foreach(activeLanguages() as $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                       href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                       aria-selected="true">{{$lang->name}}</a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                            @foreach(activeLanguages() as $lang)
                                <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                                     role="tabpanel" aria-labelledby="one-tab">
                                    <div class="flex-divs mb-3">
                                        <div class="input-block">
                                            <label>@lang('site.name') ( {{$lang->name}} )</label>
                                            <input type="text" value="{{old($lang->locale.'.name')}}" name="{{$lang->locale}}[name]" class="">

                                            @error($lang->locale.'.name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    @include('dashboard.includes.seo_inputs',['lang'=>$lang])
                                </div>

                            @endforeach
                            <div class="flex-divs">
                                <div class="select-div">
                                    <label for="">الشيخ</label>
                                    <select class="select-2" name="reader_id" class="" id="">
                                        <option value="" selected disabled> إختر شيخاً</option>
                                        @foreach($readers as $reader)

                                            <option {{old('reader_id') ==$reader->id ? 'selected':''}} value="{{$reader->id}}">{{$reader->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="btm-part">
                                <h4 class="title mb-3">الفيديو</h4>
                                <div class="flex-radio">
                                    <label for="youtube">
                                        <input checked type="radio" class="" id="youtube" value="url" {{old('video_type') =='url'  ?'checked':''}} name="video_type">
                                        <strong>رابط يوتيوب</strong>
                                    </label>
                                    <label for="file">
                                        <input type="radio" id="file" value="file" {{old('video_type') =='file'  ?'checked':''}} name="video_type">
                                        <strong>ملف</strong>
                                    </label>
                                </div>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="url" name="video" class="video_url" placeholder="الصق الرابط هنا">
                                        <input type="file" accept="video/*" name="video" class="video_file" >
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs">
                                    <iframe hidden id="video_frame" width="" height="300" src="">
                                    </iframe>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">ملف الصوت</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input accept="audio/*" type="file" name="audio">
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs" id="audio">
                                    <audio controls class="audio-custom">
                                        <source src="" type="audio/ogg">
                                    </audio>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الصوره</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input accept="image/*" type="file" name="photo">
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('javascript')

@endpush
