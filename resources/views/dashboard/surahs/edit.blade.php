@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('surahs.update',$surah->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">@lang('site.add_surah')</h4>
                        </div>
                        <a href="{{route('surahs.index')}}" class="btn btn-outline-secondary add-list">
                            رجوع
                            <i class="las la-long-arrow-alt-left"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                            @foreach(activeLanguages() as $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                       href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                       aria-selected="true">{{$lang->name}}</a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                            @foreach(activeLanguages() as $lang)
                                <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                                     role="tabpanel" aria-labelledby="one-tab">
                                    <div class="flex-divs mb-3">
                                        <div class="input-block">
                                            <label>@lang('site.name') ( {{$lang->name}} )</label>
                                            <input value="{{$surah->translate($lang->locale)['name']}}" type="text"
                                                   name="{{$lang->locale}}[name]">

                                            @error($lang->locale.'.name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    @include('dashboard.includes.seo_inputs',['lang'=>$lang,'model'=>$surah])
                                </div>

                            @endforeach

                            <div class="flex-divs">
                                <div class="select-div">
                                    <label for="">الشيخ</label>
                                    <select name="reader_id" id="">
                                        <option class="select-2" value="" selected disabled> إختر شيخاً</option>
                                        @foreach($readers as $reader)

                                            <option {{$surah->reader_id == $reader->id ?'selected':''}} value="{{$reader->id}}">
                                                {{$reader->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الفيديو</h4>
                                <div class="flex-radio">
                                    <label for="youtube">
                                        <input {{$surah->video_type == 'url'?'checked':''}} type="radio" class="" id="youtube" value="url" name="video_type">
                                        <strong>رابط يوتيوب</strong>
                                    </label>
                                    <label for="file">
                                        <input {{$surah->video_type == 'file'?'checked':''}} type="radio" id="file" value="file" name="video_type">
                                        <strong>ملف</strong>
                                    </label>
                                </div>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input {{$surah->video_type !== 'url' ? 'hidden':''}}
                                               value="{{$surah->video_type !== 'url' ? '':$surah->video}}" type="url"
                                               name="video" class="video_url" placeholder="الصق الرابط هنا">
                                        <input type="file" value="{{$surah->video_type !== 'url' ? $surah->video:''}}" accept="video/*" name="video" class="video_file" {{$surah->video_type !== 'url' ? '':'hidden'}}>
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs">

                                    <iframe id="video_frame" height="300"
                                            src="{{$surah->video_type === 'url'? getYoutubeId($surah->video) : asset('uploads/surahs/videos/'.$surah->video)}}">
                                    </iframe>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">ملف الصوت</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input value="{{asset('uploads/surahs/audios/'.$surah->audio)}}" accept="audio/*" type="file"
                                               name="audio">
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs" id="audio">
                                    <audio controls class="audio-custom">
                                        <source src="{{asset('uploads/surahs/audios/'.$surah->audio)}}" type="audio/ogg">
                                    </audio>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الصوره</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" accept="image/*" name="photo">
                                    </div>
                                </div>

                                @if($surah->photo)
                                    <img width="100"  height="100" src="{{asset('images/surahs/'.$surah->photo)}}" alt="">
                                @endif

                            </div>

                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('javascript')
    <script>
        $('input[id="youtube"]').click(function () {
            if ($(this).is(':checked')) {
                $('.video_file').val('');
                $('.video_url').removeAttr('hidden');
                $('.video_file').attr('hidden', 'hidden');
            }
        })
        $('input[id="file"]').click(function () {
            if ($(this).is(':checked')) {
                $('.video_url').val('');
                $('.video_url').attr('hidden', 'hidden')
                $('.video_file').removeAttr('hidden')
            }
        })


        $('.video_url').on(' input', function () {

            if ($(this).val().length > 0) {
                let video = $(this).val();
                let spinner = $(this).parent().find('i');
                $.ajax({
                    type: "get",
                    url: "{{route('surahs.getVideo')}}",
                    data: {
                        video: video,
                    },
                    beforeSend: function () {
                        spinner.removeClass('d-none');
                    },
                    success: function (response) {

                        $('#video_frame').attr('src', response.video).removeAttr('hidden').fadeIn(1000);

                    },
                    complete: function () {
                        spinner.addClass('d-none');
                    }
                })

            }

        })
        $('.video_file').on('change', function (e) {
            $('#video_frame').attr('src', URL.createObjectURL(e.target.files[0])).removeAttr('hidden').fadeIn(1000);
        })

        $('input[name="audio"]').on('change', function (e) {
            $('audio').attr('src', URL.createObjectURL(e.target.files[0]))
        })

    </script>

@endpush
