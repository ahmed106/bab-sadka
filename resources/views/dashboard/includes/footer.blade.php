<footer class="iq-footer">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 text-center">
                            <span class="mr-1">
                                <script>
                                    document.write(new Date().getFullYear())
                                </script>© جميع الحقوق محفوظه لدي شركة
                            </span> <a href="https://codlop.sa/ar" target="_blank">Codlop</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
