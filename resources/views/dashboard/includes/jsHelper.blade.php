{{--@foreach(activeLanguages() as $lang)--}}
{{--    <script>--}}
{{--        CKEDITOR.replace('editor-{{$lang->locale}}', {--}}
{{--            language: '{!! config('app.locale')  !!}',--}}
{{--        });--}}
{{--    </script>--}}
{{--@endforeach--}}
<script src="https://kit.fontawesome.com/2eb6657f57.js" crossorigin="anonymous"></script>

<script>
    $('textarea').summernote({
        // lang:"ar-AR"
    });
</script>



<script>
    FilePond.registerPlugin(FilePondPluginImagePreview);
    $('.my-pond').filepond({
        allowMultiple: true,
    });
</script>
<script>
    function handleFiles(event) {
        var files = event.target.files;
        $("#src").attr("src", URL.createObjectURL(files[0]));
        document.getElementById("audio").load();
    }

    // document.getElementById("customFile").addEventListener("change", handleFiles, false);
</script>
<script>
    $(".chosen-select").chosen({rtl: true});
</script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
@toastr_js
@toastr_render


<script>
    $('#check_all').on('click', function () {
        if ($(this).is(':checked')) {
            $('input[class="checkedBtn"]').prop('checked', true);

        } else {
            $('input[class="checkedBtn"]').prop('checked', false);

        }
    });
    $('#deleteAllBtn').click(function (e) {
        e.preventDefault();
        if ($('.checkedBtn[type="checkbox"]:checked').length > 0) {
            Swal.fire({
                title: 'هل أنت  متأكد',
                text: "يمكنك التراجع عن هذا الأمر!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم, حذف!',
                cancelButtonText: 'ألغاء'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#deleteAllForm').submit();
                }
            })
        } else {
            Swal.fire({
                title: 'من فضلك  قم بإختيار بعض العناصر!',
                icon: 'warning',
            })
        }
    })
</script>


<script>
    $('input[id="youtube"]').click(function () {
        if ($(this).is(':checked')) {
            $('.video_file').val('');
            $('.video_url').removeAttr('hidden');
            $('.video_file').attr('hidden', 'hidden');
        }
    })

    $('input[id="file"]').click(function () {
        if ($(this).is(':checked')) {
            $('.video_url').val('');
            $('.video_url').attr('hidden', 'hidden')
            $('.video_file').removeAttr('hidden')
        }
    })


    $('.video_url').on(' input', function () {

        if ($(this).val().length > 0) {
            let video = $(this).val();
            let spinner = $(this).parent().find('i');
            $.ajax({
                type: "get",
                url: "{{route('surahs.getVideo')}}",
                data: {
                    video: video,
                },
                beforeSend: function () {
                    spinner.removeClass('d-none');
                },
                success: function (response) {

                    $('#video_frame').attr('src', response.video).removeAttr('hidden').fadeIn(1000);

                },
                complete: function () {
                    spinner.addClass('d-none');
                }
            })

        }

    })

    $('.video_file').on('change', function (e) {
        $('#video_frame').attr('src', URL.createObjectURL(e.target.files[0])).removeAttr('hidden').fadeIn(1000);
    })

    $('input[name="audio"]').on('change', function (e) {
        $('audio').attr('src', URL.createObjectURL(e.target.files[0]))
    })

    $('input[class="_image"]').on('change', function (e) {
        let file = e.target.files[0];
        let url = URL.createObjectURL(file);
        $(this).closest('div').find('.image_preview').attr('src', url)

    })

    $('#pdf').on('change', function (e) {
        file = e.target.files[0];

        if (file) {
            let url = URL.createObjectURL(file)
            $('#preview_pdf').attr('href', url).removeClass('d-none')
        } else {
            $('#preview_pdf').attr('href', '').addClass('d-none')
        }

    })

    $('.delete_btn').on('click', function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'هل أنت  متأكد',
            text: "يمكنك التراجع عن هذا الأمر!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'نعم, حذف!',
            cancelButtonText: 'ألغاء'
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).parent('form').submit();
            }
        })

    })
</script>


// accordion
<script>
    $(document).ready(function () {
        $(".set > a").on("click", function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".box-custom")
                    .slideUp(200);
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
            } else {
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
                $(this)
                    .find("i")
                    .removeClass("fa-plus")
                    .addClass("fa-minus");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $(".content").slideUp(200);
                $(this)
                    .siblings(".box-custom")
                    .slideDown(200);
            }
        });
    });
</script>

<script>
$(document).ready(function () {
    $('.tagsinput').tagsInput();
    });
</script>

<script>
    $(document).ready(function() {
    $('.select-2').select2();
    });
</script>

<script>
    $('form').on('submit',function (){
        $('button:submit').html('<i class="fa fa-spin fa-spinner"></i>').attr('disabled',true);
    })
</script>
