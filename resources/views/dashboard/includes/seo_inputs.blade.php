<!-- strat -->
<div class="accordion-container ">
    <div class="set">
        <a href="#">
            بيانات السيو SEO
            <i class="fa fa-plus"></i>
        </a>
        <div class="box-custom">
            <div class="flex-divs">
                <div class="input-block">
                    <label>عنوان الميتا</label>
                    <input type="text" value="{{old($lang->locale.'.meta_title',isset($model)?@$model->translate($lang->locale)['meta_title']:'')}}" name="{{$lang->locale}}[meta_title]">
                </div>
                <div class="input-block">
                    <label>الكلمات الدلاليه</label>

                    <input class="tagsinput" value="{{old($lang->locale.'.meta_title',isset($model)?@$model->translate($lang->locale)['meta_keywords']:'')}}" type="text" name="{{$lang->locale}}[meta_keywords]">
                 </div>
                <div class="input-block">
                    <label>وصف الميتا</label>
                    <textarea  name="{{$lang->locale}}[meta_description]"   rows="3">{{old($lang->locale.'.meta_description',isset($model)?@$model->translate($lang->locale)['meta_description']:'')}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end -->
