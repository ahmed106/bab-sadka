<!-- Backend Bundle JavaScript -->
<script src="{{aurl()}}/js/backend-bundle.min.js"></script>
<script src="{{aurl()}}/js/table-treeview.js"></script>
<script src="{{aurl()}}/js/customizer.js"></script>
<script src="{{aurl()}}/js/chart-custom.js"></script>
<script src="{{aurl()}}/js/filepond/filepond.min.js"></script>
<script src="{{aurl()}}/js/filepond/filepond-plugin-image-preview.min.js"></script>
<script src="{{aurl()}}/js/filepond/filepond.jquery.js"></script>
<script src="{{aurl()}}/js/chosen/chosen.jquery.js"></script>
<script src="{{aurl()}}/js/steps/jquery-steps.min.js"></script>
<script src="{{aurl()}}/js/steps/jquery-steps.min.js"></script>
<script src="{{aurl()}}/js/select2/js/select2.js')}}"></script>
<script src="{{aurl()}}/plugins/tagsinput/jquery.tagsinput-revisited.js"></script>
<script src="{{aurl()}}/js/app.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{asset('toastr')}}/cute-alert.js"></script>
{{--<script src="{{aurl('plugins/ckeditor/ckeditor.js')}}"></script>--}}

<!-- include summernote css/js -->

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/lang/summernote-ar-AR.js"></script>
<script src="{{asset('assets/admin/assets/plugins/dropzone/js.js')}}"></script>
@include('dashboard.includes.jsHelper')
@stack('javascript')
