<div class="iq-top-navbar">
    <div class="iq-navbar-custom">
        <nav class="navbar navbar-expand-lg navbar-light p-0">
            <div class="iq-navbar-logo d-flex align-items-center justify-content-between">
                <i class="ri-menu-line wrapper-menu"></i>
                <a href="{{route('dashboard')}}" class="header-logo">
                    <img src="{{aurl()}}/images/logo.svg" class="img-fluid" alt="logo">
                </a>
            </div>
            <div class="iq-search-bar device-search">

            </div>
            <div class="d-flex align-items-center">
                <!--<div class="change-mode">
                  <div class="custom-control custom-switch custom-switch-icon custom-control-inline">
                      <div class="custom-switch-inner">
                          <p class="mb-0"> </p>
                          <input type="checkbox" class="custom-control-input" id="dark-mode" data-active="true">
                          <label class="custom-control-label" for="dark-mode" data-mode="toggle">
                              <span class="switch-icon-left"><i class="a-left ri-moon-clear-line"></i></span>
                              <span class="switch-icon-right"><i class="a-right ri-sun-line"></i></span>
                          </label>
                      </div>
                  </div>
              </div>-->
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-label="Toggle navigation">
                    <i class="ri-menu-3-line"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-list align-items-center">
                        <li class="nav-item nav-icon dropdown">
                            @php $langs = ['en'=>'us','ar'=>'eg']; @endphp
                            <a href="#" class="search-toggle dropdown-toggle btn border add-btn"
                               id="dropdownMenuButton02" data-toggle="dropdown" aria-haspopup="true"
                               aria-expanded="false">
                                <span class="flag-icon flag-icon-{{ $langs[LaravelLocalization::getCurrentLocale()] }}"> </span>
                                {{ LaravelLocalization::getCurrentLocaleNative() }}
                            </a>
                            <div class="iq-sub-dropdown dropdown-menu drop-sm" aria-labelledby="dropdownMenuButton2">
                                <div class="card shadow-none m-0">
                                    <div class="card-body p-1">

                                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                            <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}"
                                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                <span class="flag-icon flag-icon-{{ $langs[$localeCode] }}"> </span>
                                                {{ $properties['native'] }}
                                            </a>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item nav-icon dropdown">
                            <a href="#" class="search-toggle dropdown-toggle" id="dropdownMenuButton"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                     viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                     stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell">
                                    <path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path>
                                    <path d="M13.73 21a2 2 0 0 1-3.46 0"></path>
                                </svg>
                                <span class="bg-primary "></span>
                            </a>
                            <div class="iq-sub-dropdown dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="card shadow-none m-0">
                                    <div class="card-body p-0 ">
                                        <div class="cust-title p-3">
                                            <div class="d-flex align-items-center justify-content-between">
                                                <h5 class="mb-0">Notifications</h5>
                                                <a class="badge badge-primary badge-card" href="#">{{auth('admin')->user()->unreadNotifications()->count()}}</a>
                                            </div>
                                        </div>
                                        <div class="px-3 pt-0 pb-0 sub-card">
                                            @foreach(auth('admin')->user()->unreadNotifications()->get() as $notification)
                                                <a href="{{route('orphans.edit',$notification->data['id'])}}" class="iq-sub-card">
                                                    <div
                                                        class="media align-items-center cust-card py-3 border-bottom">
                                                        <div class="">
                                                            <img src="{{asset('images/orphans/'.$notification->data['photo'])}}" class="avatar-50 rounded-small"
                                                                 href="{{route('orphans.edit',$notification->data['id'])}}" alt="01">
                                                        </div>
                                                        <div class="media-body ml-3">
                                                            <div
                                                                class="d-flex align-items-center justify-content-between">
                                                                <h6 class="mb-0">{{$notification->data['name']}}</h6>
                                                                <small class="text-dark"><b>{{\Carbon\Carbon::make($notification->data['at'])->toDateString()}}</b></small>
                                                            </div>
                                                            <small class="mb-0">{{$notification->data['msg']}}</small>
                                                        </div>
                                                    </div>
                                                </a>
                                            @endforeach
                                        </div>
                                        <a class="right-ic btn btn-primary btn-block position-relative p-2"
                                           href="{{route('orphans.index')}}" role="button">
                                            View All
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item nav-icon dropdown caption-content">
                            <a href="{{route('admins.edit',auth('admin')->id())}}" class="search-toggle dropdown-toggle" id="dropdownMenuButton4"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{auth('admin')->user()->image}}" class="img-fluid rounded" alt="user">
                            </a>
                            <div class="iq-sub-dropdown dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="card shadow-none m-0">
                                    <div class="card-body p-0 text-center">
                                        <div class="media-body profile-detail text-center">
                                            <img src="{{auth('admin')->user()->image}}" alt="profile-img"
                                                 class="rounded profile-img img-fluid avatar-70">
                                        </div>
                                        <div class="p-3">
                                            <h5 class="mb-1">{{auth('admin')->user()->email}}</h5>
                                            <p class="mb-0">Since {{auth('admin')->user()->created_at->toDateString()}}</p>
                                            <div class="d-flex align-items-center justify-content-center mt-3">
                                                <a href="{{route('admins.edit',auth('admin')->id())}}"
                                                   class="btn border mr-2">Profile</a>
                                                <a href="{{route('admin.logout')}}" class="btn border">Sign Out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
