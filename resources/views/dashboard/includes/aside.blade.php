<div class="iq-sidebar  sidebar-default ">
    <div class="iq-sidebar-logo">
        <a href="{{route('dashboard')}}" class="header-logo">
            <img src="{{aurl()}}/images/logo.svg" class="img-fluid" alt="logo">
        </a>
        <div class="iq-menu-bt-sidebar ml-0">
            <i class="las la-bars wrapper-menu bigw"></i>
            <i class="las la-times wrapper-menu smw"></i>
        </div>
    </div>
    <div class="data-scrollbar" data-scroll="1">
        <nav class="iq-sidebar-menu">
            <ul id="iq-sidebar-toggle" class="iq-menu">
                <li class="{{request()->route()->getName() =='dashboard'? 'active':''}}">
                    <a href="{{route('dashboard')}}">
                        <em><img src="{{aurl()}}/images/icons/home-icon.svg" alt=""></em>
                        <span>@lang('site.Home')</span>
                    </a>
                </li>
                <li class="{{active('quran-readers')[0]}} {{active('surahs')[0]}}">
                    <a href="#people" class="collapsed" data-toggle="collapse" aria-expanded="{{active('quran-readers')[2]}} {{active('surahs')[2]}}">
                        <em><img src="{{aurl()}}/images/icons/quran-icon.svg" alt=""></em>
                        <span>@lang('site.el_quran_el_karem')</span>
                        <i class="fa fa-chevron-left iq-arrow-right arrow-active"></i>
                    </a>
                    <ul id="people" class="iq-submenu collapse {{active('quran-readers')[1]}} {{active('surahs')[1]}} " data-parent="#iq-sidebar-toggle">
                        <li>
                            <a href="{{route('quran-readers.index')}}"><i class="las la-minus"></i><span>@lang('site.shiokh')</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('surahs.index')}}">
                                <i class="las la-minus"></i><span>@lang('site.swar')</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="{{active('hadiths')[0]}}">
                    <a href="{{route('hadiths.index')}}">
                        <em><img src="{{aurl()}}/images/icons/hadeth-icon.svg" alt=""></em>
                        <span>@lang('site.el_hadies')</span>
                    </a>
                </li>

                <li class="{{active('prophet-stories')[0]}}">
                    <a href="{{route('prophet-stories.index')}}">
                        <em><img src="{{aurl()}}/images/icons/qesas-icon.svg" alt=""></em>
                        <span>@lang('site.prophet_stories')</span>
                    </a>
                </li>

                <li class="{{active('hesn')[0]}}">
                    <a href="{{route('hesn.index')}}">
                        <em><img src="{{aurl()}}/images/icons/hesn-icon.svg" alt=""></em>
                        <span>حصن المسلم</span>
                    </a>
                </li>

                <li class="{{active('nawawy-forty')[0]}}">
                    <a href="{{route('nawawy-forty.index')}}">
                        <em><img src="{{aurl()}}/images/icons/arba3on-icon.svg" alt=""></em>
                        <span>الأربعون النوويه</span>
                    </a>
                </li>

                <li class="{{active('video-kids')[0]}}">
                    <a href="{{route('video-kids.index')}}">
                        <em><img src="{{aurl()}}/images/icons/atfal-icon.svg" alt=""></em>
                        <span>فيديوهات الأطفال</span>
                    </a>
                </li>


                <li class="{{active('library-categories')[0]}} {{active('libraries')[0]}}">
                    <a href="#libraries" class="collapsed" data-toggle="collapse" aria-expanded="{{active('library-categories')[2]}} {{active('libraries')[2]}}">
                        <em><img src="{{aurl()}}/images/icons/maktabat-icon.svg" alt=""></em>
                        <span>المكتبات</span>
                        <i class="fa fa-chevron-left iq-arrow-right arrow-active"></i>
                    </a>
                    <ul id="libraries" class="iq-submenu collapse {{active('library-categories')[1]}} {{active('libraries')[1]}} " data-parent="#iq-sidebar-toggle">
                        <li>

                            <a href="{{route('library-categories.index')}}"><i class="las la-minus"></i><span>أقسام المكتبات</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('libraries.index')}}">
                                <i class="las la-minus"></i><span>المكتبات</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="{{active('prays')[0]}}">
                    <a href="{{route('prays.index')}}">
                        <em><img src="{{aurl()}}/images/icons/doaa-icon.svg" alt=""></em>
                        <span>الدعاء للمتوفي</span>
                    </a>
                </li>

                <li class="{{active('orphans')[0]}}">
                    <a href="{{route('orphans.index')}}">
                        <em><img src="{{aurl()}}/images/icons/yatem-icon.svg" alt=""></em>
                        <span>إضافه يتيم</span>
                    </a>
                </li>

                <li class="{{active('dead')[0]}}">
                    <a href="{{route('dead.index')}}">
                        <em><img src="{{aurl()}}/images/icons/add.svg" alt=""></em>
                        <span>الصدقات الجاريه</span>
                    </a>
                </li>

                <li class="{{active('countries')[0]}} {{active('cities')[0]}}">
                    <a href="#countries" class="collapsed" data-toggle="collapse" aria-expanded="{{active('countries')[2]}} {{active('cities')[2]}}">
                        <em><img src="{{aurl()}}/images/icons/settings-icon.svg" alt=""></em>
                        <span>الاعدادات</span>
                        <i class="fa fa-chevron-left iq-arrow-right arrow-active"></i>
                    </a>
                    <ul id="countries" class="iq-submenu collapse {{active('countries')[1]}} {{active('cities')[1]}} " data-parent="#iq-sidebar-toggle">
                        <li>

                            <a href="{{route('countries.index')}}"><i class="las la-minus"></i><span>الدول</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{route('cities.index')}}">
                                <i class="las la-minus"></i><span>المدن</span>
                            </a>
                        </li>
                        <li>

                            <a href="{{route('settings.index')}}">
                                <i class="las la-minus"></i><span>إعدادات الموقع</span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="{{active('contact-us')[0]}}">
                    <a href="{{route('contact-us.index')}}">
                        <em><img src="{{aurl()}}/images/icons/contact-icon.svg" alt=""></em>
                        <span>أتصل بنا</span>
                    </a>
                </li>

                @if(auth('admin')->user()->hasPermission('read_admins'))

                    <li class="{{active('admins')[0]}}">
                        <a href="{{route('admins.index')}}">
                            <em><img src="{{aurl()}}/images/icons/about us-icon.svg" alt=""></em>
                            <span>@lang('site.admins')</span>
                        </a>
                    </li>
                @endif
                @if(auth('admin')->user()->hasPermission('read_languages'))
                    <li class="{{active('languages')[0]}}">
                        <a href="{{route('languages.index')}}">
                            <em><img src="{{aurl()}}/images/icons/globe.svg" alt=""></em>
                            <span>@lang('site.languages')</span>
                        </a>
                    </li>
                @endif

                @if(auth('admin')->user()->hasPermission('read_roles'))
                    <li class="{{active('roles')[0]}}">
                        <a class="" href="{{route('roles.index')}}">
                            <em><img src="{{aurl()}}/images/icons/lock.svg" alt=""></em>
                            <span>@lang('site.roles')</span>
                        </a>
                    </li>
                @endif


            </ul>
        </nav>
    </div>
</div>
