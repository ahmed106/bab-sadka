<script>

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-start',
        showConfirmButton: false,
        timer: 5000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
</script>

@if($errors->any())


    @foreach($errors->all() as $error)
        <script>
            Toast.fire({
                icon: 'warning',
                title: '{{$error}}'
            })
        </script>
    @endforeach


@endif
@if(session('error'))

    <script>
        Toast.fire({
            icon: 'warning',
            title: '{{session('error')}}'
        })
    </script>

@endif



@if(session('success'))

    <script>
        Toast.fire({
            icon: 'success',
            title: '{{session('success')}}'
        })
    </script>

@endif
