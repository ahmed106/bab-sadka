<!-- Favicon -->
<link rel="shortcut icon" href="{{aurl()}}/images/logo.svg"/>
<link rel="stylesheet" href="{{aurl()}}/css/datatable/datatables.css">
<link rel="stylesheet" href="{{aurl()}}/vendor/fortawesome/css/font-awesome.css">
<link rel="stylesheet" href="{{aurl()}}/vendor/line-awesome/css/line-awesome.css">
<link rel="stylesheet" href="{{aurl()}}/vendor/remixicon/fonts/remixicon.css">
<link rel="stylesheet" href="{{aurl()}}/js/filepond/filepond.css"/>
<link rel="stylesheet" href="{{aurl()}}/js/filepond/filepond-plugin-image-preview.css"/>
<link rel="stylesheet" href="{{aurl()}}/js/chosen/chosen.css"/>
<link rel="stylesheet" href="{{aurl()}}/js/steps/jquery-steps.min.css"/>
<link rel="stylesheet" href="{{aurl()}}/js/select2/css/select2.css">
<link rel="stylesheet" href="{{aurl()}}/plugins/tagsinput/jquery.tagsinput-revisited.css"/>
<link rel="stylesheet" href="{{aurl()}}/css/style.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js">
{{--flag icon--}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">
{{--end--}}

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

<link id="layoutstyle" rel="stylesheet" href="{{asset('toastr')}}/cute-alert.css">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/admin/assets/plugins/dropzone/css.css')}}">
@toastr_css
@stack('style')
