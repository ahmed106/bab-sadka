@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('hadiths.update',$hadith->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <input type="hidden" value="{{$hadith->id}}" name="id">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">@lang('site.edit_hadith')</h4>
                        </div>
                        <a href="{{route('hadiths.index')}}" class="btn btn-outline-secondary add-list">
                            رجوع
                            <i class="las la-long-arrow-alt-left"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                            @foreach(activeLanguages() as $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                       href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                       aria-selected="true">{{$lang->name}}</a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                            @foreach(activeLanguages() as $lang)
                                <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                                     role="tabpanel" aria-labelledby="one-tab">
                                    <div class="flex-divs mb-3">
                                        <div class="input-block">
                                            <label>@lang('site.name') ( {{$lang->name}} )</label>
                                            <input value="{{$hadith->translate($lang->locale)['name']}}" type="text"
                                                   name="{{$lang->locale}}[name]">
                                            @error($lang->locale.'.name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    @include('dashboard.includes.seo_inputs',['lang'=>$lang,'model'=>$hadith])
                                </div>

                            @endforeach

                            <div class="btm-part">
                                <h4 class="title mb-3">الفيديو</h4>
                                <div class="flex-radio">
                                    <label for="youtube">
                                        <input {{$hadith->video_type == 'url' ? 'checked':''}} type="radio" class=""
                                               id="youtube" value="url" name="video_type">
                                        <strong>رابط يوتيوب</strong>
                                    </label>
                                    <label for="file">
                                        <input {{$hadith->video_type == 'file' ? 'checked':''}} type="radio" id="file"
                                               value="file" name="video_type">
                                        <strong>ملف</strong>
                                    </label>
                                </div>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input {{$hadith->video_type == 'url' ? '':'hidden'}}
                                               value="{{$hadith->video_type == 'url' ? $hadith->video:''}}" type="url"
                                               name="video" class="video_url" placeholder="الصق الرابط هنا">
                                        <input type="file" name="video" class="video_file"
                                            {{$hadith->video_type == 'file' ? '':'hidden'}}>
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs">
                                    <iframe id="video_frame" width="" height="300" src="{{$hadith->video_type == 'file' ? asset('uploads/hadiths/videos/'.$hadith->video):getYoutubeId($hadith->video)}}">
                                    </iframe>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">ملف الصوت</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" name="audio">
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs" id="audio">
                                    <audio controls class="">
                                        <source src="{{asset('uploads/hadiths/audios/'.$hadith->audio)}}" type="audio/ogg">
                                    </audio>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الصوره</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" name="photo">
                                    </div>
                                </div>

                                @if($hadith->photo)
                                    <img width="100" height="100" src="{{asset('images/hadiths/'.$hadith->photo)}}" alt="">
                                @endif

                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('javascript')

@endpush
