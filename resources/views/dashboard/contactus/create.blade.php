@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
<div class="row">
    <div class="col-sm-12">
        <form action="{{route('contact-us.store')}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">أتصل بنا</h4>
                    </div>
                    <a href="{{route('contact-us.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>
                <div class="card-body">

                    <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                        <div class="tab-pane fade active show" id="ar" role="tabpanel" aria-labelledby="one-tab">
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label>@lang('site.name')</label>
                                    <input type="text" name="name">
                                    @error('name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label>@lang('site.email')</label>
                                    <input type="text" name="email">
                                    @error('email')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label>المحتوي</label>
                                    <textarea name="body" id="editor-{{$languages->first()->locale}}" cols="30"
                                        rows="10"></textarea>
                                    @error('body')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('javascript')

@endpush
