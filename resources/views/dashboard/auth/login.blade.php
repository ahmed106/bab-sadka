<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Bab-Sadaka Dash</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{aurl()}}/images/logo.svg"/>
    <link rel="stylesheet" href="{{aurl()}}/css/datatable/datatables.css">
    <link rel="stylesheet" href="{{aurl()}}/vendor/fortawesome/css/font-awesome.css">
    <link rel="stylesheet" href="{{aurl()}}/vendor/line-awesome/css/line-awesome.css">
    <link rel="stylesheet" href="{{aurl()}}/vendor/remixicon/fonts/remixicon.css">
    <link rel="stylesheet" href="{{aurl()}}/js/filepond/filepond.css"/>
    <link rel="stylesheet" href="{{aurl()}}/js/filepond/filepond-plugin-image-preview.css"/>
    <link rel="stylesheet" href="{{aurl()}}/js/chosen/chosen.css"/>
    <link rel="stylesheet" href="{{aurl()}}/css/style.css">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

</head>

<body class="login">
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">
    </div>
</div>
<!-- loader END -->
<div class="wrapper">
    <section class="login-content">
        <div class="container">
            <div class="row align-items-center justify-content-center height-self-center">
                <div class="col-lg-8">
                    <div class="card auth-card">
                        <div class="card-body p-0">
                            <div class="d-flex align-items-center auth-content">
                                <div class="col-lg-7 align-self-center">
                                    <div class="p-3">

                                        <h2 class="mb-2">تسجيل الدخول</h2>
                                        <p>Login to stay connected.</p>
                                        <form action="{{route('admin.doLogin')}}" method="post" autocomplete="off">
                                            @csrf
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="floating-label form-group">
                                                        <input autocomplete="off" value="{{old('email')}}" class="floating-input  @error('email')  is-invalid @enderror form-control" name="email" type="email" placeholder="">
                                                        <label>البريد الالكتروني</label>
                                                        @error('email')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>


                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="floating-label form-group">
                                                        <input required class="floating-input form-control" name="password" type="password" autocomplete="new-password" placeholder=" ">
                                                        <label>كلمة المرور</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group mt-4 mb-4">
                                                <div class="captcha">
                                                    <span>{!! captcha_img('flat') !!}</span>
                                                    <button type="button" class="btn reload" id="reload">
                                                        &#x21bb;
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <input id="captcha" required type="text" class="form-control" placeholder="أدخل الرمز" name="captcha">
                                                @error('captcha')
                                                <span class="text-danger">{{$message}}</span>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-primary w-100 btn-lg mt-4">تسجيل الدخول</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-5 img-login">
                                    <img src="{{aurl()}}/images/logo.svg" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Backend Bundle JavaScript -->
<script src="{{aurl()}}/js/backend-bundle.min.js"></script>
<script src="{{aurl()}}/js/table-treeview.js"></script>
<script src="{{aurl()}}/js/customizer.js"></script>
<script async src="{{aurl()}}/js/chart-custom.js"></script>
<script src="{{aurl()}}/js/filepond/filepond.min.js"></script>
<script src="{{aurl()}}/js/filepond/filepond-plugin-image-preview.min.js"></script>
<script src="{{aurl()}}/js/filepond/filepond.jquery.js"></script>
<script>
    FilePond.registerPlugin(FilePondPluginImagePreview);
    $('.my-pond').filepond({
        allowMultiple: true,
    });
</script>
<script>
    function handleFiles(event) {
        var files = event.target.files;
        $("#src").attr("src", URL.createObjectURL(files[0]));
        document.getElementById("audio").load();
    }

    document.getElementById("customFile").addEventListener("change", handleFiles, false);
</script>
<script src="{{aurl()}}/js/chosen/chosen.jquery.js"></script>
<script>
    $(".chosen-select").chosen({rtl: true});
</script>
<script src="{{aurl()}}/js/app.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


@if(session('error'))
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'warning',
            title: '{{session('error')}}'
        })

    </script>
@endif //end if statement
<script type="text/javascript">
    $('#reload').click(function () {
        $.ajax({
            type: 'GET',
            url: '{{route('reloadCaptcha')}}',
            success: function (data) {
                $(".captcha span").html(data.captcha);
            }
        });
    });
</script>
</body>

</html>
