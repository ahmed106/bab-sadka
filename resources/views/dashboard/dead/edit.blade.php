@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">تعديل صدقه جاريه</h4>
                    </div>
                    <a href="{{route('dead.index')}}" class="btn btn-outline-secondary add-list">رجوع<i
                            class="las la-long-arrow-alt-left"></i></a>
                </div>

                <form method="post" action="{{route('dead.update',$dead->id)}}" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <input type="hidden" name="id" value="{{$dead->id}}">
                    <div class="card-body">
                        <div class="checkout-form checkout-form-custom">
                            <div class="flex-divs">
                                <div class="input-block">
                                    <label>اسم المتوفي</label>
                                    <input value="{{$dead->name}}" type="text" name="name">
                                </div>
                                <div class="input-block">
                                    <label>البريد الإلكتروني</label>
                                    <input type="email" value="{{$dead->email}}" name="email">
                                </div>
                                <div class="input-block">
                                    <label>رقم الهاتف</label>
                                    <input type="number" value="{{$dead->phone}}" name="phone">
                                </div>
                                <div class="input-block">
                                    <label>تاريخ الوفاه</label>
                                    <input type="date" value="{{$dead->death_date}}" name="death_date">
                                </div>
                                <div class="input-block select-div">
                                    <label>الدوله</label>
                                    <select class="select-2" name="country_id" id="">
                                        <option value="">إختر دوله</option>
                                        @foreach($countries as $country)
                                            <option {{$country->id == $dead->country_id ?'selected':''}} value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="input-block select-div">
                                    <label>المدينه</label>
                                    <select class="select-2" name="city_id" id="">
                                        <option value="">إختر مدينه</option>


                                    </select>
                                </div>

                                <div class="input-block select-div">
                                    <label>صوره المتوفي (إختياري)</label>
                                    <input type="file" accept="image/*" name="photo">
                                </div>
                            </div>


                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script>

        $(document).ready(function () {

            let country_id = $('select[name="country_id"]').val();
            $.ajax({
                type: "get",
                url: "{{route('getCountryCities')}}",
                data: {
                    country_id: country_id,
                },
                success: function (res) {
                    res.data.map((res) => {
                        let active = res.id == '{{$dead->city_id}}' ? 'selected' : '';
                        $('select[name="city_id"]').append(`<option ${active} value="${res.id}">${res.name}</option>`)
                    })
                }
            });
        });
        $('select[name="country_id"]').on('change', function () {
            let country_id = $(this).val();
            $('select[name="city_id"]').empty();
            $.ajax({
                type: "get",
                url: '{{route('getCountryCities')}}',
                data: {
                    country_id: country_id
                },
                success: function (res) {
                    res.data.map((res) => {
                        $('select[name="city_id"]').append(`<option value="${res.id}">${res.name}</option>`)
                    });
                }
            });
        });
    </script>

@endpush
