@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">الصدقات الجاريه</h4>
                    </div>
                    @if(auth('admin')->user()->hasPermission('create_dead'))
                        <a href="{{route('dead.create')}}" class="btn btn-secondary add-list"><i class="las la-plus"></i>إنشاء صدقه جاريه</a>
                    @endif
                </div>
                <div class="card-body">

                    @if(auth('admin')->user()->hasPermission('delete_dead'))
                        <button type="submit" class="btn  btn-danger mb-3" id="deleteAllBtn"><i class="las la-trash"></i> حذف المحدد</button>
                    @endif
                    <div class="table-responsive">
                        <form id="deleteAllForm" action="{{route('dead.bulkDelete')}}" method="post">
                            @csrf

                            <table id="datatable" class="table data-table table-bordered table-striped">
                                <thead>
                                <tr class="ligth">
                                    <th>
                                        <div class="checkbox d-inline-block">
                                            <input type="checkbox" class="checkbox-input" id="check_all">
                                            <label for="checkbox1" class="mb-0"></label>
                                        </div>
                                    </th>
                                    <th>الاسم</th>
                                    <th>رابط</th>
                                    <th>الموافقه</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('dashboard.includes.swa')
    <script>

        $('#datatable').DataTable({
            "lengthMenu": [[20, 50, -1], [20, 50, "All"]],
            "processing": true,
            "serverSide": true,
            responsive: true,
            "ajax": {
                "url": "{{route('dead.data')}}",
                "type": "GET",
            },
            "columns": [
                {data: 'check_all', name: 'check_all', sortable: false},
                {data: 'name', name: 'name'},
                {data: 'link', name: 'link'},
                {data: 'approval', name: 'approval'},
                {data: 'actions', name: 'actions'},


            ]

        });
        $(document).on('click', '.approval', function () {
            let id = $(this).data('id'),
                approval = $(this).data('approval');
            $.ajax({
                type: 'POST',
                url: '{{route('dead.approval')}}',
                data: {
                    '_token': '{{csrf_token()}}',
                    'id': id,
                    'approval': approval
                },

                success: function (response) {
                    Toast.fire({
                        title: response.data,
                        icon: 'success',
                    })
                }
            })
        })
    </script>


@endpush
