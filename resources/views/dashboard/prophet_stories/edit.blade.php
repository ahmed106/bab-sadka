@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('prophet-stories.update',$story->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <input type="hidden" value="{{$story->id}}" name="id">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">@lang('site.edit_story')</h4>
                        </div>
                        <a href="{{route('prophet-stories.index')}}" class="btn btn-outline-secondary add-list">
                            رجوع
                            <i class="las la-long-arrow-alt-left"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                            @foreach(activeLanguages() as $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                       href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                       aria-selected="true">{{$lang->name}}</a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                            @foreach(activeLanguages() as $lang)
                                <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                                     role="tabpanel" aria-labelledby="one-tab">
                                    <div class="flex-divs mb-3">
                                        <div class="input-block">
                                            <label>@lang('site.name') ( {{$lang->name}} )</label>

                                            <input value="{{@$story->translate($lang->locale)['name']}}" type="text"
                                                   name="{{$lang->locale}}[name]">

                                            @error($lang->locale.'.name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>@lang('site.name') ( {{$lang->name}} )</label>
                                            <textarea name="{{$lang->locale}}[story]" id="editor-{{$lang->locale}}" cols="30"
                                                      rows="10">{{@$story->translate($lang->locale)['story']}}</textarea>

                                            @error($lang->locale.'.name')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror

                                        </div>
                                    </div>
                                    @include('dashboard.includes.seo_inputs',['lang'=>$lang,'model'=>$story])
                                </div>

                            @endforeach

                            <div class="btm-part">
                                <h4 class="title mb-3">الفيديو</h4>
                                <div class="flex-radio">
                                    <label for="youtube">
                                        <input {{$story->video_type == 'url' ? 'checked':''}} type="radio" class=""
                                               id="youtube" value="url" name="video_type">
                                        <strong>رابط يوتيوب</strong>
                                    </label>
                                    <label for="file">
                                        <input {{$story->video_type == 'file' ? 'checked':''}} type="radio" id="file"
                                               value="file" name="video_type">
                                        <strong>ملف</strong>
                                    </label>
                                </div>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input {{$story->video_type == 'url' ? '':'hidden'}}
                                               value="{{$story->video_type == 'url' ? $story->video:''}}" type="url"
                                               name="video" class="video_url" placeholder="الصق الرابط هنا">
                                        <input type="file" name="video" class="video_file"
                                            {{$story->video_type == 'file' ? '':'hidden'}}>
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs">
                                    <iframe hidden id="video_frame" width="" height="300"
                                            src="{{$story->video_type == 'file' ? asset('uploads/prophet_stories/videos/'.$story->video):getYoutubeId($story->video)}}">
                                    </iframe>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">ملف الصوت</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" name="audio">
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs" id="audio">
                                    <audio controls class="audio-custom">
                                        <source src="{{asset('uploads/prophet_stories/audios/'.$story->audio)}}"
                                                type="audio/ogg">
                                    </audio>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الصوره</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" name="photo">
                                    </div>
                                </div>

                                @if($story->photo)
                                    <img width="100" height="100" src="{{asset('images/prophet_stories/'.$story->photo)}}" alt="">
                                @endif

                            </div>

                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('javascript')

@endpush
