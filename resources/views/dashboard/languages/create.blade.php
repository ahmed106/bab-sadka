@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div class="header-title">
                    <h4 class="card-title">اضافة لغة</h4>
                </div>
                <a href="{{route('languages.index')}}" class="btn btn-outline-secondary add-list">رجوع<i
                        class="las la-long-arrow-alt-left"></i></a>
            </div>
            @if($errors->any())
            <div class="alert alert-danger">

                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach


                </ul>
            </div>

            @endif
            <form method="post" action="{{route('languages.store')}}">
                @csrf

                <div class="card-body">
                    <div class="checkout-form checkout-form-custom">
                        <div class="flex-divs">
                            <div class="input-block">
                                <label>اسم اللغة</label>
                                <input type="text" name="name">
                            </div>
                            <div class="input-block">
                                <label>اختصار اللغة</label>
                                <input type="text" name="locale">
                            </div>
                            <div class="input-block">
                                <label>اختصار الدولة</label>
                                <input type="text" name="country_abbreviation">
                            </div>
                        </div>

                        <div class="btm-part">
                            <div class="flex-radio">
                                <label for="active">
                                    <input type="checkbox" name="active" value="1" id="active">
                                    <strong>مفعل</strong>
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script>

</script>

@endpush
