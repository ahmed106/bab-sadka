@extends('dashboard.layouts.master')
@push('style')
    <style>

    </style>
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">الدعاء للمتوفي</h4>
                    </div>
                    @if(auth('admin')->user()->hasPermission('create_prays'))
                        <a href="{{route('prays.create')}}" class="btn btn-secondary add-list"><i class="las la-plus"></i>إضافه دعاء</a>
                    @endif
                </div>
                <div class="card-body">

                    @if(auth('admin')->user()->hasPermission('delete_prays'))
                        <button type="submit" class="btn  btn-danger mb-3" id="deleteAllBtn"><i class="las la-trash"></i> حذف المحدد</button>
                    @endif
                    <div class="table-responsive">
                        <form id="deleteAllForm" action="{{route('prays.bulkDelete')}}" method="post">
                            @csrf

                            <table id="datatable" class="table data-table table-bordered table-striped">
                                <thead>
                                <tr class="ligth">
                                    <th>
                                        <div class="checkbox d-inline-block">
                                            <input type="checkbox" class="checkbox-input" id="check_all">
                                            <label for="checkbox1" class="mb-0"></label>
                                        </div>
                                    </th>

                                    <th>الدعاء</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@push('javascript')


    <script>
        $('#datatable').DataTable({

            ajax: {
                url: "{{route('prays.data')}}"
            },
            columns: [
                {data: 'check_all', name: 'check_all'},
                {data: 'pray', name: 'pray'},

                {data: 'actions', name: 'actions'},
            ]
        })

    </script>
@endpush
