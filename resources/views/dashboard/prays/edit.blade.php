@extends('dashboard.layouts.master')

@push('style')
@endpush
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('prays.update',$pray->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <div class="header-title">
                            <h4 class="card-title">تعديل</h4>
                        </div>
                        <a href="{{route('prays.index')}}" class="btn btn-outline-secondary add-list">
                            رجوع
                            <i class="las la-long-arrow-alt-left"></i></a>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                            @foreach($languages as $lang)
                                <li class="nav-item">
                                    <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                       href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                       aria-selected="true">{{$lang->name}}</a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                            @foreach($languages as $lang)
                                <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                                     role="tabpanel" aria-labelledby="one-tab">
                                    <div class="flex-divs mb-3">
                                        <div class="input-block">
                                            <label for="">عنوان الدعاء</label>
                                            <input type="text" value="{{$pray->translate($lang->locale)['title']}}"
                                                   name="title">
                                        </div>
                                    </div>
                                    <div class="flex-divs mb-3">
                                        <div class="input-block">
                                            <label>الدعاء ( {{$lang->name}} )</label>
                                            <textarea name="{{$lang->locale}}[pray]" id="editor-{{$lang->locale}}" cols="30"
                                                      rows="10">{{$pray->translate($lang->locale)['pray']}}</textarea>
                                            @error($lang->locale.'.pray')
                                            <span class="text-danger">{{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    @include('dashboard.includes.seo_inputs',['lang'=>$lang,'model'=>$pray])
                                </div>

                            @endforeach

                            <div class="btm-part">
                                <h4 class="title mb-3">الفيديو</h4>
                                <div class="flex-radio">
                                    <label for="youtube">
                                        <input {{$pray->video_type =='url' ?'checked':''}} type="radio" class=""
                                               id="youtube" value="url" name="video_type">
                                        <strong>رابط يوتيوب</strong>
                                    </label>
                                    <label for="file">
                                        <input {{$pray->video_type =='file' ?'checked':''}} type="radio" id="file"
                                               value="file" name="video_type">
                                        <strong>ملف</strong>
                                    </label>
                                </div>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input value="{{$pray->video_type =='url' ? getYoutubeId($pray->video) :''}}   "
                                               {{$pray->video_type =='url' ?'':'hidden'}} type="url" name="video"
                                               class="video_url" placeholder="الصق الرابط هنا">
                                        <input {{$pray->video_type =='file' ?'':'hidden'}} type="file" name="video"
                                               class="video_file">
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs">
                                    <iframe id="video_frame" height="300"
                                            src=" {{$pray->video_type =='file' ? asset('uploads/prays/videos/'.$pray->video):getYoutubeId($pray->video)}}">
                                    </iframe>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">ملف الصوت</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" name="audio">
                                    </div>
                                </div>
                                <div class="mt-2 flex-divs" id="audio">
                                    <audio controls class="audio-custom">
                                        <source src="{{asset('uploads/prays/audios/'.$pray->audio)}}" type="audio/ogg">
                                    </audio>
                                </div>
                            </div>

                            <div class="btm-part">
                                <h4 class="title mb-3">الصوره</h4>
                                <div class="flex-divs">
                                    <div class="input-block">
                                        <input type="file" name="photo">
                                    </div>
                                </div>

                                @if($pray->photo)
                                    <img width="100" height="100" src="{{asset('images/prays/'.$pray->photo)}}" alt="">
                                @endif

                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('javascript')




@endpush
