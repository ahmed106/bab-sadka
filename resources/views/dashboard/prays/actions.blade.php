<div class="d-flex align-items-center list-action">

    <a class="btn btn-success" href="{{route('prays.edit',encrypt($raw->id))}}"><i class="ri-pencil-line mr-0"></i> تعديل</a>


    <form method="post" action="{{route('prays.destroy',$raw->id)}}">
        @csrf
        @method('DELETE')

        <button class="btn btn-danger delete_btn " type="submit"><i class="fa fa-trash"></i>حذف</button>

    </form>


</div>


<script>
    $('.delete_btn').on('click', function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'هل أنت  متأكد',
            text: "يمكنك التراجع عن هذا الأمر!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'نعم, حذف!',
            cancelButtonText: 'ألغاء'
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).parent('form').submit();
            }
        })

    })
</script>
