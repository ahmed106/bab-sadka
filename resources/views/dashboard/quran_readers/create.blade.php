@extends('dashboard.layouts.master')
@push('style')

@endpush

@section('content')


<div class="row">
    <div class="col-sm-12">
        <form action="{{route('quran-readers.store')}}" method="post">
            @csrf
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div class="header-title">
                        <h4 class="card-title">@lang('site.add_shiekh')</h4>
                    </div>
                    <a href="{{route('quran-readers.index')}}" class="btn btn-outline-secondary add-list">
                        رجوع
                        <i class="las la-long-arrow-alt-left"></i></a>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab-1" role="tablist">
                        @foreach(activeLanguages() as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{$loop->first ? 'active':''}}" id="one-tab" data-toggle="tab"
                                href="#{{$lang->locale}}" role="tab" aria-controls="one"
                                aria-selected="true">{{$lang->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content checkout-form checkout-form-custom" id="myTabContent-2">
                        @foreach(activeLanguages() as $lang)
                        <div class="tab-pane fade {{$loop->first ? 'active':''}} show" id="{{$lang->locale}}"
                            role="tabpanel" aria-labelledby="one-tab">
                            <div class="flex-divs mb-3">
                                <div class="input-block">
                                    <label>@lang('site.name') ( {{$lang->name}} )</label>
                                    <input type="text" name="{{$lang->locale}}[name]">

                                    @error($lang->locale.'.name')
                                    <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>

                          @include('dashboard.includes.seo_inputs',['lang'=>$lang])
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> حفظ  </button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@push('javascript')



@endpush
