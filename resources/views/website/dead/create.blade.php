@extends('website.layouts.master')
@push('title')
    إنشاء صدقه جاريه
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <form id="dead_form" action="{{url('/add-dead')}}" method="post" enctype="multipart/form-data">
            @csrf

            <div class="content">
                <div class="top-menu checkout-form">
                    <div class="right">
                        <h3>إنشاء صدقه جاريه</h3>
                    </div>
                </div>
                <div class="settings">
                    {{--                    <h3>يمكنك تعديل اختيار اسم الدولة لحساب أوقات الصلاة ، واختيار اسم المتوفى</h3>--}}
                    <div class="rosary-select checkout-form">

                        <div class="select-div">
                            <label for="" class="form-label">إسم المتوفي</label>
                            <input type="text" value="{{old('name')}}" name="name">
                        </div>
                        <div class="select-div">
                            <label for="" class="form-label">البريد الإلكتروني</label>
                            <input type="email" value="{{old('email')}}" name="email">
                        </div>
                        <div class="select-div">
                            <label for="" class="form-label">رقم الهاتف</label>
                            <input type="number" value="{{old('phone')}}" name="phone">
                        </div>
                        <div class="select-div">
                            <label for="" class="form-label">تاريخ الوفاه</label>
                            <input type="date" value="{{old('death_date')}}" name="death_date">
                        </div>
                        <div class="select-div">
                            <label for="">اختر اسم الدولة</label>
                            <select class="country_id select-2" name="country_id">
                                <option value="">اختر اسم الدولة</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="select-div">
                            <label for="">اختر اسم المحافظة</label>
                            <select name="city_id" class="city_id select-2">
                                <option value="">اختر اسم المحافظة</option>
                            </select>
                        </div>

                        <div class="input-file">
                            <label for="" class="form-label">صوره المتوفي( إختياري )</label>
                            <input type="file" name="photo">
                        </div>

                        <button type="submit" class="btn-ok">موافق</button>

                    </div>
                </div>
            </div>
        </form>


    </div><!-- //Main wrapper -->




    <!-- Modal -->
    <div class="modal fade modal-index bg" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="added-successfully">
                    <img src="{{asset('assets/front/')}}/assets/images/tick.png" alt="">
                    <h3><span>شكرا لك .. </span>تم ارسال طلبك بنجاح</h3>
                    <h4>سيتم الرد عليك من قبل الإدارة في أقرب وقت</h4>
                    <div class="footer-div">
                        <h3>لمزيد من التفاصيل راسلنا على</h3>
                        <ul class="social-div">
                            <li><a href="https://wa.me/966558706532" target="_blank"><img
                                        src="http://127.0.0.1:8000/assets/front/assets/images/whatsapp.png" alt=""></a></li>
                            <li><a href="https://www.facebook.com/messages/t/108939548433772" target="_blank"><img
                                        src="http://127.0.0.1:8000/assets/front/assets/images/messenger.png" alt=""></a>
                            </li>
                        </ul>
                    </div>
                    <div class="offer-div">
                        <p>عرض خاص لأول ٥٠٠٠ مشترك بـ ١٠٠ جنية فقط <br> مقابل إشتراك مدى الحياة بدلاً من اشتراك يجدد كل سنة</p>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
@push('js')

    <script>
        $('.country_id').on('change', function () {
            let country_id = $(this).val();
            $('.city_id').empty();
            $.ajax({
                type: "get",
                url: '{{route('website.getCity')}}',
                data: {
                    country_id: country_id
                },
                success: function (res) {
                    res.data.map((res) => {

                        $('.city_id').append(`<option value="${res.id}">${res.name}</option>`)
                    });
                }
            });
        });


        $('#dead_form').on('submit', function (e) {
            e.preventDefault();
            let form = new FormData($('#dead_form')[0]),
                url = $(this).attr('action');

            $.ajax({

                type: 'post',
                url: url,
                contentType: false,
                cache: false,
                processData: false,
                data: form,
                success: function (response) {
                    $('#exampleModal').modal('show');
                    $('#dead_form').trigger('reset')
                },
                error: function (xhr) {


                    Toast.fire({
                        icon: 'warning',
                        title: xhr.responseJSON.error
                    })


                }
            })
        })

    </script>
@endpush
