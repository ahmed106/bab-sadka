@extends('website.layouts.master')

@push('title')
    صدقه جاريه علي المرحوم      {{$dead->name}}
@endpush
@push('seo')

    <meta name="title" content=" {{$dead->name}}  صدقه جاريه علي المرحوم ">
    <meta name="description" content="{{$dead->name}}  صدقه جاريه علي المرحوم ">

@endpush

<style>
    .box-2 {
        display: none !important
    }
</style>
@section('content')


    <div class="d-none">
        <audio id="audio" autoplay controls src="{{asset('sounds.mpeg')}}"></audio>
    </div>
    <div class="wrapper">
        <!-- Header -->
        <header class="header-logo">
            <div class="container">
                <img class="top-img" src="{{asset('assets/front/')}}/assets/images/bg/top_img.png" alt="">
                <img class="logo" src="{{asset('assets/front/')}}/assets/images/bg/text-img.png" alt="">
            </div>
        </header><!-- //Header -->
        <!-- Top Banner -->
        <div class="banner-area home-page">
            <div class="banner banner-slide-active slide-animate-text" dir="rtl">
                <!-- Single Banner -->
                <div class="banner__single fullscreen d-flex align-items-center"
                     style="background: url({{asset('assets/front/')}}/assets/images/bg/1.jpg);" data-black-overlay="3">
                    <video autoplay muted loop id="myVideo">
                        <source src="{{asset('assets/front/')}}/assets/images/bg.mp4" type="video/mp4"/>
                    </video>
                </div>
                <div class="banner__single fullscreen d-flex align-items-center"
                     style="background: url({{asset('assets/front/')}}/assets/images/bg/2.jpg);" data-black-overlay="4">
                    <video autoplay muted loop id="myVideo">
                        <source src="{{asset('assets/front/')}}/assets/images/bg.mp4" type="video/mp4"/>
                    </video>
                </div>
            </div>
        </div>


        <div class="custom-size">
            <div class="banner__content text-center">
                <h3>صدقة جارية عن</h3>
                <h1 id="dead-name">{{$dead->name}}</h1>
                @if($dead->photo)
                    <div class="box">
                        <img class="dead-img" src="{{asset('images/deads/'.$dead->photo)}}" alt="">
                    </div>
                @endif
            </div>
            <ul class="menu-home">
                <li>
                    <a title="{{__('site.el_quran_el_karem')}}"
                       href="{{url('quran?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/quran-icon.svg"
                             alt="{{__('site.el_quran_el_karem')}}" title="{{__('site.el_quran_el_karem')}}">
                        <span>القرآن الكريم</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.hadiths')}}" href="{{url('/hadith?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/hadeth-icon.svg"
                             alt="{{__('site.hadiths')}}" title="{{__('site.hadiths')}}">
                        <span>الحديث النبوي</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.prophet_stories')}}"
                       href="{{url('/prophets-stories?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/qesas-icon.svg"
                             alt="{{__('site.prophet_stories')}}" title="{{__('site.prophet_stories')}}">
                        <span>قصص الأنبياء</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.hesn_elmuslim')}}"
                       href="{{url('/hesn-elmuslim?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/hesn-icon.svg"
                             alt="{{__('site.hesn_elmuslim')}}" title="{{__('site.hesn_elmuslim')}}">
                        <span>حصن المسلم</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.arboaon_nwawya')}}"
                       href="{{url('/arboon-nwawia?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/arba3on-icon.svg"
                             alt="{{__('site.arboaon_nwawya')}}" title="{{__('site.arboaon_nwawya')}}">
                        <span>الأربعون النووية</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.kid_videos')}}"
                       href="{{url('/kid-videos?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/atfal-icon.svg"
                             alt="{{__('site.kid_videos')}}" title="{{__('site.kid_videos')}}">
                        <span>فيديوهات أطفال</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.masbaha')}}"
                       href="{{url('/masbaha?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/mesbaha-icon.svg"
                             alt="{{__('site.masbaha')}}" title="{{__('site.masbaha')}}">
                        <span>المسبحة</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.libraries')}}"
                       href="{{url('/libraries?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/maktabat-icon.svg"
                             alt="{{__('site.libraries')}}" title="{{__('site.libraries')}}">
                        <span>المكتبات</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/pray?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/doaa-icon.svg" alt="">
                        <span>الدعاء للمتوفي</span>
                    </a>
                </li>
                {{--                <li>--}}
                {{--                    <a title="{{__('site.add_orphan')}}"--}}
                {{--                       href="{{url('add-orphan?dead='.Session::get('user.dead'))}}">--}}
                {{--                        <img src="{{asset('assets/front/')}}/assets/images/icons/yatem-icon.svg"--}}
                {{--                             alt="{{__('site.add_orphan')}}" title="{{__('site.add_orphan')}}">--}}
                {{--                        <span>إضافة يتيم</span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                <li>
                    <a title="{{__('site.add_dead')}}"
                       href="{{url('add-dead?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/add.svg"
                             alt="{{__('site.add_dead')}}" title="{{__('site.add_dead')}}">
                        <span>إضافة صدقه جاريه</span>
                    </a>
                </li>
            </ul>
            <div class="swiper moaket" dir="rtl" id="pray_times">

            </div>
        </div>


    </div>

    <div>


    </div>
@endsection
@push('js')

    <script>
        Echo.channel('free-channel')
            .listen('.chat-event', (e) => {
                $('#audio').get(0).muted = true;
            });
        $('#play_sound').removeClass('d-none');
    </script>


    @once
        <script>
            let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            $('#pray_times').html('');
            $.ajax({
                type: 'get',
                url: '{{route('website.getPrayTimes')}}',
                data: {
                    timezone: timezone
                },
                success: function (res) {

                    $('#pray_times').append(res.data)

                }
            })
        </script>
    @endonce
@endpush
