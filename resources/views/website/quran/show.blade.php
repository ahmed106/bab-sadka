@extends('website.layouts.master')
@push('title')
    {{__('site.home')}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">
            <div class="he-vid">
                <div class="vid-main-wrapper">
                    <!-- THE YOUTUBE PLAYER -->

                    <div class="vid-container">
                        @if($surah->video_type=='url')
                            <iframe id="vid_frame" src="{{getYoutubeId($surah->video)}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @else
                            <video autoplay width="100%" controls src="{{asset('uploads/surahs/videos/'.$surah->video)}}"></video>
                        @endif


                    </div>
                    <!-- THE PLAYLIST -->


                    <div class="vid-list-container">
                        <ol class="vid-list" id="scrollable">
                            @foreach($surahs as $sura)
                                <li class="{{$sura->id==$surah->id ? 'active':''}}">
                                    <a href="{{url('/quran-videos/'.$sura->id)}}"
                                        {{--onClick="document.getElementById('vid_frame').src='{{getYoutubeId($kid_video->video)}}'"--}}>
                            <span class="vid-thumb">

                                  @if($sura->photo)
                                    <img src="{{asset('images/surahs/'.$sura->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif


                            </span>
                                        <div class="desc">{{$sura->name}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
