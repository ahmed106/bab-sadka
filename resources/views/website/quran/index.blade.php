@extends('website.layouts.master')
@push('title')
    {{__('site.el_quran_el_karem')}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">


            <section class="cr-section islams-pillar-area">
                <div class="islams-pillars__thumb text-center">
                    <img class="icon-page" src="{{asset('assets/front/assets/images/2.png')}}" alt="{{__('site.el_quran_el_karem')}}" title="{{__('site.el_quran_el_karem')}}">
                </div>


                <div class="islams-pillars__content">
                    <h2>
                        قال تعالى : ( إِنَّ الَّذِينَ يَتْلُونَ كِتَابَ اللَّهِ وَأَقَامُوا الصَّلَاةَ * وَأَنفَقُوا مِمَّا رَزَقْنَاهُمْ سِرًّا وَعَلَانِيَةً يَرْجُونَ تِجَارَةً لَّن تَبُورَ لِيُوَفِّيَهُمْ أُجُورَهُمْ وَيَزِيدَهُم مِّن فَضْلِهِ إِنَّهُ غَفُورٌ شَكُورٌ
                        )
                    </h2>
                </div>
                <ul class="btns-btm">
                    <li>
                        <a title="{{__('site.read')}}" href="{{url('quran/read')}}">
                            <img src="{{asset('assets/front/assets/images/icons/read-icon.svg')}}" alt="{{__('site.read')}}" title="{{__('site.read')}}">
                            قراءة
                        </a>
                    </li>
                    <li>
                        <a href="{{url('/quran/listen')}}" class="style1" title="{{__('site.listen')}}">
                            <img src="{{asset('assets/front/assets/images/icons/listen-icon.svg')}}" alt="{{__('site.listen')}}" title="{{__('site.listen')}}">
                            استماع
                        </a>
                    </li>
                    {{--                    <li>--}}
                    {{--                        <a href="{{url('quran/videos')}}" class="style2" title="{{__('site.videos')}}">--}}
                    {{--                            <img src="{{asset('assets/front/assets/images/icons/vedio-icon.svg')}}" alt="{{__('site.videos')}}" title="{{__('site.videos')}}">--}}
                    {{--                            فيديوهات--}}
                    {{--                        </a>--}}
                    {{--                    </li>--}}
                </ul>
            </section><!-- //Pillar Of Islam -->
        </div>
    </div>
@endsection
@push('js')

@endpush
