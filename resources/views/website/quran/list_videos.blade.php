@if(count($surahs) >0)

    <ul class="list-videos posters">
        @foreach($surahs as $index => $surah)
            <li>
                <a href="{{url('/quran-videos/'.$surah->id)}}">

                    @if($surah->photo)
                        <img src="{{asset('images/surahs/'.$surah->photo)}}" alt="">
                    @else
                        <img src="{{asset('def.png')}}" alt="">
                    @endif


                    <i class="fa fa-play"></i>
                    <span>{{$surah->name}}</span>
                </a>
            </li>
        @endforeach

    </ul>
    {{$surahs->links()}}


@else
    <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
@endif


@push('js')

@endpush

