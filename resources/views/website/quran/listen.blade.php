@extends('website.layouts.master')

@push('title')
    {{__('site.el_quran_el_karem')}} | {{__('site.listen')}}
@endpush
@push('seo')
    <meta name="title" content="{{$surahs->first() ? $surahs->first()->meta_title:''}}">
    <meta name="description" content="{!! $surahs->first() ? strip_tags($surahs->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$surahs->first() ? $surahs->first()->meta_keywords:''}}">
@endpush


@section('content')

    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>القرآن الكريم / استماع</h3>
                </div>
            </div>
            <div class="top-menu checkout-form pr-0">
                <div class="right">
                    <h3>
                        {{$surahs->first()?$surahs->first()->reader->name:''}}
                    </h3>
                </div>
                <div class="left">
                    <div class="select-div">
                        <form action="{{route('readerSearch')}}" id="search_reader" type="get">
                            <select class="select-2" onchange="$('#search_reader').submit()" name="reader_id">
                                <option value="" disabled selected>اختر القارئ</option>
                                @foreach($readers as $reader)
                                    <option {{$surahs->first() && $surahs->first()->reader_id == $reader->id?'selected':'' }} value="{{$reader->id}}">
                                        {{$reader->name}}
                                    </option>
                                @endforeach
                            </select>
                        </form>
                        <input type="hidden" name="reader_id" value="{{$surahs->first() ?$surahs->first()->reader->id:''}}">

                    </div>
                    <div class="input-div">
                        <input name="surah" type="text" placeholder="ابحث باسم السورة">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
            <div id="surahs">
                @include('website.quran.surahs')

            </div>
        </div>
    </div>


    <div class="bottombar">
        <div class="bottombar-container">
            <div id="sound-player" class="jp-jplayer">
                <img id="jp_poster_0">
                <audio id="jp_audio_0" preload="metadata"></audio>
            </div>

            <div id="sound-container-1" class="footer_sound_container jp-audio top-player audio_player">

                <div class="player-nine">

                    <div class="jp-type-single">
                        <div class="jp-gui jp-interface">
                            <div class="player-container-left">
                                <a id="prev-button" class="prev-button prev-button-disabled" title="السابق"></a>

                                <a class="jp-play jp_play_footer  " tabindex="2" title="تشغيل"></a>
                                <a class="jp-pause jp_pause_footer " tabindex="2" title="ايقاف"></a>
                                <a id="next-button" class="next-button next-button-disabled" title="التالي"></a>
                                <div class="jp-current-time" id="current-time">00:00</div>
                            </div>
                            <div class="player-container-middle">
                                <div class="jp-progress">
                                    <div class="jp-seek-bar">
                                        <div class="jp-play-bar">
                                            <div class="bullet"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="player-container-right">
                                <div class="jp-duration" id="duration-time">00:00</div>
                                <a class="jp-mute" tabindex="1" title="صامت"></a>
                                <a class="jp-unmute" tabindex="1" title="غير صامت"></a>
                                <div class="jp-volume-bar" title="مستوى الصوت">
                                    <div class="jp-volume-bar-value">
                                        <div class="bullet"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="player-three">
                    <div id="topbar-track-info">
                        <div id="topbar-track-details">


                            <div class=" d-none" id="loader">
                                <i class="fa fa-spin fa-spinner fa-1x text-center"></i>
                            </div>
                            <div class="topbar-song-name d-none " id="sw-song-name"></div>

                            <div class="topbar-song-name d-none" id="sw-author-name"></div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@push('js')

    <script src="{{asset('assets/front/assets/js/jPlayer/jquery.jplayer.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('.jp-play').on('click', function () {
                let audio_id = $(this).data('audio_id');

                $('.footer_sound_container').attr('id', 'sound-container-' + audio_id);
                playAudio(audio_id, 'surahs');
                play(audio_id)
            });

            function playAudio(id, type) {
                $.ajax({
                    type: 'get',
                    url: '{{route('hadith.getAudio')}}',
                    data: {
                        id: id,
                        'class': type
                    },
                    beforeSend: function () {

                    },
                    success: function (data) {
                        $('.audio_player').html(data.data);


                    },
                    complete: function () {
                        $('#loader').addClass('d-none')
                    }
                });
            }

            function play(id) {
                $('#loader').removeClass('d-none');
                $('.topbar-song-name').html('')
                $('#sound-container-' + id).jPlayer({
                    ready: function () { // The $.jPlayer.event.ready event
                        $(this).jPlayer("setMedia", { // Set the media
                            m4a: "{{asset('uploads/surahs/audios')}}" + '/' + $('#sound-container-' + id).data('sound'),
                            oga: "{{asset('uploads/surahs/audios')}}" + '/' + $('#sound-container-' + id).data('sound')
                        }).jPlayer("play"); // Attempt to auto play the media
                    },
                    play: function () {
                        $(this).jPlayer("pauseOthers", 0); // pause all players except this one.
                    },
                    cssSelectorAncestor: "#sound-container-" + id,
                    swfPath: "/js",
                    supplied: "m4a, oga",
                    useStateClassSkin: true,
                    autoBlur: true,
                    smoothPlayBar: true,
                    keyEnabled: true,
                    remainingDuration: true,
                    toggleDuration: true


                });


            }

        });
    </script>

    <script>
        $('input[name="surah"]').on('keyup', function () {
            let surah = $(this).val(),
                reader_id = $('input[name = "reader_id"]').val();
            $.ajax({
                get: "get",
                url: "{{route('quran.getSurah')}}",
                data: {
                    reader: reader_id,
                    surah: surah,
                },
                success: function (response) {
                    $('#surahs').html(response.view)
                }

            })

        })

    </script>
@endpush
