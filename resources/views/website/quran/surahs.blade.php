@if(count($surahs) > 0)
    <ul class="list-sor">
        @foreach($surahs as $index=> $surah)
            @if($surah->audio)
                <li class="sora-container">
                    <div class="sora-num">
                        <img src="{{asset('assets/front')}}/assets/images/icons/rub-el-hizb-icon.svg" alt="{{$surah->name}}" title="{{$surah->name}}">
                        <span>{{++$index}}</span>
                    </div>
                    <div class="player">
                        <div class="sora-top">
                            <div class="sora-titles">
                                <div class="sora-title">
                                    <input type="hidden" value="{{$surah->name}}">
                                    {{$surah->name}}
                                </div>
                            </div>
                        </div>
                        <div class="player-controls">
                            <div id="sound-container-{{$surah->id}}" data-reader_name="{{$surah->reader->name}}" data-id="{{$surah->id}}" data-surah_name="{{$surah->name}}" data-sound="{{$surah->audio}}" class="jp-jplayer sound_player"></div>
                            <div id="sound-container-{{$surah->id}}" class="jp-audio top-player">
                                <div class="player-nine">
                                    <div class="jp-type-single">
                                        <div class="jp-gui jp-interface">
                                            <div class="player-container-left">

                                                <a class="jp-play" data-audio_id="{{$surah->id}}" tabindex="1" title="تشغيل"></a>
                                                <a class="jp-pause" tabindex="1" title="ايقاف"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            @endif

        @endforeach


    </ul>
@else
    <div class="alert text-center alert-danger">للأسف لا يوجد بيانات</div>
@endif
<script src="{{asset('assets/front/assets/js/jPlayer/jquery.jplayer.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.jp-play').on('click', function () {
            let audio_id = $(this).data('audio_id');

            $('.footer_sound_container').attr('id', 'sound-container-' + audio_id);
            playAudio(audio_id, 'surahs');
            play(audio_id)
        });

        function playAudio(id, type) {
            $.ajax({
                type: 'get',
                url: '{{route('hadith.getAudio')}}',
                data: {
                    id: id,
                    'class': type
                },

                beforeSend: function () {
                    $('#loader').removeClass('d-none');
                    $('.topbar-song-name').html('')
                },
                success: function (data) {
                    $('.audio_player').html(data.data);

                },
                complete: function () {
                    $('#loader').addClass('d-none')
                }
            });
        };

        function play(id) {

            $('#sound-container-' + id).jPlayer({
                ready: function () { // The $.jPlayer.event.ready event
                    $(this).jPlayer("setMedia", { // Set the media
                        m4a: "{{asset('uploads/surahs/audios')}}" + '/' + $('#sound-container-' + id).data('sound'),
                        oga: "{{asset('uploads/surahs/audios')}}" + '/' + $('#sound-container-' + id).data('sound')
                    }).jPlayer("play"); // Attempt to auto play the media
                },
                play: function () {
                    $(this).jPlayer("pauseOthers", 0); // pause all players except this one.
                },
                cssSelectorAncestor: "#sound-container-" + id,
                swfPath: "/js",
                supplied: "m4a, oga",
                useStateClassSkin: true,
                autoBlur: true,
                smoothPlayBar: true,
                keyEnabled: true,
                remainingDuration: true,
                toggleDuration: true


            });
        }

    });
</script>



