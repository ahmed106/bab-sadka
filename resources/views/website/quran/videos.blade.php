@extends('website.layouts.master')
@push('title')
    {{__('site.el_quran_el_karem')}} | {{__('site.videos')}}
@endpush
@push('seo')
    <meta name="title" content="{{$surahs->first() ? $surahs->first()->meta_title:''}}">
    <meta name="description" content="{!! $surahs->first() ? strip_tags($surahs->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$surahs->first() ? $surahs->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <input type="hidden" name="reader_id" value="{{$surahs->first()?$surahs->first()->reader->id:''}}">
        <div class="content">
           
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> القرآن الكريم / فيديوهات</h3>
                </div>
                <div class="left">
                    <div class="select-div">
                        <form action="{{route('quran.videos')}}" method="get">
                            <select class="select-2" onchange="$(this).parent().submit()" name="reader_id">
                                <option value="0" disabled selected>اختر القارئ</option>
                                @foreach($readers as $reader)
                                    <option {{$surahs->first() && $surahs->first()->reader_id == $reader->id?'selected':'' }}
                                            value="{{$reader->id}}">{{$reader->name}}</option>
                                @endforeach
                            </select>
                        </form>
                    </div>
                    <div class="input-div">
                        <input type="text" name="surah" placeholder="ابحث باسم السورة">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
            <h3 class="qare2">{{ $surahs->first()?$surahs->first()->reader->name:'' }}</h3>
            <div id="list_videos">
                @include('website.quran.list_videos')
            </div>

        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')



    <script>
        $('input[name="surah"]').on('keyup', function () {

            let surah = $(this).val(),
                reader_id = $('input[name = "reader_id"]').val();


            $.ajax({
                get: "get",
                url: "{{route('quran.getVideos')}}",
                data: {
                    reader: reader_id,
                    surah: surah,
                },
                success: function (response) {
                    $('#list_videos').html(response.view)
                }

            })

        });


    </script>
@endpush
