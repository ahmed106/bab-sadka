@extends('website.layouts.master')
@push('title')
    {{__('site.settings')}}
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <form action="{{url('/settings')}}" method="post">
            @csrf

            <div class="content">
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>الإعدادات</h3>
                </div>
            </div>
                <div class="settings">
                    <h3>يمكنك تعديل اختيار اسم الدولة لحساب أوقات الصلاة ، واختيار اسم المتوفى</h3>
                    <div class="rosary-select checkout-form">
                        <div class="select-div">
                            <label for="">اختر اسم الدولة</label>
                            <select class="country_id select-2" name="country">
                                <option value="">اختر اسم الدولة</option>

                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="select-div">
                            <label for="">اختر اسم المحافظة</label>
                            <select name="city" class="city_id select-2">
                                <option value="">اختر اسم المحافظة</option>

                            </select>

                        </div>
                        <div class="select-div">
                            <label for="">اختر اسم المتوفى</label>
                            <select class="select-2 deads" name="dead-name">
                                <option value="">اختر اسم المتوفى</option>

                            </select>

                        </div>
                        <button type="submit" class="btn-ok">موافق</button>
                    </div>
                </div>
            </div>
        </form>

    </div><!-- //Main wrapper -->
@endsection
@push('js')

    <script>
        $('.country_id').on('change', function () {
            let country_id = $(this).val();
            $('.city_id').empty();
            $.ajax({
                type: "get",
                url: '{{route('website.getCity')}}',
                data: {
                    country_id: country_id
                },
                success: function (res) {

                    let cities = `<option value="">إختر اسم المدينه </option>`
                    res.data.map((res) => {
                        cities += `<option value="${res.id}">${res.name}</option>`

                    });
                    $('.city_id').append(cities)
                }
            });
        });
        
        $('.city_id').on('change', function () {
            let city_id = $(this).val();
            $('.deads').empty();
            $.ajax({
                type: "get",
                url: '{{route('website.getDeads')}}',
                data: {
                    city_id: city_id
                },
                success: function (res) {
                    let deads = `<option value="">إختر اسم المتوفي </option>`
                    res.data.map((res) => {
                        deads += `<option value="${res.name}">${res.name}</option>`


                    });
                    $('.deads').append(deads)
                },

            });
        });

    </script>
@endpush
