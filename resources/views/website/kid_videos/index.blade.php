@extends('website.layouts.master')
@push('title')
    {{__('site.kid_videos')}}
@endpush
@push('seo')
    <meta name="title" content="{{$videos->first() ? $videos->first()->meta_title:''}}">
    <meta name="description" content="{!! $videos->first() ? strip_tags($videos->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$videos->first() ? $videos->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> فيديوهات الأطفال </h3>
                </div>
            </div>

            @if(count($videos) >0)
                <ul class="list-videos posters">
                    @foreach($videos as $index => $video)

                        <li>
                            <a href="{{url('/kid-videos/'.$video->id)}}">
                                @if($video->photo)
                                    <img src="{{asset('images/video_kids/'.$video->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif


                                <i class="fa fa-play"></i>
                                <span>{{$video->title}}</span>
                            </a>
                        </li>



                    @endforeach

                </ul>

                {{$videos->links()}}
                <span> showing {{$videos->links()->paginator->currentPage()}}  from {{$pages}} pages</span>

            @else
                <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
            @endif


        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')

    {{--    <script>--}}
    {{--        var SITEURL = "{{ url('/') }}";--}}
    {{--        var page = 1; //track user scroll as page number, right now page number is 1--}}
    {{--        load_more(page); //initial content load--}}
    {{--        $(window).scroll(function() { //detect page scroll--}}
    {{--            if($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled from top to bottom of the page--}}
    {{--                page++; //page number increment--}}
    {{--                load_more(page); //load content--}}
    {{--            }--}}
    {{--        });--}}
    {{--        function load_more(page){--}}
    {{--            $.ajax({--}}
    {{--                url: SITEURL + "/kid-videos?page=" + page,--}}
    {{--                type: "get",--}}
    {{--                datatype: "html",--}}
    {{--                beforeSend: function()--}}
    {{--                {--}}
    {{--                    $('.ajax-loading').show();--}}
    {{--                }--}}
    {{--            })--}}
    {{--                .done(function(data)--}}
    {{--                {--}}
    {{--                    console.log(data)--}}
    {{--                    if(data.length == 0){--}}
    {{--                        console.log(data.length);--}}
    {{--                        //notify user if nothing to load--}}
    {{--                        $('.ajax-loading').html("No more records!");--}}
    {{--                        return;--}}
    {{--                    }--}}
    {{--                    $('.ajax-loading').hide(); //hide loading animation once data is received--}}
    {{--                    $(".list-videos").append(data); //append data into #results element--}}
    {{--                    console.log('data.length');--}}
    {{--                })--}}
    {{--                .fail(function(jqXHR, ajaxOptions, thrownError)--}}
    {{--                {--}}
    {{--                    alert('No response from server');--}}
    {{--                });--}}
    {{--        }--}}
    {{--    </script>--}}


@endpush
