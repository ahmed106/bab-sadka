@extends('website.layouts.master')
@push('title')
    {{__('site.kid_videos')}} | {{$video->title}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">
            <div class="he-vid">
                <div class="vid-main-wrapper">
                    <!-- THE YOUTUBE PLAYER -->

                    <div class="vid-container">
                        @if($video->video_type=='url')
                            <iframe id="vid_frame" src="{{getYoutubeId($video->video)}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @else
                            <video autoplay width="100%" controls src="{{asset('uploads/video_kids/videos/'.$video->video)}}"></video>
                        @endif

                    </div>
                    <!-- THE PLAYLIST -->
                    <div class="vid-list-container">
                        <ol class="vid-list" id="scrollable">
                            @foreach($kid_videos as $kid_video)
                                <li class="{{$kid_video->id==$video->id ? 'active':''}}">
                                    <a href="{{url('/kid-videos/'.$kid_video->id)}}"
                                        {{--onClick="document.getElementById('vid_frame').src='{{getYoutubeId($kid_video->video)}}'"--}}>
                            <span class="vid-thumb">

                                  @if($kid_video->photo)
                                    <img src="{{asset('images/video_kids/'.$kid_video->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif

                            </span>
                                        <div class="desc">{{$kid_video->title}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
