@extends('website.layouts.master')
@push('title')
    {{__('site.hadiths')}} | {{__('site.videos')}}
@endpush
@push('seo')
    <meta name="title" content="{{$hadith->first()?$hadith->first()->meta_title:''}}">
    <meta name="description" content="{!! strip_tags($hadith->first()?$hadith->first()->meta_description:'') !!}">
    <meta name="keywords" content="{{$hadith->first()?$hadith->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> الحديث النبوي / فيديوهات </h3>
                </div>

            </div>

            @if(count($hadith) >0)
                <ul class="list-videos posters">
                    @foreach($hadith as $index => $had)
                        <li>
                            <a href="{{url('/hadith-videos/'.$had->id)}}">

                                @if($had->photo)
                                    <img src="{{asset('images/hadiths/'.$had->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif
                                <i class="fa fa-play"></i>
                                <span>{{$had->name}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
                {{$hadith->links()}}
                <span> showing {{$hadith->links()->paginator->currentPage()}}  from {{$pages}} pages</span>
            @else
                <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
            @endif


        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')





@endpush
