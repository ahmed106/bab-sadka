@extends('website.layouts.master')
@push('title')
    {{__('site.hadiths')}} | {{__('site.videos')}} | {{$hadith->name}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">
            <div class="he-vid">
                <div class="vid-main-wrapper">
                    <!-- THE YOUTUBE PLAYER -->


                    <div class="vid-container">
                        @if($hadith->video_type=='url')
                            <iframe id="vid_frame" src="{{getYoutubeId($hadith->video)}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @else

                            <video autoplay width="100%" controls src="{{asset('uploads/hadiths/videos/'.$hadith->video)}}"></video>
                        @endif

                    </div>
                    <!-- THE PLAYLIST -->
                    <div class="vid-list-container">
                        <ol class="vid-list" id="scrollable">
                            @foreach($hadiths as $had)
                                <li class="{{$had->id==$hadith->id ? 'active':''}}">
                                    <a href="{{url('/hadith-videos/'.$had->id)}}"
                                        {{--onClick="document.getElementById('vid_frame').src='{{getYoutubeId($kid_video->video)}}'"--}}>
                            <span class="vid-thumb">

                                  @if($had->photo)
                                    <img src="{{asset('images/hadiths/'.$had->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif

                            </span>
                                        <div class="desc">{{$had->name}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
