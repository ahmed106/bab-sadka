<div id="sound-container-{{$model->id}}" class="footer_sound_container jp-audio top-player">
    <div class="player-nine">
        <div class="jp-type-single">
            <div class="jp-gui jp-interface">
                <div class="player-container-left">
                    <a id="prev-button" class="prev-button prev-button-disabled" title="السابق"></a>
                    <a class="jp-play jp_play_footer  " tabindex="2" title="تشغيل"></a>
                    <a class="jp-pause jp_pause_footer " tabindex="2" title="ايقاف"></a>
                    <a id="next-button" class="next-button next-button-disabled" title="التالي"></a>
                    <div class="jp-current-time" id="current-time">00:00</div>
                </div>
                <div class="player-container-middle">
                    <div class="jp-progress">
                        <div class="jp-seek-bar">
                            <div class="jp-play-bar">
                                <div class="bullet"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="player-container-right">
                    <div class="jp-duration" id="duration-time">00:00</div>
                    <a class="jp-mute" tabindex="1" title="صامت"></a>
                    <a class="jp-unmute" tabindex="1" title="غير صامت"></a>
                    <div class="jp-volume-bar" title="مستوى الصوت">
                        <div class="jp-volume-bar-value">
                            <div class="bullet"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="player-three">
        <div id="topbar-track-info">
            <div id="topbar-track-details">
                <div class=" d-none" id="loader">
                    <i class="fa fa-spin fa-spinner fa-1x text-center"></i>
                </div>
                <div class="topbar-song-name" id="sw-song-name">{{$model->name??$model->title}}</div>
                @if($model->reader)
                    <div class="topbar-song-name" id="sw-author-name">{{$model->reader->name}}</div>
                @endif

            </div>
        </div>
    </div>
</div>


<script>
    $('#sound-container-' + {{$model->id}}).jPlayer({
        play: function () {
            $('#sw-song-name').text($(this).data('surah_name'));
            $(this).jPlayer("pauseOthers", 0); // pause all players except this one.
        },
        pause: function () {

        },

        ready: function () {
            $('#sound-container-' + {{$model->id}}).jPlayer("setMedia", {
                title: "Bubble",
                m4a: "{{asset('uploads/'.$type.'/audios/')}}" + '/' + '{{$model->audio}}',
                oga: "{{asset('uploads/'.$type.'/audios/')}}" + '/' + '{{$model->audio}}'
            });
        },


        cssSelectorAncestor: "#sound-container-" + {{$model->id}},
        swfPath: "/js",
        supplied: "m4a, oga",
        useStateClassSkin: true,
        autoBlur: true,
        smoothPlayBar: true,
        keyEnabled: true,
        remainingDuration: true,
        toggleDuration: true
    });
</script>


