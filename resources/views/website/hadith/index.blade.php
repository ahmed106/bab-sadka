@extends('website.layouts.master')
@push('title')
    {{__('site.hadiths')}}
@endpush
@section('content')

    <div class="wrapper">
        <!-- Header -->
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
          
            <section class="cr-section islams-pillar-area">
                <div class="islams-pillars__thumb text-center">
                    <img class="icon-page" src="{{asset('assets/front')}}/assets/images/hadith.png" title="{{$settings->title}}" alt="{{$settings->title}}">
                </div>
                <div class="islams-pillars__content">
                    <h2>
                        عَنِ النَّبِيِّ ﷺ أَنَّهُ قَالَ: " نَضَّرَ اللَّهُ امْرَأً سَمِعَ مَقَالَتِيَ فَحَفِظَهَا فَأَدَّاهَا كَمَا سَمِعَهَا " . ففي هذا الحديث : الدعاء أو الإخبار من النبي صلى الله عليه وسلم بحصول النضرة لمن حفظ الحديث وبلغه كما حفظه , وأما معنى النضرة فهي الحسن والرونق .
                    </h2>
                </div>
                <ul class="btns-btm">
                    <li>
                        <a href="https://sunnah.one/" target="_blank" title="{{__('site.read')}}">
                            <img src="{{asset('assets/front')}}/assets/images/icons/read-icon.svg" title="{{__('site.read')}}" alt="{{__('site.read')}}">
                            قراءة
                        </a>
                    </li>
                    <li>
                        <a href="{{route('hadith.listen')}}" class="style1" title="{{__('site.listen')}}">
                            <img src="{{asset('assets/front')}}/assets/images/icons/listen-icon.svg" title="{{__('site.listen')}}" alt="{{__('site.listen')}}">
                            استماع
                        </a>
                    </li>
                    <li>
                        <a href="{{route('hadith.videos')}}" class="style2" title="{{__('site.videos')}}">
                            <img src="{{asset('assets/front')}}/assets/images/icons/vedio-icon.svg" title="{{__('site.videos')}}" alt="{{__('site.videos')}}">
                            فيديوهات
                        </a>
                    </li>
                </ul>
            </section><!-- //Pillar Of Islam -->
        </div>

    </div><!-- //Main wrapper -->
@endsection
