@extends('website.layouts.master')
@section('content')

    <div class="wrapper">
        <!-- Top Banner -->
        <div class="banner-area">
            <div class="banner banner-slide-active slide-animate-text">
                <!-- Single Banner -->
                <div class="banner__single fullscreen d-flex align-items-center" style="background: url({{asset('assets/front')}}/assets/images/bg/1.jpg);" data-black-overlay="4">
                    <video autoplay muted loop id="myVideo">
                        <source src="{{asset('assets/front')}}/assets/images/bg.mp4" type="video/mp4"/>
                    </video>
                </div><!-- //Single Banner -->
                <div class="banner__single fullscreen d-flex align-items-center" style="background: url({{asset('assets/front')}}/assets/images/bg/2.jpg);" data-black-overlay="4">
                    <video autoplay muted loop id="myVideo">
                        <source src="{{asset('assets/front')}}/assets/images/bg.mp4" type="video/mp4"/>
                    </video>
                </div><!-- //Single Banner -->
            </div>
        </div>

        <div class="content">
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>الحديث النبوي / استماع</h3>
                </div>
            </div>
            <ul class="list-hadith">
                <li class="sora-container">
                    <div class="player">
                        <div class="sora-top">
                            <div class="sora-titles">
                                <div class="sora-title">
                                    الصدق
                                </div>
                            </div>
                        </div>
                        <button>Play</button>
                        {{--                        <div class="player-controls">--}}
                        {{--                            <div id="sound-container" class="jp-jplayer"></div>--}}
                        {{--                            <div id="sound-container" class="jp-audio top-player">--}}
                        {{--                                <div class="player-nine">--}}
                        {{--                                    <div class="jp-type-single">--}}
                        {{--                                        <div class="jp-gui jp-interface">--}}
                        {{--                                            <div class="player-container-left">--}}
                        {{--                                                <a class="jp-play" tabindex="1" title="تشغيل"></a>--}}
                        {{--                                                <a class="jp-pause" tabindex="1" title="ايقاف"></a>--}}
                        {{--                                            </div>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                </li>
                <li class="sora-container">
                    <div class="player">
                        <div class="sora-top">
                            <div class="sora-titles">
                                <div class="sora-title">
                                    الصلاة
                                </div>
                            </div>
                        </div>
                        <div class="player-controls">
                            <button>Play</button>
                        </div>
                    </div>
                </li>

            </ul>

        </div>


        <div class="bottombar">
            <div class="bottombar-container">
                <div id="sound-player" class="jp-jplayer">
                    <img id="jp_poster_0">
                    <audio id="jp_audio_0" preload="metadata"></audio>
                </div>
                <div id="sound-container" class="jp-audio top-player">
                    <div class="player-nine">
                        <div class="jp-type-single">
                            <div class="jp-gui jp-interface">
                                <div class="player-container-left">
                                    <a id="prev-button" class="prev-button prev-button-disabled" title="السابق"></a>
                                    <a class="jp-play" tabindex="1" title="تشغيل"></a>
                                    <a class="jp-pause" tabindex="1" title="ايقاف"></a>
                                    <a id="next-button" class="next-button next-button-disabled" title="التالي"></a>
                                    <div class="jp-current-time" id="current-time">00:00</div>
                                </div>
                                <div class="player-container-middle">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar">
                                                <div class="bullet"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="player-container-right">
                                    <div class="jp-duration" id="duration-time">00:00</div>
                                    <a class="jp-mute" tabindex="1" title="صامت"></a>
                                    <a class="jp-unmute" tabindex="1" title="غير صامت"></a>
                                    <div class="jp-volume-bar" title="مستوى الصوت">
                                        <div class="jp-volume-bar-value">
                                            <div class="bullet"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="player-three">
                        <div id="topbar-track-info">
                            <div id="topbar-track-details">
                                <div class="topbar-song-name" id="sw-song-name">الصدق</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection

<script>

</script>
@push('js')
    <script src="{{asset('assets/front/assets/js/jPlayer/jquery.jplayer.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sound-container").jPlayer({
                ready: function () {
                    $(this).jPlayer("setMedia", {
                        title: "Bubble",
                        m4a: "{{asset('assets/front')}}/assets/quran.mp3",
                        oga: "{{asset('assets/front')}}/assets/quran.mp3"
                    });
                },
                cssSelectorAncestor: "#sound-container",
                swfPath: "/js",
                supplied: "m4a, oga",
                useStateClassSkin: true,
                autoBlur: false,
                smoothPlayBar: true,
                keyEnabled: true,
                remainingDuration: true,
                toggleDuration: true
            });
        });
    </script>
@endpush
