@extends('website.layouts.master')
@push('title')
{{__('site.about')}}
@endpush
@section('content')

<!-- Main wrapper -->
<div class="wrapper">
    <!-- Top Banner -->
    @include('website.includes.banner_area')

    <div class="content">
        <div class="top-menu checkout-form">
            <div class="right">
                <h3>{{__('site.about')}}</h3>
            </div>
        </div>
        <div class="about">
            <img src="{{asset('assets/front/assets')}}/images/logo/logo.svg" alt="{{$settings->website_name}}"
                title="{{$settings->website_name}}">
            <!-- <p>
                    {!!$settings->website_description !!}
                </p> -->
            <h2>"باب صدقة"</h2>
            <div style="margin: 0px; padding: 0px;">&nbsp;صدقة جارية لكل من توفى من أحبابك أو كان على قيد الحياة فالصدقة
                تجوز على الحي وعلى المتوفي ، من خلال واجهة إلكترونية مخصصة بالإسم الذي اخترته .</div>
            <div style="margin: 0px; padding: 0px;"><br style="margin: 0px; padding: 0px;"></div>
            <h2>على ماذا يحتوي موقع باب صدقة ؟</h2>
            <div style="margin: 0px; padding: 0px;">​به جميع ما ينفع المسلمين مثل : مواقيت الصلاة ورفع الأذان في الموقع،
                القرآن كريم، والحديث النبوي، وقصص الأنبياء، وحصن المسلم، وفيديوهات خاصة بالأطفال، ومكتبات إسلامية ،
                ومسبحة إلكترونية وقسم خاص بالدعاء للمتوفي</div>
            <div style="margin: 0px; padding: 0px;"><br></div>
            <h2 style="margin: 0px; padding: 0px;"><span style="margin: 0px; padding: 0px;"></span></h2>
            <h2>لمن تكون الصدقة ؟&nbsp;</h2>
            <div style="margin: 0px; padding: 0px;">لك، الوالدين، الأحباء، الأقرباء، لسائر المسلمين الأحياء منهم
                والأموات.</div>
            <div style="margin: 0px; padding: 0px;">تستطيع مشاركتها مع أصدقائك وأهلك لتدر عليك وعلى من أوقفتها لهم أنهار
                بإذن الله من الحسنات ، فالصدقة حياة لما بعد الممات .</div>
            <div style="margin: 0px; padding: 0px;"><br></div>
            <h2 style="margin: 0px; padding: 0px;"><span style="margin: 0px; padding: 0px;"></span></h2>
            <h2>رحلة موقع "باب صدقة" داخل شركة كود لوب :</h2>
            <div style="margin: 0px; padding: 0px;"><b><span style="margin: 0px; padding: 0px;">*</span>&nbsp;<span
                        style="margin: 0px; padding: 0px;">بداية من كيفية اختيار الاسم :</span></b>&nbsp;استنادًا إلى أن
                للجنة لها 8 أبواب، منهم باب الصدقة، يدخل منه المتصدقون .</div>
            <div style="margin: 0px; padding: 0px;"><br></div>
            <div style="margin: 0px; padding: 0px;"><span style="margin: 0px; padding: 0px;"><b>* إلى تصميم الهوية كاملة
                        :</b></span>&nbsp;بألوان تناسب طبيعة المشروع وهدفه.</div>
            <div style="margin: 0px; padding: 0px;"><br></div>
            <div style="margin: 0px; padding: 0px;"><b><span style="margin: 0px; padding: 0px;">* مرحلة
                        التخطيط&nbsp;</span>:</b>بدأت بأفضل فريق وبخبرة أكثر من 7 سنوات، لكل خصائص الموقع بأن يصبح لكل
                موقع واجهة خاصة باسمه وصورة الشخص -اختياري- ويمكن مشاركتها للأهل والأصدقاء بكافة .</div>
            <div style="margin: 0px; padding: 0px;"><br></div>
            <div style="margin: 0px; padding: 0px;"><b><span style="margin: 0px; padding: 0px;">*</span>&nbsp;<span
                        style="margin: 0px; padding: 0px;">مرحلة التصميم :</span></b>&nbsp;فريق تصميم متخصص صمم الموقع
                بواجهة مستخدم سهلة وواضحة.</div>
            <div style="margin: 0px; padding: 0px;"><br></div>
            <div style="margin: 0px; padding: 0px;"><b><span style="margin: 0px; padding: 0px;">*</span>&nbsp;<span
                        style="margin: 0px; padding: 0px;">مرحلة التنفيذ البرمجي :</span></b>&nbsp;فريق المطورين قاموا
                بتنفيذ التصور بأفضل برمجة بواجهة سريعة وخفيفة لكل انواع الأجهزة الكمبيوتر والأندرويد والأيفون .<br><br>
                <div style="margin: 0px; padding: 0px;">
                    <h2>سياسات موقع باب صدقة :</h2>
                </div>
                <div style="margin: 0px; padding: 0px;">1.<span style="white-space:pre"> </span>الإلتزامات والواجبات
                    المعدة أدناه والتي تحكم سياسة إعداد وإستخدام الموقع معدة بضوء القوانين والإرشادات الخاصة بسلامة
                    إستخدام المواقع.</div>
                <div style="margin: 0px; padding: 0px;">2.<span style="white-space:pre"> </span>جميع المحتويات بالموقع
                    من تسجيلات قرآنية أو آذكار أو أدعية أو حديث أو حصن المسلم محتويات مسموح بإعاده الإستخدام رخصة المشاع
                    الإبداعي على&nbsp;YouTube<br>&nbsp;<a href="https://support.google.com/youtube/answer/2797468"
                        target="_blank">shorturl.at/gyQYZ</a>&nbsp;.</div>
                <div style="margin: 0px; padding: 0px;">3.<span style="white-space:pre"> </span>الكتب الواردة في المكتبه
                    الدينية : تم جلبها من موقع books-library.net.</div>
                <div style="margin: 0px; padding: 0px;">وجميع الحقوق محفوظة لدى دور النشر والمؤلفين والمكتبة&nbsp; لا
                    تنتهك حقوق النشر وحقوق التأليف والملكيّة والكتب المنشورة ملك لأصحابها أو ناشريها ولا تعبّر عن وجهة
                    نظر الموقع ، ونبذل قصارى الجهد لمراجعة الكتب قبل نشرها ..للتبليغ عن كتاب محمي بحقوق نشر او مخالف
                    للقوانين و الأعراف و في حالة الإعتراض على نشر الكتاب الرجاء التواصل معنا من خلال
                    codelop424@gmail.com أو&nbsp;<a href="https://babsadaka.com/contact-us" target="_blank"
                        style=";">تواصل معنا </a>على الفور من خلال الموقع .</div>
                <div style="margin: 0px; padding: 0px;">4.<span style="white-space:pre"> </span>ينص الإعلان العالمي
                    لحقوق الإنسان على أنه "لكل شخص حق المشاركة الحرة في حياة المجتمع الثقافية، وفي الاستمتاع بالفنون،
                    والإسهام في التقدم العلمي وفي الفوائد التي تنجم عنه. لكل شخص حق في حماية المصالح المعنوية والمادية
                    المترتِّبة على أيِّ إنتاج علمي أو أدبي أو فنِّي من صنعه".</div>
                <div style="margin: 0px; padding: 0px;">5.<span style="white-space:pre"> </span>موقع باب صدقة لا يشمل
                    إعلانات بنرات أو جوجل أدوردز أو غيرها.</div>
                <div style="margin: 0px; padding: 0px;">6.<span style="white-space:pre"> </span>يتم ازالة أي محتويات من
                    الموقع فى حاله التقدم بطلب بإزالة محتوى ونلتزم بإضافة محتوى آخر.</div>
                <div style="margin: 0px; padding: 0px;">7.<span style="white-space:pre"> </span>مده الإشتراك سنة من
                    تاريخ التعاقد قابل للتجديد، أما بالنسبة لأول 5000 مشترك سيكون الإشتراك مقابل 100 جنيه لمدى الحياة.
                </div>
                <div style="margin: 0px; padding: 0px;">8.<span style="white-space:pre"> </span>في حالة وجود أي مشكلة
                    فنية يتم ارسال رسالة بالمشكلة وصورة من الموقع توضح المشكلة إلى codelop424@gmail.com وسيتم الرد والحل
                    من 24 إلى 48 ساعه عمل .</div>
                <div style="margin: 0px; padding: 0px;">9.<span style="white-space:pre"> </span>أي تحديثات على الموقع أو
                    الصفحات المرتبطة تتم من الشركة بشكل دوري ودون تنبيه للمستخدمين وبدون تكلفة.</div>
                <div style="margin: 0px; padding: 0px;">10.<span style="white-space:pre"> </span>التحدث مع خدمة العملاء
                    أو الدعم الفني من خلال تواصل معنا في الموقع أو التواصل معنا على صفحة الفيسبوك.</div>
                <div style="margin: 0px; padding: 0px;">11.<span style="white-space:pre"> </span>يتم فورًا حذف أي مادة
                    علمية أو كتاب أو أي محتوى يتضح انه ينتهك حقوق النشر ونلتزم بإضافة محتوى آخر.</div>
            </div>
        </div>
    </div>

</div><!-- //Main wrapper -->
@endsection
