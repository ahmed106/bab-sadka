@extends('website.layouts.master')
@push('title')
    {{__('site.share_app')}}
@endpush
@section('content')

    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
            <div class="share-div">
                <h3 class="title-page">مشاركة الموقع</h3>
                <div class="addthis_inline_share_toolbox" data-url="https://babsadaka.com" data-title="باب صدقة"></div>
            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection

@push('js')

<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61bef336ede87099"></script>


@endpush


