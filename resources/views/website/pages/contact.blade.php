@extends('website.layouts.master')
@push('title')
    {{__('site.contact_us')}}
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>تواصل معنا</h3>
                </div>
            </div>
            <div class="contact">
                <img src="{{asset('assets/front/assets')}}/images/logo/logo.svg" alt="">
                <form autocomplete="off" action="{{route('website.contact')}}" method="post">
                    @csrf

                    <div class="inputs">
                        <div class="input-div">
                            <input autocomplete="off" type="text" name="name" placeholder="الإسم بالكامل">
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="input-div">
                            <input autocomplete="off" type="email" name="email" placeholder="البريد الالكتروني">
                            <i class="fa fa-envelope"></i>
                        </div>

                        <div class="input-div">
                            <input autocomplete="off" type="numper" name="phone" placeholder="رقم الجوال">
                            <i class="fa fa-phone"></i>
                        </div>

                        <textarea autocomplete="off" name="body" id="" rows="4" placeholder="اكتب اقتراحك أو شكواك هنا"></textarea>
                        <button class="btn-send">ارسال</button>
                    </div>
                </form>
            </div>
            <ul class="social-div">
                <li>
                    <a href="https://wa.me/966558706532" target="_blank">
                        <img src="{{asset('assets/front/assets')}}/images/whatsapp.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/%D8%A8%D8%A7%D8%A8-%D8%B5%D8%AF%D9%82%D8%A9-108939548433772" target="_blank">
                        <img src="{{asset('assets/front/assets')}}/images/facebook.png" alt="">
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/messages/t/108939548433772" target="_blank">
                        <img src="{{asset('assets/front/assets')}}/images/messenger.png" alt="">
                    </a>
                </li>
            </ul>

        </div>

    </div><!-- //Main wrapper -->
    </div>
@endsection
