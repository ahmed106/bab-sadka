@extends('website.layouts.master')
@push('title')
    {{__('site.pray')}} | {{__('site.videos')}} | {{$pray->title}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">
            <div class="he-vid">
                <div class="vid-main-wrapper">
                    <!-- THE YOUTUBE PLAYER -->

                    <div class="vid-container">
                        @if($pray->video_type=='url')
                            <iframe id="vid_frame" src="{{getYoutubeId($pray->video)}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @else
                            <video autoplay width="100%" controls src="{{asset('uploads/prays/videos/'.$pray->video)}}"></video>
                        @endif


                    </div>
                    <!-- THE PLAYLIST -->
                    <div class="vid-list-container">
                        <ol class="vid-list" id="scrollable">
                            @foreach($prays as $pr)
                                <li class="{{$pr->id==$pray->id ? 'active':''}}">
                                    <a href="{{url('/pray-videos/'.$pr->id)}}"
                                        {{--onClick="document.getElementById('vid_frame').src='{{getYoutubeId($kid_video->video)}}'"--}}>
                            <span class="vid-thumb">

                                  @if($pr->photo)
                                    <img src="{{asset('images/prays/'.$pr->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif


                            </span>
                                        <div class="desc">{{$pr->title}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
