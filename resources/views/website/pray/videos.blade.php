@extends('website.layouts.master')
@push('title')
    {{__('site.pray')}} | {{__('site.videos')}}
@endpush
@push('seo')
    <meta name="title" content="{{$prays->first() ? $prays->first()->meta_title:''}}">
    <meta name="description" content="{!! $prays->first() ? strip_tags($prays->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$prays->first() ? $prays->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> الدعاء للمتوفي / فيديوهات </h3>
                </div>

            </div>

            @if(count($prays) >0)

                <ul class="list-videos posters">
                    @foreach($prays as $index => $pray)
                        <li>
                            <a href="{{url('/pray-videos/'.$pray->id)}}">
                                @if($pray->photo)
                                    <img src="{{asset('images/prays/'.$pray->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif

                                <i class="fa fa-play"></i>
                                <span>{{$pray->title}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
                {{$prays->links()}}
                <span> showing {{$prays->links()->paginator->currentPage()}}  from {{$pages}} pages</span>

            @else
                <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
            @endif


        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')




@endpush
