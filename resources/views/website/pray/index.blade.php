@extends('website.layouts.master')
@push('title')
    {{__('site.pray')}}
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Header -->
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
          
            <section class="cr-section islams-pillar-area">
                <div class="islams-pillars__thumb text-center">
                    <img class="icon-page" src="{{asset('assets/front')}}/assets/images/pray.png" alt="">
                </div>
                <div class="islams-pillars__content">
                    <h2>
                        عن أبي هريرة رضي الله عنه أن رسول الله ﷺ قال: إذا :مات ابن آدم انقطع عمله إلا من ثلاث .صدقة جارية، أو علم ينتفع به، أو ولد صالح يدعو له
                    </h2>
                </div>
                <ul class="btns-btm">
                    <li>
                        <a href="{{url('pray/read')}}">
                            <img src="{{asset('assets/front')}}/assets/images/icons/read-icon.svg" alt="{{__('site.read')}}" title="{{__('site.read')}}">
                            قراءة
                        </a>
                    </li>
                    <li>
                        <a href="{{url('pray/listen')}}" class="style1">
                            <img src="{{asset('assets/front')}}/assets/images/icons/listen-icon.svg" alt="{{__('site.listen')}}" title="{{__('site.listen')}}">
                            استماع
                        </a>
                    </li>
                    <li>
                        <a href="{{url('pray/videos')}}" class="style2">
                            <img src="{{asset('assets/front')}}/assets/images/icons/vedio-icon.svg" alt="{{__('site.videos')}}" title="{{__('site.videos')}}">
                            فيديوهات
                        </a>
                    </li>
                </ul>
            </section><!-- //Pillar Of Islam -->
        </div>

    </div><!-- //Main wrapper -->
@endsection
