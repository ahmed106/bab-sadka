@extends('website.layouts.master')
@push('title')
    {{__('site.pray')}} | {{__('site.read')}}
@endpush
@push('seo')
    <meta name="title" content="{{$prays->first() ? $prays->first()->meta_title:''}}">
    <meta name="description" content="{!! $prays->first() ? strip_tags($prays->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$prays->first() ? $prays->first()->meta_keywords:''}}">
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
          
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> الدعاء للمتوفي / قراءة</h3>
                </div>
            </div>
            <ul class="read-praying">
                @foreach($prays as $pray)
                    <li>
                        <p>{!! $pray->pray !!}</p>
                    </li>
                @endforeach


            </ul>
        </div>

    </div><!-- //Main wrapper -->
@endsection
