@extends('website.layouts.master')
@push('title')
    {{__('site.hesn_elmuslim')}} | {{__('site.videos')}} | {{$hesn->title}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">
            <div class="he-vid">
                <div class="vid-main-wrapper">
                    <!-- THE YOUTUBE PLAYER -->

                    <div class="vid-container">
                        @if($hesn->video_type=='url')
                            <iframe id="vid_frame" src="{{getYoutubeId($hesn->video)}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @else
                            <video autoplay width="100%" controls src="{{asset('uploads/hesn/videos/'.$hesn->video)}}"></video>
                        @endif


                    </div>
                    <!-- THE PLAYLIST -->


                    <div class="vid-list-container">
                        <ol class="vid-list" id="scrollable">
                            @foreach($hesns as $hesn_elmuslim)
                                <li class="{{$hesn_elmuslim->id==$hesn->id ? 'active':''}}">
                                    <a href="{{url('/hesn-elmuslim-videos/'.$hesn_elmuslim->id)}}"
                                        {{--onClick="document.getElementById('vid_frame').src='{{getYoutubeId($kid_video->video)}}'"--}}>
                            <span class="vid-thumb">

                                   @if($hesn_elmuslim->photo)
                                    <img src="{{asset('images/hesns/'.$hesn_elmuslim->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif

                            </span>
                                        <div class="desc">{{$hesn_elmuslim->title}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
