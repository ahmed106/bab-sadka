@extends('website.layouts.master')
@push('title')
    {{__('site.hesn_elmuslim')}}
@endpush
@section('content')
    <div class="wrapper">
        <!-- Header -->
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">

            <section class="cr-section islams-pillar-area">
                <div class="islams-pillars__thumb text-center">
                    <img class="icon-page" src="{{asset('assets/front/')}}/assets/images/hesn.png" alt="">
                </div>
                <div class="islams-pillars__content">
                    <h2>
                        حصن المسلم كتاب أدعية يحتوي على أذكار النبي محمد صلى الله عليه وسلم في مختلف مواضع الحياة اليومية وهو من أكثر الكتب الإسلامية انتشارا .لسهولة أسلوبه والتزامه بالصحيح من الأحاديث
                    </h2>
                </div>
                <ul class="btns-btm">
                    <li>
                        <a href="https://www.hisnmuslim.com/" target="_blank">
                            <img src="{{asset('assets/front/')}}/assets/images/icons/read-icon.svg" alt="{{__('site.read')}}" title="{{__('site.read')}}">
                            قراءة
                        </a>
                    </li>
                    <li>
                        <a href="{{url('hesn-elmuslim/listen')}}" class="style1">
                            <img src="{{asset('assets/front/')}}/assets/images/icons/listen-icon.svg" alt="{{__('site.listen')}}" title="{{__('site.listen')}}">
                            استماع
                        </a>
                    </li>
                    <li>
                        <a href="{{url('hesn-elmuslim/videos')}}" class="style2">
                            <img src="{{asset('assets/front/')}}/assets/images/icons/vedio-icon.svg" alt="{{__('site.videos')}}" title="{{__('site.videos')}}">
                            فيديوهات
                        </a>
                    </li>
                </ul>
            </section><!-- //Pillar Of Islam -->
        </div>

    </div><!-- //Main wrapper -->
@endsection
