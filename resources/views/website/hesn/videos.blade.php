@extends('website.layouts.master')
@push('title')
    {{__('site.hesn_elmuslim')}} | {{__('site.videos')}}
@endpush

@push('seo')
    <meta name="title" content="{{$hesns->first() ? $hesns->first()->meta_title:''}}">
    <meta name="description" content="{!! $hesns->first() ? strip_tags($hesns->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$hesns->first() ? $hesns->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> حصن المسلم/ فيديوهات </h3>
                </div>

            </div>

            @if(count($hesns) >0)


                <ul class="list-videos posters">
                    @foreach($hesns as $index => $hesn)
                        <li>
                            <a href="{{url('/hesn-elmuslim-videos/'.$hesn->id)}}">
                                @if($hesn->photo)
                                    <img src="{{asset('images/hesns/'.$hesn->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif


                                <i class="fa fa-play"></i>
                                <span>{{$hesn->title}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
                {{$hesns->links()}}

                <span> showing {{$hesns->links()->paginator->currentPage()}}  from {{$pages}} pages</span>


            @else
                <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
            @endif


        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')




@endpush
