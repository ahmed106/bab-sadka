@if(count($books) >0)
    <ul class="books">

        @foreach($books as $book)
            <li>
                <a href="{{route('libraries.readBook',$book->id)}}">
                    <img src="{{asset('images/libraries/'.$book->photo)}}" alt="{{$book->name}}" title="{{$book->name}}">
                    <div class="name-book">
                        <h3>{{$book->name}}</h3>
                        <p>
                            <i class="fa fa-file-pdf-o"></i>
                            قراءة وتحميل
                        </p>
                    </div>
                </a>
            </li>
        @endforeach
    </ul>
@else
    <div class="alert alert-danger text-center ">لأسف لايوجد أي بيانات</div>
@endif
