@extends('website.layouts.master')
@push('title')
    {{__('site.libraries')}}
@endpush
@push('seo')
    <meta name="title" content="{{$categories->first() ? $categories->first()->meta_title:''}}">
    <meta name="description" content="{!! $categories->first() ? strip_tags($categories->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$categories->first() ? $categories->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
           
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>المكتبات</h3>
                </div>
            </div>
            <div class="img-library">
                <img src="{{asset('assets/front')}}/assets/images/library.png" alt="{{__('site.libraries')}}" title="{{__('site.libraries')}}">
            </div>
            <ul class="libraries">
                @foreach($categories as $category)
                    <li>
                        <a href="{{route('libraries.showCategory',$category->id)}}">
                            <img src="{{asset('images/library_categories/'.$category->photo)}}" alt="{{$category->name}}" title="{{$category->name}}">
                            <p>{{$category->name}}</p>
                        </a>
                    </li>
                @endforeach


            </ul>
        </div>

    </div><!-- //Main wrapper -->
@endsection
