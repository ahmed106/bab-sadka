@extends('website.layouts.master')
@push('title')
    {{__('site.libraries')}} | {{$books->first() ? $books->first()->category->name:''}}
@endpush
@push('seo')
    <meta name="title" content="{{$books->first() ? $books->first()->meta_title:''}}">
    <meta name="description" content="{!! $books->first() ? strip_tags($books->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$books->first() ? $books->first()->meta_keywords:''}}">
@endpush
@section('content')

    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>المكتبات / {{$books->first() ?$books->first()->category->name:''}}</h3>
                </div>
            </div>
            <div class="top-menu checkout-form tob-mb">
                <div class="left">
                    <div class="input-div">
                        <input type="text" name="book_name" placeholder="ابحث باسم الكتاب">
                        <input type="hidden" name="category_id" value="{{$books->first() ?$books->first()->category->id:''}}">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
            </div>
            <div id="books">
                @include('website.libraries.search')
            </div>

        </div>

    </div><!-- //Main wrapper -->
@endsection



@push('js')
    <script>
        $('input[name="book_name"]').on('keyup', function () {
            let name = $(this).val(),
                cat_id = $('input[name="category_id"]').val();
            $.ajax({
                type: 'get',
                url: '{{route('libraries.books_search')}}',
                data: {
                    book_name: name,
                    cat_id: cat_id
                },
                success: function (response) {
                    $('#books').html(response.view)

                }
            })
        })
    </script>
@endpush
