@extends('website.layouts.master')
@push('title')
    {{__('site.libraries')}} | {{__('site.read')}} | {{$book->name}}
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>{{$book->name}}</h3>
                </div>
            </div>
            <div class="solid-container"></div>
        </div>

    </div><!-- //Main wrapper -->
@endsection



@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".solid-container").flipBook({
                pdfUrl: "{{asset('uploads/libraries/pdf/'.$book->pdf)}}",
                rightToLeft: true,
                btnSize: 18,
                backgroundColor: "#0a0c06"
            });

        })
    </script>
@endpush
