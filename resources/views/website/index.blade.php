@extends('website.layouts.master')
@push('title')
    {{__('site.home')}}
@endpush
@section('content')
    <div class="wrapper">

        {{--        <audio loop="loop" id="audio" src="{{asset('sounds.mpeg')}}"></audio>--}}

        <div class="d-none">
            <audio id="audio" autoplay controls src="{{asset('sounds.mpeg')}}"></audio>
        </div>

        <!-- Header -->
        <header class="header-logo">
            <div class="container">
                <img class="top-img" src="{{asset('assets/front/')}}/assets/images/bg/top_img.png" alt="">
                <img class="logo" src="{{asset('assets/front/')}}/assets/images/bg/text-img.png" alt="">
            </div>
        </header><!-- //Header -->
        <!-- Top Banner -->
        <div class="banner-area home-page">
            <div class="banner banner-slide-active slide-animate-text" dir="rtl">
                <!-- Single Banner -->
                <div class="banner__single fullscreen d-flex align-items-center"
                     style="background: url({{asset('assets/front/')}}/assets/images/bg/1.jpg);" data-black-overlay="3">
                    <video autoplay muted loop id="myVideo">
                        <source src="{{asset('assets/front/')}}/assets/images/bg.mp4" type="video/mp4"/>
                    </video>
                </div>
                <div class="banner__single fullscreen d-flex align-items-center"
                     style="background: url({{asset('assets/front/')}}/assets/images/bg/2.jpg);" data-black-overlay="4">
                    <video autoplay muted loop id="myVideo">
                        <source src="{{asset('assets/front/')}}/assets/images/bg.mp4" type="video/mp4"/>
                    </video>
                </div>
            </div>
        </div>

        <div class="custom-size">
            <div class="banner__content text-center">
                <div class="banner__content text-center custom-text">
                    <h3>عن أبي هريرة رضي الله عنه أن رسول الله ﷺ قال:</h3>
                    <h1 id="dead-name"> إذا مات ابن آدم انقطع عمله إلا من ثلاث: صدقة جارية ، أو علم ينتفع به،
                        <br>
                        أو ولد صالح يدعو له، رواه مسلم</h1>
                </div>
            </div>
            <ul class="menu-home">
                <li>
                    <a title="{{__('site.el_quran_el_karem')}}"
                       href="{{url('quran?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/quran-icon.svg"
                             alt="{{__('site.el_quran_el_karem')}}" title="{{__('site.el_quran_el_karem')}}">
                        <span>القرآن الكريم</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.hadiths')}}" href="{{url('/hadith?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/hadeth-icon.svg"
                             alt="{{__('site.hadiths')}}" title="{{__('site.hadiths')}}">
                        <span>الحديث النبوي</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.prophet_stories')}}"
                       href="{{url('/prophets-stories?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/qesas-icon.svg"
                             alt="{{__('site.prophet_stories')}}" title="{{__('site.prophet_stories')}}">
                        <span>قصص الأنبياء</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.hesn_elmuslim')}}"
                       href="{{url('/hesn-elmuslim?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/hesn-icon.svg"
                             alt="{{__('site.hesn_elmuslim')}}" title="{{__('site.hesn_elmuslim')}}">
                        <span>حصن المسلم</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.arboaon_nwawya')}}"
                       href="{{url('/arboon-nwawia?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/arba3on-icon.svg"
                             alt="{{__('site.arboaon_nwawya')}}" title="{{__('site.arboaon_nwawya')}}">
                        <span>الأربعون النووية</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.kid_videos')}}"
                       href="{{url('/kid-videos?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/atfal-icon.svg"
                             alt="{{__('site.kid_videos')}}" title="{{__('site.kid_videos')}}">
                        <span>فيديوهات أطفال</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.masbaha')}}"
                       href="{{url('/masbaha?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/mesbaha-icon.svg"
                             alt="{{__('site.masbaha')}}" title="{{__('site.masbaha')}}">
                        <span>المسبحة</span>
                    </a>
                </li>
                <li>
                    <a title="{{__('site.libraries')}}"
                       href="{{url('/libraries?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/maktabat-icon.svg"
                             alt="{{__('site.libraries')}}" title="{{__('site.libraries')}}">
                        <span>المكتبات</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/pray?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/doaa-icon.svg" alt="">
                        <span>الدعاء للمتوفي</span>
                    </a>
                </li>
                {{--                <li>--}}
                {{--                    <a title="{{__('site.add_orphan')}}"--}}
                {{--                       href="{{url('add-orphan?dead='.Session::get('user.dead'))}}">--}}
                {{--                        <img src="{{asset('assets/front/')}}/assets/images/icons/yatem-icon.svg"--}}
                {{--                             alt="{{__('site.add_orphan')}}" title="{{__('site.add_orphan')}}">--}}
                {{--                        <span>إضافة يتيم</span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
                <li>
                    <a title="{{__('site.add_dead')}}"
                       href="{{url('add-dead?dead='.Session::get('user.dead'))}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/add.svg"
                             alt="{{__('site.add_dead')}}" title="{{__('site.add_dead')}}">
                        <span>إنشاء صدقه جاريه</span>
                    </a>
                </li>
            </ul>
            <div class="swiper  moaket" dir="rtl" id="pray_times">

            </div>
        </div>


    </div>
    <!-- //Main wrapper -->
    <form id="modal_form" action="{{url('/settings')}}" method="post">
        @csrf
	<input type="hidden" name="ajax_type" value="ajax">
        <div class="modal fade modal-index" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="settings">
                        <img src="{{asset('assets/front/')}}/assets/images/logo/logo.svg" alt="">
                        <div class="rosary-select checkout-form">
                            <div class="select-div">
                                <label for="">اختر اسم الدولة</label>
                                <select class="country_id select-modal" name="country">
                                    <option value="">اختر اسم الدولة</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="select-div">
                                <label for="">اختر اسم المحافظة</label>
                                <select class="city_id select-modal" name="city">
                                    <option value="">اختر اسم المدينه</option>
                                </select>
                            </div>
                            <div class="select-div">
                                <label for="">اختر اسم المتوفى</label>
                                <select name="dead-name" class="select-modal deads">
                                    <option value="">اختر اسم المتوفى</option>
                                    {{--                                    @foreach($deads as $dead)--}}
                                    {{--                                        <option value="{{$dead->name}}">{{$dead->name}}</option>--}}
                                    {{--                                    @endforeach--}}
                                </select>
                            </div>
                            <div class="flex-btns">
                                <button id="sumit_modal" type="submit" class="btn-ok">موافق</button>
                                <a href="{{url('/',['skip'=>1])}}" data-bs-dismiss="modal" class="btn-ok btn-skip">تخطي</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>

    <div>


    </div>
@endsection
@push('js')

    <script>


        $(function () {
            $('#play_sound').removeClass('d-none');
            $('#audio').get(0).play();


        })

        $('#modal_form').on('submit', function (e) {
            e.preventDefault();
            let data = $(this).serialize(),
                url = $(this).attr('action');
            $('.modal').modal('hide');


            $.ajax({
                type: 'post',
                url: url,
		dataType:"json",
                data: data,
                success: function (response) {
                    console.log(response.data.id)
                    window.location.href = '/D/' + response.data.id;
                },
                error: function (xhr) {
                    Toast.fire(xhr.responseJSON.errors);

                }
            })

        });


        $('.country_id').on('change', function () {
            let country_id = $(this).val();
            $('.city_id').empty();
            $.ajax({
                type: "get",
                url: '{{route('website.getCity')}}',
                data: {
                    country_id: country_id
                },
                success: function (res) {
                    let options = `<option value="">إختر المحافظه</option>`
                    res.data.map((res) => {
                        options += `<option value="${res.id}">${res.name}</option>`
                    });
                    $('.city_id').append(options)
                },

            });
        });

        $('.city_id').on('change', function () {
            let city_id = $(this).val();
            $('.deads').empty();
            $.ajax({
                type: "get",
                url: '{{route('website.getDeads')}}',
                data: {
                    city_id: city_id
                },
                success: function (res) {
                    let deads = `<option value="">إختر اسم المتوفي </option>`
                    res.data.map((res) => {
                        deads += `<option value="${res.name}">${res.name}</option>`


                    });
                    $('.deads').append(deads)
                },

            });
        });


        $('document').ready(function () {
            $('.modal-index').modal('show');
        });


        $(document).on('click', '#fire_event', function (e) {
            e.preventDefault();
            let url = $(this).attr('href');
            $.ajax({
                type: 'get',
                url: url,
            })
        });


    </script>

    @once
        <script>
            let timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            $('#pray_times').html('');
            $.ajax({
                type: 'get',
                url: '{{route('website.getPrayTimes')}}',
                data: {
                    timezone: timezone
                },
                success: function (res) {

                    $('#pray_times').append(res.data)

                }
            });


        </script>
    @endonce

@endpush
