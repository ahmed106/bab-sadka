@extends('website.layouts.master')
@push('title')
    {{__('site.arboaon_nwawya')}} | {{__('site.read')}}
@endpush
@push('seo')
    <meta name="title" content="{{$forty_nwawia->first() ? $forty_nwawia->first()->meta_title:''}}">
    <meta name="description" content="{!! $forty_nwawia->first() ? strip_tags($forty_nwawia->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$forty_nwawia->first() ? $forty_nwawia->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
          
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>الاربعون النووية / قراءة</h3>
                </div>
            </div>
            <div class="tabs-forty">
                <ul class="nav nav-tabs list-forty" id="myTab" role="tablist">
                    @foreach($forty_nwawia as $nwawia)
                        <li class="nav-item" role="presentation">
                            <button class="nav-link {{$loop->first ? 'active':''}}" id="tab-{{$nwawia->id}}" data-bs-toggle="tab" data-bs-target="#content-{{$nwawia->id}}" type="button" role="tab" aria-controls="content-{{$nwawia->id}}" aria-selected="true">
                                <!-- <span>{{substr($nwawia->title,0,12)}}</span> -->
                                <p>{{$nwawia->title}}</p>
                            </button>
                        </li>
                    @endforeach


                </ul>

                <div class="tab-content">
                    @foreach($forty_nwawia as $nwawia)
                        <div class="tab-pane  {{$loop->first?'active show fade':''}}" id="content-{{$nwawia->id}}" role="tabpanel" aria-labelledby="tab-{{$nwawia->id}}">
                            <div class="text-hadith">
                                <h3>{{$nwawia->title}}</h3>
                                <p>
                                    {!! $nwawia->content !!}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection
