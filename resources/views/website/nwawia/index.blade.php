@extends('website.layouts.master')
@push('title')
    {{__('site.arboaon_nwawya')}}
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Header -->
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
          
            <section class="cr-section islams-pillar-area">
                <div class="islams-pillars__thumb text-center">
                    <img class="icon-page" src="{{asset('assets/front/')}}/assets/images/forty.png" alt="">
                </div>
                <div class="islams-pillars__content">
                    <h2>
                        الأربعون النووية هي مؤلف يحتوي على أربعين حديثاً نبويًّا شريفًا، جمعها: الإمام النووي الذي التزم في جمعها أن تكون صحيحة، وحذف أسانيدها ليسهل حفظها
                    </h2>
                </div>
                <ul class="btns-btm">
                    <li>
                        <a href="{{url('arboon-nwawia/read')}}">
                            <img src="{{asset('assets/front/')}}/assets/images/icons/read-icon.svg" alt="{{__('site.read')}}" title="{{__('site.read')}}">
                            قراءة
                        </a>
                    </li>
                    <li>
                        <a href="{{url('arboon-nwawia/listen')}}" class="style1">
                            <img src="{{asset('assets/front/')}}/assets/images/icons/listen-icon.svg" alt="{{__('site.listen')}}" title="{{__('site.listen')}}">
                            استماع
                        </a>
                    </li>
                    <li>
                        <a href="{{url('arboon-nwawia/videos')}}" class="style2">
                            <img src="{{asset('assets/front/')}}/assets/images/icons/vedio-icon.svg" alt="{{__('site.videos')}}" title="{{__('site.videos')}}">
                            فيديوهات
                        </a>
                    </li>
                </ul>
            </section><!-- //Pillar Of Islam -->
        </div>

    </div><!-- //Main wrapper -->
@endsection
