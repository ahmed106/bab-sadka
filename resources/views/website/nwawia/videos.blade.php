@extends('website.layouts.master')
@push('title')
    {{__('site.arboaon_nwawya')}} | {{__('site.videos')}}
@endpush
@push('seo')
    <meta name="title" content="{{$forty_nwawia->first() ? $forty_nwawia->first()->meta_title:''}}">
    <meta name="description" content="{!! $forty_nwawia->first() ? strip_tags($forty_nwawia->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$forty_nwawia->first() ? $forty_nwawia->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> الأربعون النوويه / فيديوهات </h3>
                </div>

            </div>

            @if(count($forty_nwawia) >0)

                <ul class="list-videos posters">
                    @foreach($forty_nwawia as $index => $nwawia)
                        <li>
                            <a href="{{url('/arboon-nwawia-videos/'.$nwawia->id)}}">
                                @if($nwawia->photo)
                                    <img src="{{asset('images/nawawy_forties/'.$nwawia->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif


                                <i class="fa fa-play"></i>
                                <span>{{$nwawia->title}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
                {{$forty_nwawia->links()}}

                <span> showing {{$forty_nwawia->links()->paginator->currentPage()}}  from {{$pages}} pages</span>

            @else
                <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
            @endif


        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')




@endpush
