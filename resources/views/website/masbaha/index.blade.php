@extends('website.layouts.master')
@push('title')
    {{__('site.masbaha')}}
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>المسبحة الالكترونية</h3>
                </div>
            </div>
            <div class="rosary-center">
                <img src="{{asset('assets/front/assets/images/icons/mesbaha-icon.svg')}}" alt="">
                <div class="rosary">
                    <div class="rosary-select checkout-form">
                        <div class="select-div">
                            <select id="zekr">
                                <option value="" disabled selected>اختر الذكر</option>
                                <option value="سبحان الله">سبحان الله</option>
                                <option value="الحمد لله">الحمد لله</option>
                                <option value="لا اله الا الله">لا اله الا الله</option>
                                <option value="الله اكبر">الله اكبر</option>
                                <option value="استغفر الله الذي لا إله إلا هو الحي القيوم وأتوب إليه إني كنت من الظالمين">استغفر الله الذي لا إله إلا هو الحي القيوم وأتوب إليه إني كنت من الظالمين</option>
                                <option value="استغفر الله العظيم ">استغفر الله العظيم</option>
                                <option value="اللهم صل على نبينا محمد وعلى آله وصحبه وسلم">اللهم صل على نبينا محمد وعلى آله وصحبه وسلم</option>
                                <option value="لا حول ولا قوة إلا بالله">لا حول ولا قوة إلا بالله</option>
                                <option value="سبحان الله وبحمده ">سبحان الله وبحمده</option>
                                <option value="سبحان الله العظيم ">سبحان الله العظيم</option>
                                <option value=" الله لا إله إلا هو وحده لا شريك له ، له الملك وله الحمد يحيي ويميت وهو على كل شئ قدير"> الله لا إله إلا هو وحده لا شريك له ، له الملك وله الحمد يحيي ويميت وهو على كل شئ قدير</option>
                                <option value="سبحان الله وبحمده عدد خلقه ورضاء نفسه وزنه عرشه ومداد كلماته">سبحان الله وبحمده عدد خلقه ورضاء نفسه وزنه عرشه ومداد كلماته</option>
                            </select>
                        </div>
                        <input class="zekr_count" type="number" placeholder="عدد مرات الذكر">
                        <button class="btn-ok submit_count ">موافق</button>
                        <input type="number" readonly value="" class="btn-ok zekr_amount">
                    </div>

                    <h3 class="rosary-title">اختر الذكر</h3>

                    <div class="rosary-count">

                        <button class="btn-plus increase-zekr"><i class="fa fa-plus"></i></button>
                        <input type="number" min="0" id="count_zekr" value="0">
                        <button class="btn-minus decrease-zekr"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection
@push('js')
    <script>
        $('#zekr').on('change', function (e) {
            $('#count_zekr ,.zekr_amount,.zekr_count').val(0)
            $('.rosary-title').text(e.target.value)
        });
        $('.submit_count').on('click', function () {

            $('.zekr_amount').val($('.zekr_count').val())
            // $('.zekr_count').val(0);
            $('#count_zekr').val(0)
            count = 0;
        });

        let count = $('#count_zekr').val();
        $('.increase-zekr').on('click', function () {

            if ($('.zekr_amount').val() <= 0 || $('#zekr').val() == '') {

                Toast.fire({
                    icon: 'warning',
                    title: 'من فضلك أختر الذكر أو أدخل عدد مرات الذكر'
                })

            } else {

                count++;
                $('#count_zekr').val(count)

                if (parseInt($('#count_zekr').val()) >= parseInt($('.zekr_amount').val())) {
                    Toast.fire({
                        icon: 'success',
                        title: 'لقد أتممت  الذكر'
                    })
                    count = 0;
                    // $('#count_zekr').val(count)
                }
            }


        });

        $('.decrease-zekr').on('click', function () {
            count--
            $(this).parent().find('input').val(count);

            if ($('#count_zekr').val() <= 0) {
                $('#count_zekr').val(0);
                count = 0;
            }
        })

    </script>
@endpush
