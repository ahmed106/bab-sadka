@extends('website.layouts.master')
@push('title')
    {{__('site.prophet_stories')}} | {{__('site.listen')}}
@endpush
@push('seo')
    <meta name="title" content="{{$prophets->first() ? $prophets->first()->meta_title:''}}">
    <meta name="description" content="{!! $prophets->first() ? strip_tags($prophets->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$prophets->first() ? $prophets->first()->meta_keywords:''}}">
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3>قصص الأنبياء / استماع</h3>
                </div>
            </div>


            @if(count($prophets) > 0)
                <ul class="list-sor">


                    @foreach($prophets as $index=> $prophet)

                        @if($prophet->audio )
                            <li class="sora-container">
                                <div class="sora-num">
                                    <img src="{{asset('assets/front/assets/images/icons/rub-el-hizb-icon.svg')}}" alt="{{$prophet->name}}" title="{{$prophet->name}}">
                                    <span>{{++$index}}</span>
                                </div>
                                <div class="player">
                                    <div class="sora-top">
                                        <div class="sora-titles">
                                            <div class="sora-title">
                                                <input type="hidden" value="{{$prophet->name}}">
                                                {{$prophet->name}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="player-controls">
                                        <div id="sound-container-{{$prophet->id}}" data-id="{{$prophet->id}}" data-reader_name="{{$prophet->name}}" data-surah_name="{{$prophet->name}}" data-sound="{{$prophet->audio}}" class="jp-jplayer sound_player"></div>
                                        <div id="sound-container-{{$prophet->id}}" class="jp-audio top-player">
                                            <div class="player-nine">
                                                <div class="jp-type-single">
                                                    <div class="jp-gui jp-interface">
                                                        <div class="player-container-left">
                                                            <a class="jp-play   " data-audio_id="{{$prophet->id}}" tabindex="1" title="تشغيل"></a>
                                                            <a class="jp-pause" tabindex="1" title="ايقاف"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endif

                    @endforeach


                </ul>
            @else
                <div class="alert text-center alert-danger">للأسف لا يوجد بيانات</div>
            @endif


        </div>


        <div class="bottombar">
            <div class="bottombar-container">
                <div id="sound-player" class="jp-jplayer">
                    <img id="jp_poster_0">
                    <audio id="jp_audio_0" preload="metadata"></audio>
                </div>
                <div id="sound-container-1" class="footer_sound_container jp-audio top-player audio_player">
                    <div class="player-nine">
                        <div class="jp-type-single">
                            <div class="jp-gui jp-interface">
                                <div class="player-container-left">
                                    <a id="prev-button" class="prev-button prev-button-disabled" title="السابق"></a>
                                    <a class="jp-play jp_play_footer  " tabindex="2" title="تشغيل"></a>
                                    <a class="jp-pause jp_pause_footer " tabindex="2" title="ايقاف"></a>
                                    <a id="next-button" class="next-button next-button-disabled" title="التالي"></a>
                                    <div class="jp-current-time" id="current-time">00:00</div>
                                </div>
                                <div class="player-container-middle">
                                    <div class="jp-progress">
                                        <div class="jp-seek-bar">
                                            <div class="jp-play-bar">
                                                <div class="bullet"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="player-container-right">
                                    <div class="jp-duration" id="duration-time">00:00</div>
                                    <a class="jp-mute" tabindex="1" title="صامت"></a>
                                    <a class="jp-unmute" tabindex="1" title="غير صامت"></a>
                                    <div class="jp-volume-bar" title="مستوى الصوت">
                                        <div class="jp-volume-bar-value">
                                            <div class="bullet"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="player-three">
                        <div id="topbar-track-info">
                            <div id="topbar-track-details">
                                <div class=" d-none" id="loader">
                                    <i class="fa fa-spin fa-spinner fa-1x text-center"></i>
                                </div>
                                <div class="topbar-song-name" id="sw-song-name"></div>

                                <div class="topbar-song-name" id="sw-author-name"></div>


                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection

@push('js')

    <script src="{{asset('assets/front/assets/js/jPlayer/jquery.jplayer.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.jp-play').on('click', function () {
                let audio_id = $(this).data('audio_id');

                $('.footer_sound_container').attr('id', 'sound-container-' + audio_id);
                playAudio(audio_id, 'prophet_stories');
                play(audio_id)
            });

            function playAudio(id, type) {
                $.ajax({
                    type: 'get',
                    url: '{{route('hadith.getAudio')}}',
                    data: {
                        id: id,
                        'class': type
                    },

                    success: function (data) {
                        $('.audio_player').html(data.data);


                    },
                    complete: function () {
                        $('#loader').addClass('d-none')
                    }
                });
            }

            function play(id) {
                $('#loader').removeClass('d-none');
                $('.topbar-song-name').html('')
                $('#sound-container-' + id).jPlayer({
                    ready: function () { // The $.jPlayer.event.ready event
                        $(this).jPlayer("setMedia", { // Set the media
                            m4a: "{{asset('uploads/prophet_stories/audios')}}" + '/' + $('#sound-container-' + id).data('sound'),
                            oga: "{{asset('uploads/prophet_stories/audios')}}" + '/' + $('#sound-container-' + id).data('sound')
                        }).jPlayer("play"); // Attempt to auto play the media
                    },
                    play: function () {
                        $(this).jPlayer("pauseOthers", 0); // pause all players except this one.
                    },
                    cssSelectorAncestor: "#sound-container-" + id,
                    swfPath: "/js",
                    supplied: "m4a, oga",
                    useStateClassSkin: true,
                    autoBlur: true,
                    smoothPlayBar: true,
                    keyEnabled: true,
                    remainingDuration: true,
                    toggleDuration: true


                });


            }

        });
    </script>
@endpush
