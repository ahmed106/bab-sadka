@extends('website.layouts.master')
@push('title')
    {{__('site.prophet_stories')}} | {{__('site.videos')}}
@endpush
@push('seo')
    <meta name="title" content="{{$prophets->first() ? $prophets->first()->meta_title:''}}">
    <meta name="description" content="{!! $prophets->first() ? strip_tags($prophets->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$prophets->first() ? $prophets->first()->meta_keywords:''}}">
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')


        <div class="content">

            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> قصص الأنبياء/ فيديوهات </h3>
                </div>
                {{--                <div class="left form-group">--}}

                {{--                    <form id="pagination_form" action="{{url('prophets-stories/videos')}}" method="get">--}}

                {{--                        <select onchange="$('#pagination_form').submit()" name="paginator" id="" class="form-control">--}}
                {{--                            <option {{$paginator == 12 ?'selected':''}} value="12">12</option>--}}
                {{--                            <option {{$paginator == 16 ?'selected':''}} value="16">16</option>--}}
                {{--                            <option {{$paginator == 20 ?'selected':''}} value="20">20</option>--}}
                {{--                        </select>--}}
                {{--                        <label for="">show </label>--}}
                {{--                    </form>--}}

                {{--                </div>--}}

            </div>

            @if(count($prophets) >0)

                <ul class="list-videos posters">
                    @foreach($prophets as $index => $prophet)
                        <li>
                            <a href="{{url('/prophets-stories-videos/'.$prophet->id)}}">

                                @if($prophet->photo)
                                    <img src="{{asset('images/prophet_stories/'.$prophet->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif


                                <i class="fa fa-play"></i>
                                <span>{{$prophet->name}}</span>
                            </a>
                        </li>
                    @endforeach

                </ul>
                {{$prophets->links()}}

                <span> يعرض {{$prophets->links()->paginator->currentPage()}}  من {{$pages}} صفحه  \ صفحات</span>


            @else
                <div class="alert alert-danger text-center">للأسف لايوجد بيانات</div>
            @endif


        </div>
    </div><!-- //Main wrapper -->
@endsection

@push('js')




@endpush
