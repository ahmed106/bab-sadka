@extends('website.layouts.master')
@push('title')
    {{__('site.prophet_stories')}} | {{__('site.videos')}} | {{$prophet_story->name}}
@endpush
@section('content')
    <div class="wrapper">
        @include('website.includes.banner_area')
        <div class="content">
            <div class="he-vid">
                <div class="vid-main-wrapper">
                    <!-- THE YOUTUBE PLAYER -->

                    <div class="vid-container">
                        @if($prophet_story->video_type=='url')
                            <iframe id="vid_frame" src="{{getYoutubeId($prophet_story->video)}}"
                                    frameborder="0" allowfullscreen></iframe>
                        @else
                            <video autoplay width="100%" controls src="{{asset('uploads/prophet_stories/videos/'.$pray->video)}}"></video>
                        @endif


                    </div>
                    <!-- THE PLAYLIST -->


                    <div class="vid-list-container">
                        <ol class="vid-list" id="scrollable">
                            @foreach($stories as $story)
                                <li class="{{$prophet_story->id == $story->id ? 'active':''}}">
                                    <a href="{{url('/prophets-stories-videos/'.$story->id)}}"
                                        {{--onClick="document.getElementById('vid_frame').src='{{getYoutubeId($kid_video->video)}}'"--}}>
                            <span class="vid-thumb">
                                 @if($story->photo)
                                    <img src="{{asset('images/prophet_stories/'.$story->photo)}}" alt="">
                                @else
                                    <img src="{{asset('def.png')}}" alt="">
                                @endif
                            </span>
                                        <div class="desc">{{$story->name}}</div>
                                    </a>
                                </li>
                            @endforeach
                        </ol>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
