@extends('website.layouts.master')
@push('title')
    {{__('site.prophet_stories')}} | {{__('site.read')}}
@endpush
@push('seo')
    <meta name="title" content="{{$prophets->first() ? $prophets->first()->meta_title:''}}">
    <meta name="description" content="{!! $prophets->first() ? strip_tags($prophets->first()->meta_description):''!!}">
    <meta name="keywords" content="{{$prophets->first() ? $prophets->first()->meta_keywords:''}}">
@endpush
@section('content')
    <div class="wrapper">
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
          
            <div class="top-menu checkout-form">
                <div class="right">
                    <h3> قصص الأنبياء / قراءة</h3>
                </div>
            </div>
            <div class="tabs-forty">
                <ul class="nav nav-tabs list-forty read-qessas" id="myTab" role="tablist">
                    @foreach($prophets as $prophet)
                        <li class="nav-item" role="presentation">
                            <button class="nav-link {{$loop->first ? 'active':''}}" id="prophet-{{$prophet->id}}-tab" data-bs-toggle="tab" data-bs-target="#prophet-{{$prophet->id}}"
                                    type="button" role="tab" aria-controls="prophet-{{$prophet->id}}" aria-selected="true">
                                <span>{{$prophet->name}}</span>
                            </button>
                        </li>
                    @endforeach


                </ul>
                <div class="tab-content">

                    @foreach($prophets as  $prophet)
                        <div class="tab-pane  {{$loop->first ? 'active fade show':''}}" id="prophet-{{$prophet->id}}" role="tabpanel" aria-labelledby="prophet-{{$prophet->id}}-tab">
                            <div class="text-hadith">
                                <h3>{{$prophet->name}}</h3>
                                <p>
                                    {!! $prophet->story !!}
                                </p>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection
