@extends('website.layouts.master')
@push('title')
    {{__('site.prophet_stories')}}
@endpush
@section('content')
    <div class="wrapper">
        <!-- Header -->
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
           
            <section class="cr-section islams-pillar-area">
                <div class="islams-pillars__thumb text-center">
                    <img class="icon-page" src="{{asset('assets/front/assets/images/qessas.png')}}" alt="">
                </div>
                <div class="islams-pillars__content">
                    <h2>
                        إن ذكر الانبياء وقصصهم لها حكم متعددة أهمها تبيان أن كل الرسل والأنبياء هم رسل الله تعالى وجاءوا بنفس العقيدة التي جاء بها محمد ﷺ وأن كل الأمم مطالبة بنفس العقيدة وهي عقيدة التوحيد.
                    </h2>
                </div>
                <ul class="btns-btm">
                    <li>
                        <a href="{{route('prophet.read')}}">
                            <img src="{{asset('assets/front/assets/images/icons/read-icon.svg')}}" alt="{{__('site.read')}}" title="{{__('site.read')}}">
                            قراءة
                        </a>
                    </li>
                    <li>
                        <a href="{{route('prophet.listen')}}" class="style1">
                            <img src="{{asset('assets/front/assets/images/icons/listen-icon.svg')}}" alt="{{__('site.listen')}}" title="{{__('site.listen')}}">
                            استماع
                        </a>
                    </li>
                    <li>
                        <a href="{{route('prophet.videos')}}" class="style2">
                            <img src="{{asset('assets/front/assets/images/icons/vedio-icon.svg')}}" alt="{{__('site.videos')}}" title="{{__('site.videos')}}">
                            فيديوهات
                        </a>
                    </li>
                </ul>
            </section><!-- //Pillar Of Islam -->
        </div>

    </div><!-- //Main wrapper -->
@endsection
