@extends('website.layouts.master')

@push('title')
    {{__('site.add_orphan')}}
@endpush
@section('content')
    <!-- Main wrapper -->
    <div class="wrapper">
        <!-- Header -->
        <!-- Top Banner -->
        @include('website.includes.banner_area')

        <div class="content">
            <div class="top-menu">
                <div class="right">
                    <h3>اضافة يتيم</h3>
                </div>
            </div>
            <div class="step-app" id="demo-steps">
                <ul class="step-steps">
                    <li data-step-target="step1"><span>1</span> بيانات اليتيم</li>
                    <li data-step-target="step2"><span>2</span> بيانات أهل اليتيم</li>
                </ul>
                <div class="step-content checkout-form">
                    <form id="form" method="post" enctype="multipart/form-data" action="{{route('website.storeOrphan')}}">
                        @csrf
                        <div class="step-tab-panel" data-step="step1">
                            <div class="flex-divs">
                                <div class="input-block">
                                    <label>الإسم رباعي</label>
                                    <input type="text" accept="text" value="{{old('name')}}" name="name" placeholder="">
                                </div>
                                <div class="input-block">
                                    <label>رقم الهوية</label>
                                    <input type="number" value="{{old('id_number')}}" name="id_number" placeholder="">
                                </div>
                                <div class="input-select">
                                    <div class="select-div">
                                        <label>النوع</label>
                                        <select name="gender">
                                            <option value="male">ذكر</option>
                                            <option value="female">انثى</option>
                                        </select>
                                    </div>
                                    <div class="input-block">
                                        <label>تاريخ الميلاد</label>
                                        <input type="date" value="{{old('birthdate')}}" name="birthdate" placeholder="">
                                    </div>
                                </div>
                                <div class="select-div">
                                    <label>الدولة</label>
                                    <select class="select-2" name="country_id">
                                        <option value="" selected>إختر دوله</option>
                                        @foreach($countries as $country)

                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="select-div">
                                    <label>المدينه</label>
                                    <select class="select-2" name="city_id">
                                        <option value="" selected disabled>إختر المدينه</option>
                                    </select>
                                </div>
                                <div class="input-block">
                                    <label>العنوان</label>
                                    <input name="address" value="{{old('address')}}" type="text" placeholder="">
                                </div>
                                <div class="input-select">
                                    <div class="select-div">
                                        <label>اليتيم فقد</label>
                                        <select name="lost">
                                            <option value="father">الاب</option>
                                            <option value="mother"> الام</option>
                                            <option value="both">كلاهما</option>
                                        </select>
                                    </div>
                                    <div class="input-block">
                                        <label>مع من يعيش اليتيم</label>
                                        <input name="live_with" value="{{old('live_with')}}" type="text" placeholder="">
                                    </div>
                                </div>
                                <div class="input-select">
                                    <div class="select-div">
                                        <label>كود الدولة</label>
                                        <select name="country_code" class="code">
                                            <option value="+966">+966</option>
                                            <option value="+20">+20</option>
                                        </select>
                                    </div>
                                    <div class="input-block">
                                        <label>رقم الجوال</label>
                                        <input name="phone" value="{{old('phone')}}" type="number" placeholder="">
                                    </div>
                                </div>
                                <div class="input-block">
                                    <label>صاحب الرقم</label>
                                    <input name="phone_owner" value="{{old('phone_owner')}}" type="text" placeholder="">
                                </div>
                            </div>
                            <div class="btm-part">
                                <h3 class="title">الحالة الصحية لليتيم</h3>
                                <p class="text">هل اليتيم يعاني من اي امراض ؟</p>
                                <div class="flex-radio">
                                    <label for="yes">
                                        <input type="radio" name="has_disease" value="1" id="yes">
                                        <strong>نعم</strong>
                                    </label>
                                    <label for="no">
                                        <input type="radio" name="has_disease" value="0" id="no">
                                        <strong>لا</strong>
                                    </label>
                                </div>
                                <div class="flex-divs disease_details d-none">
                                    <div class="input-block">
                                        <label>نوع المرض</label>
                                        <input name="disease_type" value="{{old('disease_type')}}" type="text" placeholder="">
                                    </div>
                                    <div class="input-block">
                                        <label>تكلفة العلاج شهريا</label>
                                        <input type="text" value="{{old('disease_cost')}}" name="disease_cost" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="input-file">
                                <label>اضافة صورة هوية اليتيم</label>
                                <input name="photo" type="file" placeholder="">
                            </div>
                        </div>
                        <div class="step-tab-panel" data-step="step2">
                            <h3 class="tilte-head">بيانات والد اليتيم</h3>
                            <div class="flex-divs">
                                <div class="input-block">
                                    <label>الإسم رباعي</label>
                                    <input name="father_name" value="{{old('father_name')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block">
                                    <label>رقم الهوية</label>
                                    <input name="father_id" type="number" value="{{old('father_id')}}" placeholder="">
                                </div>

                                <div class="input-block father_death">
                                    <label>تاريخ الوفاة</label>
                                    <input name="father_death_date" value="{{old('father_death_date')}}" type="date" placeholder="">
                                </div>
                                <div class="input-block father_death">
                                    <label>سبب الوفاة</label>
                                    <input name="father_death_reason" value="{{old('father_death_reason')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block father_death">
                                    <label>مكان الوفاة</label>
                                    <input name="father_death_place" value="{{old('father_death_place')}}" type="text" placeholder="">
                                </div>

                                <div class="input-block father_details d-none">
                                    <label>المستوي التعليمي</label>
                                    <input name="father_education_level" value="{{old('father_education_level')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block father_details d-none">
                                    <label>المهنة</label>
                                    <input name="father_job" value="{{old('father_job')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block father_details d-none">
                                    <label>الراتب</label>
                                    <input name="father_salary" value="{{old('father_salary')}}" type="number" placeholder="">
                                </div>
                            </div>
                            <div class="input-file father_death_application">
                                <label>اضافة صورة شهادة الوفاة</label>
                                <input name="father_death_application" type="file" placeholder="">
                            </div>
                            <div class="input-file father_photo d-none ">
                                <label>اضافة صورة الهوية</label>
                                <input class="_image" name="father_photo" type="file" placeholder="">

                            </div>

                            <hr>

                            <h3 class="tilte-head">بيانات والدة اليتيم</h3>
                            <div class="flex-divs">
                                <div class="input-block">
                                    <label>الإسم رباعي</label>
                                    <input name="mother_name" value="{{old('mother_name')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block">
                                    <label>رقم الهوية</label>
                                    <input type="number" name="mother_id" value="{{old('mother_id')}}" placeholder="">
                                </div>
                                <div class="input-block mother_death d-none">
                                    <label>تاريخ الوفاه</label>
                                    <input name="mother_death_date" value="{{old('mother_death_date')}}" type="date" placeholder="">
                                </div>


                                <div class="input-block mother_death d-none">
                                    <label>سبب الوفاة</label>
                                    <input name="mother_death_reason" value="{{old('mother_death_reason')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block mother_death d-none">
                                    <label>مكان الوفاة</label>
                                    <input name="mother_death_place" value="{{old('mother_death_place')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block mother_details ">
                                    <label>المستوي التعليمي</label>
                                    <input name="mother_education_level" value="{{old('mother_education_level')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block mother_details ">
                                    <label>المهنة</label>
                                    <input name="mother_job" value="{{old('mother_job')}}" type="text" placeholder="">
                                </div>
                                <div class="input-block mother_details ">
                                    <label>الراتب</label>
                                    <input name="mother_salary" value="{{old('mother_salary')}}" type="number" placeholder="">
                                </div>

                            </div>
                            <div class="input-file mother_death_application d-none">
                                <label>اضافة صورة شهادة الوفاة</label>
                                <input class="_image" name="mother_death_application" type="file" placeholder="">

                            </div>
                            <div class="input-file mother_photo">
                                <label>اضافة صورة الهوية</label>
                                <input class="_image" name="mother_photo" type="file" placeholder="">

                            </div>
                        </div>
                    </form>
                </div>
                <div class="step-footer">
                    <button data-step-action="prev" class="button btn-prev">السابق</button>
                    <button data-step-action="next" class="button btn-next">التالي</button>
                    <button data-step-action="finish" class="button btn-add">ارسال</button>
                </div>
            </div>
        </div>

    </div><!-- //Main wrapper -->
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#demo-steps').steps({
                onFinish: function () {
                    $('#form').submit();
                }
            })
        });

        $('input[name="has_disease"]').on('change', function () {
            $(this).val() == 1 ? $('.disease_details').removeClass('d-none') : $('.disease_details').addClass('d-none')
        })
        $('select[name="lost"]').on('change', function () {

            if ($(this).val() == 'father') {
                $('.mother_death,.father_details,.father_photo,.mother_death_application').addClass('d-none');
                $('.father_death,.mother_details,.father_death_application,.mother_photo').removeClass('d-none');

            } else if ($(this).val() == 'mother') {
                $('.father_death,.mother_details,.mother_photo,.father_death_application').addClass('d-none');
                $('.mother_death,.father_details,.mother_death_application,.father_photo').removeClass('d-none');

            } else {
                $('.father_death ,.mother_death,.father_death_application,.mother_death_application').removeClass('d-none');
                $('.father_details ,.mother_details,.mother_photo,.father_photo').addClass('d-none');
            }

        });

        $('select[name="country_id"]').on('change', function () {
            let country_id = $(this).val();
            $('select[name="city_id"]').empty();
            $.ajax({
                type: "get",
                url: '{{route('website.getCity')}}',
                data: {
                    country_id: country_id
                },
                success: function (res) {

                    res.data.map((res) => {
                        $('select[name="city_id"]').append(`<option value="${res.id}">${res.name}</option>`)
                    });
                }
            });
        });
    </script>
@endpush
