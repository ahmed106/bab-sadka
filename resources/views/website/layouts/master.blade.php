<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @stack('seo')
    <title>{{$settings?$settings->website_name:''}} | @stack('title') </title>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="{!! strip_tags(isset($settings)?$settings->meta_title:'') !!}">
    <meta name="description" content="{!! strip_tags(isset($settings)?$settings->meta_description:'') !!}">
    <meta name="keywords" content="{!! strip_tags(isset($settings)?$settings->meta_keywords:'') !!}">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-75823513-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-122322331-1');
    </script>
    @include('website.includes.css')
</head>

<body>
<div class="loader-div">
    <span></span>
    <span></span>
</div>

<div class="page" id="app">

    <!-- sidebar -->
    <div class="top-head">
        <i class="fa fa-list btn-sidebar"></i>
        @include('website.includes.dead_session')
    </div>
    <div class="shadow-sidebar"></div>
    @include('website.includes.aside')

    <div class="d-none">
        <audio muted id="myAudio" controls src="{{asset('adan.mp3')}}"></audio>
    </div>


    <!-- Main wrapper -->
    @yield('content')

    <div class="footer">
        <p>جميع الحقوق محفوظة © لدى شركة <a href="https://codlop.sa/ar" target="_blank">Codlop</a></p>
    </div>
</div>

<script src="{{asset('js/app.js')}}"></script>
@include('website.includes.js')
<link rel="stylesheet" href="{{asset('assets/front/assets/css/plugins/animation.css')}}">

<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-75823513-5');
</script>
<script>


    Echo.channel('free-channel')
        .listen('.chat-event', (e) => {

            Swal.fire({
                title: e.message,
                showClass: {
                    popup: 'animate__animated animate__fadeInDown'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp'
                }
            });


            // Toast.fire({
            //     position: e.message,
            //     title: 'asd',
            //     timer: 15000,
            //
            // });

            $('#myAudio').get(0).play();

            $('#play_sound').removeClass('d-none');

            $('#pause_sound').addClass('d-none');


        });


    $('#play_sound , #pause_sound').on('click', function () {

        $('#myAudio').get(0).muted = true;
        $('#audio').get(0).muted = true;

        $('#play_sound').addClass('d-none');
        $('#pause_sound').removeClass('d-none');
    });

    $('#pause_sound').on('click', function () {
        $('#play_sound').removeClass('d-none');
        $('#pause_sound').addClass('d-none');
        $('#myAudio').get(0).muted = false;
        $('#audio').get(0).muted = false;

    });


</script>


</body>

</html>
