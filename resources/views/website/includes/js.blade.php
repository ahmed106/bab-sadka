<!-- JS Files -->

<script src="{{asset('assets/front/')}}/assets/js/jquery-3.6.0.min.js"></script>
<script src="{{asset('assets/front/')}}/assets/js/jquery-migrate-3.3.2.min.js"></script>
<script src="{{asset('assets/front/')}}/assets/js/modernizr-3.11.2.min.js"></script>
<script src="{{asset('assets/front/')}}/assets/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('assets/front/')}}/assets/js/plugins.js"></script>
<script src="{{asset('assets/front/')}}/assets/js/steps/jquery-steps.min.js"></script>
<script src="{{asset('assets/front/assets/js/flip-book/js/flipbook.min.js')}}"></script>
<script src="{{asset('assets/front/assets/js/select2/js/select2.js')}}"></script>
<script src="{{asset('assets/front/assets/js/swiper/swiper-bundle.min.js')}}"></script>
<script src="{{asset('assets/front/')}}/assets/js/active.js?v=2"></script>
<script src="{{asset('assets/front/')}}/assets/js/scripts.js?v=2"></script>
@include('website.includes.js_helper')

<script>
    $(document).ready(function () {
        $('.select-2').select2();
    });
</script>

<script>
    $('.select-modal').select2({
        dropdownParent: $('.modal-index')
    });
</script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@include('dashboard.includes.swa')


@stack('js')
