@if(session('dead'))
    <div class="box-2">
        @if(session('dead.photo') != null)
            <img src="{{asset('images/deads/'.session('dead.photo'))}}" alt="">
        @endif

        <span>{{session('dead.name')}}</span>
    </div>

@endif
