<!-- Favicons -->
<link rel="shortcut icon" href="{{asset('assets/front/')}}/assets/images/logo/logo.svg">
<!-- Stylesheets -->
<link rel="stylesheet" href="{{asset('assets/front/')}}/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="{{asset('assets/front/')}}/assets/css/plugins.css">
<link href="{{asset('assets/front/')}}/assets/js/flip-book/css/flipbook.style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{asset('assets/front/')}}/assets/js/steps/jquery-steps.min.css">
<link rel="stylesheet" href="{{asset('assets/front/')}}/assets/js/select2/css/select2.css">
<link rel="stylesheet" href="{{asset('assets/front/')}}/assets/js/swiper/swiper-bundle.min.css">
<link rel="stylesheet" href="{{asset('assets/front/')}}/assets/css/style.css?v=11">
@stack('css')
