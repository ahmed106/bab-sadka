<div class="sidebar">
    <div class="play-pause">
        <img id="play_sound" class="d-none" src="{{asset('assets/front/')}}/assets/images/volume-up.png" alt="">
        <img id="pause_sound" class="d-none" src="{{asset('assets/front/')}}/assets/images/silent.png" alt="">
        <!-- <i id="play_sound" class="d-none fa fa-volume-up"></i>
        <i id="pause_sound" class="d-none fa fa-volume-off "></i> -->
    </div>
    <a href="{{url('/')}}">
        <img class="logo" src="{{asset('assets/front/')}}/assets/images/logo/logo.svg" alt="{{$settings?$settings->website_name:''}}" title="{{$settings?$settings->website_name:''}}">
    </a>
    <ul>
        <li>
            <span>
                <a title="{{__('site.home')}}" href="{{url('/')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/home-icon.svg" title="{{__('site.home')}}" alt="{{__('site.home')}}"></a></span>
            <a title="{{__('site.home')}}" href="{{url('/')}}">الرئيسية</a>

        </li>
        <li>

            <span>
                 <a title="{{__('site.el_quran_el_karem')}}" href="{{session('dead') ? url('quran?D='.base64_encode(session('dead.id'))):url('quran') }}">
                      <img src="{{asset('assets/front/')}}/assets/images/icons/quran-icon.svg" alt="{{__('site.el_quran_el_karem')}}" title="{{__('site.el_quran_el_karem')}}">
                 </a>

            </span>
            <a title="{{__('site.el_quran_el_karem')}}" href="{{session('dead') ? url('quran?D='.base64_encode(session('dead.id'))):url('quran') }}">القرآن الكريم</a>
        </li>
        <li>
            <span>
                 <a title="{{__('site.hadiths')}}" href=" {{session('dead') ? url('hadith?D='.base64_encode(session('dead.id'))):url('hadith') }} ">
                     <img src="{{asset('assets/front/')}}/assets/images/icons/hadeth-icon.svg" alt="{{__('site.hadiths')}}" title="{{__('site.hadiths')}}">
                 </a>
            </span>
            <a title="{{__('site.hadiths')}}" href="{{session('dead') ? url('hadith?D='.base64_encode(session('dead.id'))):url('hadith') }}">الحديث النبوي</a>
        </li>
        <li>
            <span>
                <a title="{{__('site.prophet_stories')}}" href="{{session('dead') ? url('prophets-stories?D='.base64_encode(session('dead.id'))):url('prophets-stories') }}">
                <img src="{{asset('assets/front/')}}/assets/images/icons/qesas-icon.svg" alt="{{__('site.prophet_stories')}}" title="{{__('site.prophet_stories')}}">
                    </a>
            </span>
            <a title="{{__('site.prophet_stories')}}" href="{{session('dead') ? url('prophets-stories?D='.base64_encode(session('dead.id'))):url('prophets-stories') }}">قصص الأنبياء</a>
        </li>
        <li>
            <span><a title="{{__('site.hesn_elmuslim')}}" href=" {{session('dead') ? url('hesn-elmuslim?D='.base64_encode(session('dead.id'))):url('hesn-elmuslim') }}"><img src="{{asset('assets/front/')}}/assets/images/icons/hesn-icon.svg" alt="{{__('site.hesn_elmuslim')}}" title="{{__('site.hesn_elmuslim')}}"></a></span>
            <a title="{{__('site.hesn_elmuslim')}}" href=" {{session('dead') ? url('hesn-elmuslim?D='.base64_encode(session('dead.id'))):url('hesn-elmuslim') }}">حصن المسلم</a>
        </li>
        <li>
            <span><a title="{{__('site.arboaon_nwawya')}}" href=" {{session('dead') ? url('arboon-nwawia?D='.base64_encode(session('dead.id'))):url('arboon-nwawia') }}"><img src="{{asset('assets/front/')}}/assets/images/icons/arba3on-icon.svg" alt="{{__('site.arboaon_nwawya')}}" title="{{__('site.arboaon_nwawya')}}"></a></span>
            <a title="{{__('site.arboaon_nwawya')}}" href="{{session('dead') ? url('arboon-nwawia?D='.base64_encode(session('dead.id'))):url('arboon-nwawia') }}">الأربعون النووية</a>
        </li>
        <li>
            <span> <a title="{{__('site.kid_videos')}}" href="{{url('/kid-videos')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/atfal-icon.svg" alt="{{__('site.kid_videos')}}" title="{{__('site.kid_videos')}}"></a></span>
            <a title="{{__('site.kid_videos')}}" href="{{url('/kid-videos')}}">فيديوهات أطفال</a>
        </li>
        <li>
            <span> <a title="{{__('site.masbaha')}}" href=" {{session('dead') ? url('masbaha?D='.base64_encode(session('dead.id'))):url('masbaha') }}"><img src="{{asset('assets/front/')}}/assets/images/icons/mesbaha-icon.svg" alt="{{__('site.masbaha')}}" title="{{__('site.masbaha')}}"></a></span>
            <a title="{{__('site.masbaha')}}" href=" {{session('dead') ? url('masbaha?D='.base64_encode(session('dead.id'))):url('masbaha') }}">المسبحة</a>
        </li>
        <li>
            <span> <a title="{{__('site.libraries')}}" href="{{url('/libraries')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/maktabat-icon.svg" alt="{{__('site.libraries')}}" title="{{__('site.libraries')}}"></a></span>
            <a title="{{__('site.libraries')}}" href="{{url('/libraries')}}">المكتبات</a>
        </li>
        {{--        <li>--}}
        {{--            <span>--}}
        {{--                <a title="{{__('site.add_orphan')}}" href="{{url('add-orphan')}}">--}}
        {{--                     <img src="{{asset('assets/front/')}}/assets/images/icons/yatem-icon.svg" alt="{{__('site.add_orphan')}}" title="{{__('site.add_orphan')}}">--}}
        {{--                </a>--}}

        {{--            </span>--}}
        {{--            <a title="{{__('site.add_orphan')}}" href="{{url('add-orphan')}}">إضافة يتيم</a>--}}
        {{--        </li>--}}


        <li>
                    <span>
                         <a title="{{__('site.add_dead')}}" href="{{url('add-dead')}}">
                        <img src="{{asset('assets/front/')}}/assets/images/icons/add.svg" alt="{{__('site.add_dead')}}" title="{{__('site.add_dead')}}">
                         </a>
                    </span>
            <a title="{{__('site.add_dead')}}" href="{{url('add-dead')}}">إنشاء صدقه جاريه</a>
        </li>
        <li>
            <span> <a href=" {{session('dead') ? url('pray?D='.base64_encode(session('dead.id'))):url('pray') }}"><img src="{{asset('assets/front/assets/images/icons/doaa-icon.svg')}}" alt=""></a></span>
            <a href=" {{session('dead') ? url('pray?D='.base64_encode(session('dead.id'))):url('pray') }}">الدعاء للمتوفي</a>

        </li>
        {{--        <li>--}}
        {{--            <span> <a title="{{__('site.settings')}}" href="{{url('settings/')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/settings-icon.svg" alt="{{__('site.settings')}}" title="{{__('site.settings')}}"></a></span>--}}
        {{--            <a title="{{__('site.settings')}}" href="{{url('settings/')}}">الإعدادات</a>--}}
        {{--        </li>--}}
        <li>
            <span><a href="{{url('/share')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/share-icon.svg" alt=""></a></span>
            <a href="{{url('/share')}}">مشاركة الموقع</a>
        </li>
        <li>
            <span>  <a title="{{__('site.about')}}" href="{{url('/about-us')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/about us-icon.svg" alt="{{__('site.about')}}" title="{{__('site.about')}}"></a></span>
            <a title="{{__('site.about')}}" href="{{url('/about-us')}}">من نحن</a>
        </li>
        <li>
            <span>
                <a title="{{__('site.contact_us')}}" href="{{url('/contact-us')}}"><img src="{{asset('assets/front/')}}/assets/images/icons/contact-icon.svg" alt="{{__('site.contact_us')}}" title="{{__('site.contact_us')}}"></a></span>
            <a title="{{__('site.contact_us')}}" href="{{url('/contact-us')}}">تواصل معنا</a>
        </li>
    </ul>
</div>
