<div class="banner-area" dir="rtl">
    <div class="banner banner-slide-active slide-animate-text">
        <!-- Single Banner -->
        <div class="banner__single fullscreen d-flex align-items-center" style="background: url({{asset('assets/front')}}/assets/images/bg/1.jpg);" data-black-overlay="3">
            <video autoplay muted loop id="myVideo">
                <source src="{{asset('assets/front')}}/assets/images/bg.mp4" type="video/mp4"/>
            </video>
        </div><!-- //Single Banner -->
        <div class="banner__single fullscreen d-flex align-items-center" style="background: url({{asset('assets/front')}}/assets/images/bg/2.jpg);" data-black-overlay="3">
            <video autoplay muted loop id="myVideo">
                <source src="{{asset('assets/front')}}/assets/images/bg.mp4" type="video/mp4"/>
            </video>
        </div><!-- //Single Banner -->
    </div>
</div>
