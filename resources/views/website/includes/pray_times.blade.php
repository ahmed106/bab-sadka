<ul class="swiper-wrapper">
    <li class="swiper-slide">
        <img src="{{asset('assets/front/')}}/assets/images/bg/moaket/1.png" alt="">
        <div>

            <p>الفجر</p>
            <span>{{$pray_times['fajr']}}</span>
        </div>
    </li>
    <li class="swiper-slide">
        <img src="{{asset('assets/front/')}}/assets/images/bg/moaket/2.png" alt="">
        <div>
            <p>الظهر</p>

            <span>{{$pray_times['dhuhr']}}</span>
        </div>
    </li>
    <li class="swiper-slide">
        <img src="{{asset('assets/front/')}}/assets/images/bg/moaket/3.png" alt="">
        <div>
            <p>العصر</p>
            <span>{{$pray_times['asr']}}</span>
        </div>
    </li>
    <li class="swiper-slide">
        <img src="{{asset('assets/front/')}}/assets/images/bg/moaket/4.png" alt="">
        <div>
            <p>المغرب</p>
            <span>{{$pray_times['maghrab']}}</span>
        </div>
    </li>
    <li class="swiper-slide">
        <img src="{{asset('assets/front/')}}/assets/images/bg/moaket/5.png" alt="">
        <div>
            <p>العشاء</p>
            <span>{{$pray_times['isha']}}</span>
        </div>
    </li>
</ul>
<script>

$(document).ajaxComplete(function(){

var swiper = new Swiper(".moaket", {
speed: 300,
slidesPerView: 'auto',
freeMode:true,
spaceBetween: 20,
navigation: false,
breakpoints: {  
0: {
    spaceBetween: 10,
},
576: {
    spaceBetween: 20,
}
}
});
});
</script>