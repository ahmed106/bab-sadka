<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Language::create([
            'name' => 'العربيه',
            'locale' => 'ar',
            'country_abbreviation' => 'eg',
            'active' => 1
        ]);
        \App\Models\Language::create([
            'name' => 'English',
            'locale' => 'en',
            'country_abbreviation' => 'us',
            'active' => 0
        ]);
    }
}
