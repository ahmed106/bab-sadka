<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(ProphetTableSeeder::class);
        $this->call(NawawyFortyTableSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(PrayTimeSeeder::class);
    }
}
