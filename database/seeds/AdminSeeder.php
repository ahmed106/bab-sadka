<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin = \App\Models\Admin::create([
            'name' => 'super_admin',
            'email' => 'superadmin@admin.com',
            'password' => bcrypt('admin'),
            'photo' => null,
        ]);

        $admin = \App\Models\Admin::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'photo' => null,

        ]);

        $super_admin->attachRole('super_admin');
        $admin->attachRole('admin');
    }
}
