<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $country = new \App\Models\Country();
        $city = new \App\Models\City();

        $country->create([
            'name' => 'Egypt',
        ]);

    }
}
