<?php

use App\Models\PrayTime;
use Illuminate\Database\Seeder;

class PrayTimeSeeder extends Seeder
{

   use \App\Traits\prayTimes;
    public function run()
    {
        $countries = [
            0 => [
                'name' => 'Egypt',
                'timezone' => 'Africa/Cairo'
            ],
            1 => [
                'name' => 'Sudia Arabia',
                'timezone' => 'Asia/Riyadh'
            ]
        ];
        foreach ($countries as $index => $country) {
            $pray_times = $this->prayTimes($countries[$index]['name']);
            PrayTime::create([
                'country' => $countries[$index]['name'],
                'timezone' => $countries[$index]['timezone'],
                'fajr' => $pray_times['fajr'],
                'dhuhr' => $pray_times['dhuhr'],
                'asr' => $pray_times['asr'],
                'maghrab' => $pray_times['maghrab'],
                'isha' => $pray_times['isha'],
            ]);
        }
    }
}
