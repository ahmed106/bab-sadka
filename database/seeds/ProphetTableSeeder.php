<?php

use Illuminate\Database\Seeder;

class ProphetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \Goutte\Client();
        $crawler = $client->request('GET', 'https://abdohantash.github.io/bab-sadaka/read-qessas.html');

        $names = [];
        $index = 0;
        $crawler->filter('#myTab li')->each(function ($node) use (&$index, &$names, &$crawler) {
            $names[$index]['ar']['name'] = $node->text();
            $names[$index]['video_type'] = 'url';
            $names[$index]['video'] = 'https://www.youtube.com/watch?v=NI4631cGv_k';
            $names[$index]['audio'] = '2215_112.mp3';
            $crawler->filter('.tab-content p')->each(function ($node) use (&$index, &$names) {
                $names[$index]['ar']['story'] = $node->text();

            });
            $index++;
        });

        \DB::table('prophet_stories')->delete();
        foreach ($names as $new) {
            \App\Models\ProphetStory::create($new);
        }
    }
}
