<?php

use Illuminate\Database\Seeder;

class NawawyFortyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new \Goutte\Client();
        $crawler = $client->request('GET', 'https://abdohantash.github.io/bab-sadaka/read-forty.html');

        $names = [];
        $index = 0;
        $crawler->filter('#myTab li')->each(function ($node) use (&$index, &$names, &$crawler) {
            $names[$index]['ar']['title'] = $node->text();
            $names[$index]['video_type'] = 'url';
            $names[$index]['video'] = 'https://www.youtube.com/watch?v=NI4631cGv_k';
            $names[$index]['audio'] = '5452_ahmad_huth_001.mp3';
            $crawler->filter('.tab-content p')->each(function ($node) use (&$index, &$names) {
                $names[$index]['ar']['content'] = $node->text();

            });
            $index++;
        });

        foreach ($names as $name) {
            \App\Models\NawawyForty::create($name);
        }
    }
}
