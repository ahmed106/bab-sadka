<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surahs', function (Blueprint $table) {
            $table->id();
            $table->enum('video_type', ['url', 'file']);
            $table->string('video')->nullable();
            $table->string('audio')->nullable();
            $table->unsignedBigInteger('reader_id');
            $table->timestamps();

            $table->foreign('reader_id')->references('id')->on('quran_readers')->onDelete('cascade');
        });
        Schema::create('surah_translations', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('surah_id');
            $table->string('locale')->index();
            $table->unique(['surah_id', 'locale']);
            $table->timestamps();
            $table->foreign('surah_id')->references('id')->on('surahs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surahs');
        Schema::dropIfExists('surah_translations');
    }
}
