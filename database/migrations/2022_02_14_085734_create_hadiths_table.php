<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHadithsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hadiths', function (Blueprint $table) {
            $table->id();
            $table->enum('video_type', ['url', 'file']);
            $table->string('video');
            $table->string('audio');
            $table->timestamps();
        });

        Schema::create('hadith_translations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('locale')->index();
            $table->unsignedBigInteger('hadith_id');
            $table->unique(['hadith_id', 'locale']);

            $table->timestamps();
            $table->foreign('hadith_id')->references('id')->on('hadiths')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hadiths');
        Schema::dropIfExists('hadith_translations');
    }
}
