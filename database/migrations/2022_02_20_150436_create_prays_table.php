<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePraysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prays', function (Blueprint $table) {
            $table->id();
            $table->enum('video_type', ['url', 'file']);
            $table->string('video');
            $table->string('audio')->nullable();
            $table->timestamps();
        });
        Schema::create('pray_translations', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('pray');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('pray_id');
            $table->string('locale');
            $table->unique(['locale', 'pray_id']);
            $table->foreign('pray_id')->references('id')->on('prays')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prays');
        Schema::dropIfExists('pray_translations');
    }
}
