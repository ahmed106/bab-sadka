<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrphansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orphans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('id_number')->nullable();
            $table->enum('gender', ['male', 'female']);
            $table->date('birthdate');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('city_id');
            $table->string('address');
            $table->enum('lost', ['mother', 'father', 'both']);
            $table->string('live_with');
            $table->integer('country_code');
            $table->bigInteger('phone');
            $table->bigInteger('phone_owner');
            $table->boolean('has_disease')->default(false);
            $table->string('disease_type')->nullable();
            $table->integer('disease_cost')->nullable();
            $table->string('photo')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            // orphan father
            $table->string('father_name');
            $table->string('father_id');
            $table->date('father_death_date')->nullable();
            $table->string('father_death_reason')->nullable();
            $table->string('father_death_place')->nullable();
            $table->string('father_death_application')->nullable();
            $table->string('father_photo')->nullable();
            $table->string('father_education_level')->nullable();
            $table->string('father_job')->nullable();
            $table->string('father_salary')->nullable();
            // orphan mother
            $table->string('mother_name');
            $table->string('mother_id');
            $table->date('mother_death_date')->nullable();
            $table->string('mother_death_reason')->nullable();
            $table->string('mother_death_place')->nullable();
            $table->string('mother_death_application')->nullable();
            $table->string('mother_photo')->nullable();
            $table->string('mother_education_level')->nullable();
            $table->string('mother_job')->nullable();
            $table->string('mother_salary')->nullable();

            $table->boolean('approval')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orphans');
    }
}
