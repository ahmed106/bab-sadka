<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProphetStoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prophet_stories', function (Blueprint $table) {
            $table->id();
            $table->enum('video_type', ['url', 'file']);
            $table->string('video');
            $table->string('audio');
            $table->timestamps();
        });
        Schema::create('prophet_story_translations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->longText('story');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('prophet_story_id');
            $table->string('locale')->index();
            $table->unique(['locale', 'prophet_story_id']);

            $table->timestamps();
            $table->foreign('prophet_story_id')->references('id')->on('prophet_stories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prophet_stories');
        Schema::dropIfExists('prophet_story_translations');
    }
}
