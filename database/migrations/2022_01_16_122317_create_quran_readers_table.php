<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuranReadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quran_readers', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });

        Schema::create('quran_reader_translations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('quran_reader_id');
            $table->string('locale')->index();
            $table->unique(['locale', 'quran_reader_id']);
            $table->timestamps();
            $table->foreign('quran_reader_id')->references('id')->on('quran_readers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quran_readers');
        Schema::dropIfExists('quran_reader_translations');
    }
}
