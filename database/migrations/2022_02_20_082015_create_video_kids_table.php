<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoKidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_kids', function (Blueprint $table) {
            $table->id();

            $table->enum('video_type', ['file', 'url']);
            $table->string('video');
            $table->timestamps();
        });

        Schema::create('video_kid_translations', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('video_kid_id');
            $table->string('locale')->index();
            $table->unique(['locale', 'video_kid_id']);

            $table->foreign('video_kid_id')->references('id')->on('video_kids')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_kids');
        Schema::dropIfExists('video_kid_translations');
    }
}
