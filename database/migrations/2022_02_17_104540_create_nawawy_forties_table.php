<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNawawyFortiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nawawy_forties', function (Blueprint $table) {
            $table->id();
            $table->enum('video_type', ['url', 'file']);
            $table->string('video')->nullable();
            $table->string('audio')->nullable();
            $table->timestamps();
        });
        Schema::create('nawawy_forty_translations', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->longText('content');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('nawawy_forty_id');
            $table->string('locale')->index();
            $table->unique(['locale', 'nawawy_forty_id']);
            $table->foreign('nawawy_forty_id')->references('id')->on('nawawy_forties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nawawy_forties');
        Schema::dropIfExists('nawawy_forty_translations');
    }
}
