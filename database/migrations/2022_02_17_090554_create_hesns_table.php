<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHesnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hesns', function (Blueprint $table) {
            $table->id();
            $table->enum('video_type', ['url', 'file']);
            $table->string('video');
            $table->string('audio');
            $table->timestamps();
        });

        Schema::create('hesn_translations', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->unsignedBigInteger('hesn_id');
            $table->string('locale')->index();
            $table->unique(['hesn_id', 'locale']);
            $table->foreign('hesn_id')->references('id')->on('hesns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hesns');
        Schema::dropIfExists('hesn_translations');

    }
}
