<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrayTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pray_times', function (Blueprint $table) {
            $table->id();
            $table->string('country');
            $table->string('timezone');
            $table->time('fajr');
            $table->time('dhuhr');
            $table->time('asr');
            $table->time('maghrab');
            $table->time('isha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pray_times');
    }
}
