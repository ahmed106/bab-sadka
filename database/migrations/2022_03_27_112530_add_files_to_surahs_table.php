<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilesToSurahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surahs', function (Blueprint $table) {

            $table->string('photo')->nullable();

        });
        Schema::table('hadiths', function (Blueprint $table) {
            $table->string('photo')->nullable();
        });
        Schema::table('prophet_stories', function (Blueprint $table) {
            $table->string('photo')->nullable();
        });
        Schema::table('hesns', function (Blueprint $table) {
            $table->string('photo')->nullable();
        });
        Schema::table('nawawy_forties', function (Blueprint $table) {
            $table->string('photo')->nullable();
        });
        Schema::table('video_kids', function (Blueprint $table) {
            $table->string('photo')->nullable();
        });
        Schema::table('prays', function (Blueprint $table) {
            $table->string('photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surahs', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
        Schema::table('hadiths', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
        Schema::table('prophet_stories', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
        Schema::table('hesns', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
        Schema::table('nawawy_forties', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
        Schema::table('video_kids', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
        Schema::table('prays', function (Blueprint $table) {
            $table->dropColumn('photo');
        });
    }
}
