<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [

        'super_admin' => [
            'admins' => 'c,r,u,d',
            'languages' => 'c,r,u,d',
            'quran_readers' => 'c,r,u,d',
            'roles' => 'c,r,u,d',
            'surahs' => 'c,r,u,d',
            'hadiths' => 'c,r,u,d',
            'prophet_stories' => 'c,r,u,d',
            'hesns' => 'c,r,u,d',
            'nawawy_forty' => 'c,r,u,d',
            'video_kids' => 'c,r,u,d',
            'library_categories' => 'c,r,u,d',
            'libraries' => 'c,r,u,d',
            'prays' => 'c,r,u,d',
            'countries' => 'c,r,u,d',
            'cities' => 'c,r,u,d',
            'orphans' => 'c,r,u,d',
            'contactUs' => 'c,r,u,d',
            'dead' => 'c,r,u,d',


        ],
        'admin' => [
            'admins' => 'c,r,',
            'languages' => 'c,r',
            'quran_readers' => 'r,c',
            'roles' => 'c,r',
            'surahs' => 'c,r,u,d',
            'hadiths' => 'c,r,u,d',
            'prophet_stories' => 'c,r,u,d',
            'hesns' => 'c,r,u,d',
            'nawawy_forty' => 'c,r,u,d',
            'video_kids' => 'c,r,u,d',
            'library_categories' => 'c,r,u,d',
            'libraries' => 'c,r,u,d',
            'prays' => 'c,r,u,d',
            'countries' => 'c,r,u,d',
            'cities' => 'c,r,u,d',
            'orphans' => 'c,r,u,d',
            'contactUs' => 'c,r,u,d',
            'dead' => 'c,r,u,d',

        ],
        'owner' => [
            'admins' => 'c,r,u,d',
            'languages' => 'c,r,u,d',
            'quran_readers' => 'r,u',
            'roles' => 'c,r,u,d',
            'surahs' => 'c,r,u,d',
            'hadiths' => 'c,r,u,d',
            'prophet_stories' => 'c,r,u,d',
            'hesns' => 'c,r,u,d',
            'nawawy_forty' => 'c,r,u,d',
            'video_kids' => 'c,r,u,d',
            'library_categories' => 'c,r,u,d',
            'libraries' => 'c,r,u,d',
            'prays' => 'c,r,u,d',
            'countries' => 'c,r,u,d',
            'cities' => 'c,r,u,d',
            'orphans' => 'c,r,u,d',
            'contactUs' => 'c,r,u,d',
            'dead' => 'c,r,u,d',
        ],

    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
