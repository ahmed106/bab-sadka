<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class AddOrphan extends Notification
{
    use Queueable;

    public $orphan;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orphan)
    {
        $this->orphan = $orphan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
//    public function toMail($notifiable)
//    {
//        return (new MailMessage)
//            ->line('The introduction to the notification.')
//            ->action('Notification Action', url('/'))
//            ->line('Thank you for using our application!');
//    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'msg' => 'تم إضافه يتيم جديد',
            'name' => $this->orphan['name'],
            'at' => $this->orphan['created_at'],
            'photo' => $this->orphan['photo'],
            'id' => $this->orphan['id']
        ];
    }
}
