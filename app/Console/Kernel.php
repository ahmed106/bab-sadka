<?php

namespace App\Console;

use App\Console\Commands\Asr;
use App\Console\Commands\Dhuhr;
use App\Console\Commands\Fajr;
use App\Console\Commands\GetPrayTime;
use App\Console\Commands\Isha;
use App\Console\Commands\Maghrap;
use App\Models\PrayTime;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{


    protected $commands = [
        GetPrayTime::class,
        Asr::class,
        Dhuhr::class,
        Maghrap::class,
        Isha::class,
        Fajr::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('pray-time')->daily();

        $dhuhr = Carbon::parse(PrayTime::where('timezone', date_default_timezone_get())->first()->dhuhr)->format('H:i');
        $asr = Carbon::parse(PrayTime::where('timezone', date_default_timezone_get())->first()->asr)->format('H:i');
        $marhrap = Carbon::parse(PrayTime::where('timezone', date_default_timezone_get())->first()->maghrab)->format('H:i');
        $isha = Carbon::parse(PrayTime::where('timezone', date_default_timezone_get())->first()->isha)->format('H:i');
        $fajr = Carbon::parse(PrayTime::where('timezone', date_default_timezone_get())->first()->fajr)->format('H:i');

        $schedule->command('adan-dhuhr')->dailyAt($dhuhr);
        $schedule->command('adan-asr')->dailyAt($asr);
        $schedule->command('adan-maghrap')->dailyAt($marhrap);
        $schedule->command('adan-isha')->dailyAt($isha);
        $schedule->command('adan-fajr')->dailyAt($fajr);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected
    function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
