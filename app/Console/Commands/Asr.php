<?php

namespace App\Console\Commands;

use App\Events\PrayNotification;
use Illuminate\Console\Command;

class Asr extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adan-asr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'adan el asr Time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        event(new PrayNotification('حان الآن صلاه العصر'));
    }
}
