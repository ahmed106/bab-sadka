<?php

namespace App\Console\Commands;

use App\Models\PrayTime;
use App\Traits\prayTimes;
use Illuminate\Console\Command;

class GetPrayTime extends Command
{
    use prayTimes;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pray-time';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get pray times';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \DB::table('pray_times')->truncate();

        $countries = [
            0 => [
                'name' => 'Egypt',
                'timezone' => 'Africa/Cairo'
            ],
            1 => [
                'name' => 'Sudia Arabia',
                'timezone' => 'Asia/Riyadh'
            ]
        ];
        foreach ($countries as $index => $country) {
            $pray_times = $this->prayTimes($countries[$index]['name']);
            PrayTime::create([
                'country' => $countries[$index]['name'],
                'timezone' => $countries[$index]['timezone'],
                'fajr' => $pray_times['fajr'],
                'dhuhr' => $pray_times['dhuhr'],
                'asr' => $pray_times['asr'],
                'maghrab' => $pray_times['maghrab'],
                'isha' => $pray_times['isha'],
            ]);
        }

    }
}
