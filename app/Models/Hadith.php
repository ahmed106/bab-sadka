<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Hadith extends Model
{
    use Translatable;

    protected $table = 'hadiths';
    protected $guarded = [];

    public $translatedAttributes = ['name','meta_title','meta_description','meta_keywords'];
}
