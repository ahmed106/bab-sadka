<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use Translatable;

    protected $table = 'countries';
    protected $guarded = [];

    protected $translatedAttributes = ['name'];

    public function cities()
    {
        return $this->hasMany(City::class);


    }//end of cities function
}
