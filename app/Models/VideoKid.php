<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class VideoKid extends Model
{
    use Translatable;

    protected $guarded = [];
    protected $table = 'video_kids';

    public $translatedAttributes = ['title','meta_title','meta_description','meta_keywords'];
}
