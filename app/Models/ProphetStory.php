<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProphetStory extends Model
{
    use Translatable;

    protected $table = 'prophet_stories';
    protected $guarded = [];

    public $translatedAttributes = ['name', 'story','meta_title','meta_description','meta_keywords'];
}
