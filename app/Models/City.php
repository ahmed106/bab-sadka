<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use PHPUnit\Framework\Constraint\Count;

class City extends Model
{
    use Translatable;

    protected $table = 'cities';
    protected $guarded = [];

    protected $translatedAttributes = ['name'];

    public function country()
    {

        return $this->belongsTo(Count::class, 'country_id');
    }//end of country function
}
