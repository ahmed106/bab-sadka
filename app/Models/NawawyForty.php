<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class NawawyForty extends Model
{
    use Translatable;

    protected $table = 'nawawy_forties';
    protected $guarded = [];

    public $translatedAttributes = ['title', 'content','meta_title','meta_description','meta_keywords'];


}
