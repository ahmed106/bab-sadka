<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Pray extends Model
{
    use Translatable;

    protected $table = 'prays';
    protected $guarded = [];

    protected $translatedAttributes = ['pray', 'title','meta_title','meta_description','meta_keywords'];


}
