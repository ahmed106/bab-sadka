<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrayTranslation extends Model
{
    protected $table = 'pray_translations';
    protected $guarded = [];

    public $timestamps = false;

    public function setMetaKeywordsAttribute($value)
    {
        return $this->attributes['meta_keywords']  = json_encode(explode(',',$value));

    }//end of setMetaKeywords function

    public function getMetaKeywordsAttribute($value)
    {
        if(!$value){
            return ;
        }
        return implode(',',json_decode($value));

    }//end of getMetaKeywordsAttribute function

}
