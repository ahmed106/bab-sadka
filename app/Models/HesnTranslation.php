<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HesnTranslation extends Model
{
    protected $table = 'hesn_translations';

    protected $guarded = [];

    public $timestamps = false;

    public function setMetaKeywordsAttribute($value)
    {
        return $this->attributes['meta_keywords']  = json_encode(explode(',',$value));

    }//end of setMetaKeywords function

    public function getMetaKeywordsAttribute($value)
    {
        return implode(',',json_decode($value));

    }//end of getMetaKeywordsAttribute function
}
