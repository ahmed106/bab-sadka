<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Library extends Model
{
    use Translatable;

    protected $table = 'libraries';
    protected $guarded = [];

    public $translatedAttributes = ['name','meta_title','meta_description','meta_keywords'];

    public function category()
    {

        return $this->belongsTo(LibraryCategory::class, 'category_id');

    }//end of category function


}
