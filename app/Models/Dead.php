<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dead extends Model
{
    protected $table = 'deads';
    protected $guarded = [];

    public function scopeApproved()
    {

        return $this->where('approval', 1);
    }//end of scopeApproval function


}
