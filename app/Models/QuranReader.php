<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class QuranReader extends Model
{
    use Translatable;

    protected $table = 'quran_readers';
    protected $guarded = [];

    public $translatedAttributes = ['name','meta_title','meta_description','meta_keywords'];

    public function surahs()
    {
        return $this->hasMany(Surah::class, 'reader_id');
    }//end of surahs function


}
