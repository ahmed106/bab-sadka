<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class LibraryCategory extends Model
{
    use Translatable;

    protected $table = 'library_categories';
    protected $guarded = [];

    public $translatedAttributes = ['name','meta_title','meta_description','meta_keywords'];

    public function books()
    {
        return $this->hasMany(Library::class, 'category_id', 'id');


    }//end of books function
}
