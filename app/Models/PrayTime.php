<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrayTime extends Model
{
    protected $table = 'pray_times';
    protected $guarded = [];
}
