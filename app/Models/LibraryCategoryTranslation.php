<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LibraryCategoryTranslation extends Model
{

    protected $table = 'library_category_translations';
    protected $guarded = [];

    public $timestamps = false;


    public function setMetaKeywordsAttribute($value)
    {
        return $this->attributes['meta_keywords']  = json_encode(explode(',',$value));

    }//end of setMetaKeywords function

    public function getMetaKeywordsAttribute($value)
    {
        return implode(',',json_decode($value));

    }//end of getMetaKeywordsAttribute function
}
