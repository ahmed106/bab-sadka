<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Surah extends Model
{
    use Translatable;

    protected $table = 'surahs';

    public $translatedAttributes = ['name','meta_title','meta_description','meta_keywords'];
    protected $guarded = [];

    public function reader()
    {
        return $this->belongsTo(QuranReader::class, 'reader_id');
    }//end of  function
}
