<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use Translatable;

    protected $table = 'settings';
    protected $guarded = [];

    protected $translatedAttributes = ['website_name', 'website_description', 'meta_description', 'meta_title', 'meta_keywords'];

}
