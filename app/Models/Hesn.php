<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;

use Illuminate\Database\Eloquent\Model;

class Hesn extends Model
{
    use Translatable;
    protected $table = 'hesns';
    protected $guarded = [];
    public $translatedAttributes = ['title','meta_title','meta_description','meta_keywords'];
}
