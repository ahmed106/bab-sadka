<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        $rules = [
            'country_id' => 'required|exists:countries,id|numeric'
        ];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|unique:city_translations,name|string'];
        }
        return $rules;
    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'country_id' => 'required|exists:countries,id|numeric'
        ];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|unique:city_translations,name,' . request()->id . ',city_id|string'];
        }
        return $rules;

    }//end of onUpdate function
}
