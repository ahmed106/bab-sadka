<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PrayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
           // 'audio' => 'required|file|mimes:mp3,mp4',
            'video' => 'required',
        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [
                $activeLanguage->locale . '.pray' => 'required|string',

            ];
        }

        return $rules;

    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
            'audio' => 'file|mimes:mp3,mp4',

        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [
                $activeLanguage->locale . '.pray' => 'required|string',
            ];
        }

        return $rules;

    }//end of onUpdate function
}
