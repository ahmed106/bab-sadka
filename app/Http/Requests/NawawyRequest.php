<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NawawyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        $rules = [
            'video_type' => 'required|in:file,url',
            'video' => 'required',
            'audio' => 'required|file|mimes:mp3,mp4',
        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [
                $activeLanguage->locale . '.title' => 'required|string|max:100',
                $activeLanguage->locale . '.content' => 'required',
            ];
        }

        return $rules;


    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'video_type' => 'required|in:file,url',
            'audio' => 'file|mimes:mp3,mp4',
        ];

        foreach (activeLanguages() as $activeLanguage) {

            $rules += [
                $activeLanguage->locale . '.title' => 'required|string|max:100',
                $activeLanguage->locale . '.content' => 'required',
            ];
        }

        return $rules;

    }//end of  function
}
