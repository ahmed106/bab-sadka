<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        $rules = [];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|unique:country_translations,name|string'];
        }
        return $rules;
    }//end of onStore function

    public function onUpdate()
    {
        $rules = [];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|unique:country_translations,name,' . request()->id . ',country_id|string'];
        }
        return $rules;

    }//end of onUpdate function
}
