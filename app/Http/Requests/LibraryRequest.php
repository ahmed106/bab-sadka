<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LibraryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {

        $rules = [
            'photo' => 'required|file|mimes:png,jpg,jpeg,gif',
            'pdf' => 'required|file|mimes:pdf',
            'category_id' => 'required|numeric|exists:library_categories,id'
        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|string|unique:library_translations,name'];
        }


        return $rules;

    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'photo' => 'file|mimes:png,jpg,jpeg,gif',
            'pdf' => 'file|mimes:pdf',
            'category_id' => 'required|numeric|exists:library_categories,id'
        ];


        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|string|unique:library_translations,name,' . request()->id . ',library_id'];
        }

        return $rules;

    }//end of onStore function
}
