<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HadithRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('post') ? $this->onStore() : $this->onUpdate();
    }

    public function onStore()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
            'video' => 'required',
            'audio' => 'required',
            'photo' => 'sometimes|nullable',
        ];

        foreach (activeLanguages() as $lang) {
            $rules += [$lang->locale . '.name' => 'required|string|unique:hadith_translations,name'];
        }

        return $rules;
    }//end of onStore function

    public function onUpdate()
    {

        $rules = [
            'video_type' => 'required|in:url,file',
            'photo' => 'sometimes|nullable',

        ];

        foreach (activeLanguages() as $lang) {
            $rules += [$lang->locale . '.name' => Rule::unique('hadith_translations','name')->ignore($this->id,'hadith_id')];
        }

        return $rules;
    }//end of onUpdate function
}
