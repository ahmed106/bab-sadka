<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HesnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
            'video' => 'required',
            'audio' => 'required|mimes:mp4,mp3'
        ];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.title' => 'required|string'];
        }

        return $rules;

    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
        ];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.title' => 'required|string'];
        }

        return $rules;

    }//end of onUpdate function
}
