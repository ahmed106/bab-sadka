<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuranReaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules():array
    {
        return request()->isMethod('post') ? $this->onStore() : $this->onUpdate();
    }

    public function onStore():array
    {
        $rules = [];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [
                $activeLanguage->locale . '.name' => 'required|unique:quran_reader_translations,name|string|max:150',
                $activeLanguage->locale . '.meta_title' => 'sometimes|nullable|string|max:150',
                $activeLanguage->locale . '.meta_keywords' => 'sometimes|nullable|string|max:150',
                $activeLanguage->locale . '.meta_description' => 'sometimes|nullable|string|max:255',
                ];
        }

        return $rules;
    }//end of store function


    public function onUpdate():array
    {


        $rules = [];
        foreach (activeLanguages() as $activeLanguage) {

            $rules += [
                $activeLanguage->locale . '.name' => ['required', 'string','max:150', Rule::unique('quran_reader_translations', 'name')->ignore(request()->id, 'quran_reader_id')],
                $activeLanguage->locale . '.meta_title' => 'sometimes|nullable|string|max:150',
                $activeLanguage->locale . '.meta_keywords' => 'sometimes|nullable|string|max:150',
                $activeLanguage->locale . '.meta_description' => 'sometimes|nullable|string|max:255',

            ];
        }

        return $rules;


    }//end of update function
}
