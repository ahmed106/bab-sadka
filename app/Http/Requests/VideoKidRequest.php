<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoKidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
            'video' => 'required',
        ];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.title' => 'required|string|max:100'];
        }
        return $rules;

    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'video_type' => 'required|in:url,file',
            'video' => 'sometimes|nullable',
        ];
        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.title' => 'required|string|max:100'];
        }
        return $rules;
    }//end of onUpdate function
}
