<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {

        return [
            'name' => 'required|string|unique:deads,name',
            'email' => 'sometimes:nullable',
            'phone' => 'required:integer',
            'death_date' => 'required|date',
            'country_id' => 'required|exists:countries,id',
            'city_id' => 'required|exists:cities,id',
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'name' => 'required|string|unique:deads,name,' . request()->id,
            'email' => 'sometimes:nullable',
            'phone' => 'required:integer',
            'death_date' => 'required|date',
            'country_id' => 'required|exists:countries,id',
            'city_id' => 'required|exists:cities,id',
        ];

    }//end of onUpdate function
}
