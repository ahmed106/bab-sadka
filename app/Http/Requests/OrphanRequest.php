<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrphanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }


    public function onStore()
    {
        return [
            'name' => 'required|string|min:4|unique:orphans,name',
            'id_number' => 'sometimes:nullable|min:11',
            'gender' => 'required|in:male,female',
            'birthdate' => 'required|date',
            'country_id' => 'required|exists:countries,id',
            'city_id' => 'required|exists:cities,id',
            'address' => 'required|string',
            'lost' => 'required|in:father,mother,both',
            'live_with' => 'required|string',
            'country_code' => 'required|starts_with:+20,+966',
            'phone' => 'required|numeric',
            'phone_owner' => 'required|string',
            'has_disease' => 'required|in:0,1',
            'disease_type' => 'required_if:has_disease,1|exclude_if:has_disease,0',
            'disease_cost' => 'required_if:has_disease,1|exclude_if:has_disease,0',
            'photo' => 'sometimes:nullable|file|mimes:png,jpg,jpeg,gif',
            'father_name' => 'required|string',
            'father_id' => 'required|integer|min:11',
            'father_death_date' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_reason' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_place' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_application' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_photo' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_education_level' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_job' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_salary' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'mother_name' => 'required|string',
            'mother_id' => 'required|integer|min:11',
            'mother_death_date' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_reason' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_place' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_application' => 'required_if:lost,mother|exclude_if:lost,father',
            'mother_photo' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_education_level' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_job' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_salary' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'name' => 'required|string|min:4|unique:orphans,name,' . request()->id,
            'id_number' => 'sometimes:nullable|min:11',
            'gender' => 'required|in:male,female',
            'birthdate' => 'required|date',
            'country_id' => 'required|exists:countries,id',
            'city_id' => 'required|exists:cities,id',
            'address' => 'required|string',
            'lost' => 'required|in:father,mother,both',
            'live_with' => 'required|string',
            'country_code' => 'required|starts_with:+20,+966',
            'phone' => 'required|numeric',
            'phone_owner' => 'required|string',
            'has_disease' => 'required|in:0,1',
            'disease_type' => 'required_if:has_disease,1|exclude_if:has_disease,0',
            'disease_cost' => 'required_if:has_disease,1|exclude_if:has_disease,0',
            'photo' => 'sometimes:nullable|file|mimes:png,jpg,jpeg,gif',
            'father_name' => 'required|string',
            'father_id' => 'required|integer|min:11',
            'father_death_date' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_reason' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_place' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_application' => 'sometimes:nullable|required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_photo' => 'sometimes:nullable|required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_education_level' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_job' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_salary' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'mother_name' => 'required|string',
            'mother_id' => 'required|integer|min:11',
            'mother_death_date' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_reason' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_place' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_application' => 'sometimes:nullable|required_if:lost,mother|exclude_if:lost,father',
            'mother_photo' => 'sometimes:nullable|required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_education_level' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_job' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_salary' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',

        ];

    }//end of onUpdate function
}
