<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('post') ? $this->onStore() : $this->onUpdate();
    }

    public function onStore()
    {
        $rules = [
            'video_type' => 'required|string|in:url,file',
            'reader_id' => 'required|exists:quran_readers,id',
            'video' => 'sometimes|nullable',
            'audio' => 'required',
        ];
        foreach (activeLanguages() as $language) {
            $rules += [$language->locale . '.name' => ['required', 'string', 'max:255']];

        }
        return $rules;

    }//end of onStore function

    public function OnUpdate()
    {
        $rules = [
            'video_type' => 'required|string|in:url,file',
            'reader_id' => 'required|exists:quran_readers,id',
        ];
        foreach (activeLanguages() as $language) {
            $rules += [$language->locale . '.name' => ['required', 'string', 'max:255']];

        }


        return $rules;

    }//end of OnUpdate function
}
