<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {

        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:contact_us,email',
            'body' => 'required',
            'phone' => 'sometimes|nullable',
        ];

        return $rules;

    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email|unique:contact_us,email,' . request()->id, ',email',
            'body' => 'required',
            'phone' => 'sometimes|nullable',
        ];

        return $rules;
    }//end of onUpdate function
}
