<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LibraryCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {

        $rules = [
            'photo' => 'required|file|mimes:png,jpg,jpeg,gif',
        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|string|unique:library_category_translations,name'];
        }


        return $rules;

    }//end of onStore function

    public function onUpdate()
    {
        $rules = [
            'photo' => 'file|mimes:png,jpg,jpeg,gif',
        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.name' => 'required|string|unique:library_category_translations,' . request()->id . ',library_category_id'];
        }

        return $rules;

    }//end of onStore function
}
