<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use Illuminate\Support\Facades\Session;

class MasbahaController extends Controller
{
    public function index()
    {
        if (\request('D')) {
            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }
        return view('website.masbaha.index');
    }//end of index function
}
