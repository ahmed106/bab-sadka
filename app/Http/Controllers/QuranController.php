<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use App\Models\QuranReader;
use App\Models\Surah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class QuranController extends Controller
{
    public function index()
    {

        if (\request('D')) {
            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }

        return view('website.quran.index');
    }//end of index function

    public function read()
    {

        return view('website.quran.read');

    }//end of read function

    public function listen(Request $request)
    {
        $reader_id = $request->reader_id;
        $readers = QuranReader::query()->withTranslation()->get();

        if ($request->reader_id!=null){
            $surahs =Surah::withTranslation()->whereReaderId($reader_id)->get();
            $surahs = $reader_id ?
                : Surah::withTranslation()->whereHas('reader', function ($q) use ($readers) {
                    count($readers) > 0 ? $q->where('id', $readers->first()->id) : '';

                })->get();
        }else{
          $surahs =  Surah::withTranslation()->whereHas('reader', function ($q) use ($readers) {
              count($readers) > 0 ? $q->where('id', $readers->first()->id) : '';

          })->get();
        }

        $surahs = $surahs ?? abort(403);
        return view('website.quran.listen', compact('readers', 'surahs'));

    }//end of list function

    public function videos(Request $request)
    {

        $reader_id = $request->reader_id;

        $readers = QuranReader::with('translations', 'surahs', 'surahs.translations')->simplePaginate(16);


        $surahs = $reader_id ? Surah::with('translations', 'reader.translations')->whereReaderId($reader_id)->simplePaginate(16)
            : Surah::with('translations', 'reader', 'reader.translations')->whereHas('reader', function ($q) use ($readers) {
                count($readers) > 0 ? $q->where('id', $readers->first()->id) : '';
            })->simplePaginate(16);

        $surahs = $surahs ?? abort(403);

        return view('website.quran.videos', compact('readers', 'surahs'));

    }//end of list function

    public function getSurah(Request $request)
    {
        $surah = $request->surah;
        $reader = $request->reader;
        $surahs = Surah::with(['reader', 'translations'])->when($request->surah, function ($q) use ($reader, $surah) {
            if (isset($surah) && $surah != '') {
                $q->whereTranslationLike('name', '%' . $surah . '%');
            }
        })->where('reader_id', $reader)->get();


        $view = View::make('website.quran.surahs', ['surahs' => $surahs])->render();

        return response()->json([
            'view' => $view,
        ], 200);

    }//end of getSurah function

    public function getVideos(Request $request)
    {
        $surah = $request->surah;
        $reader = $request->reader;
        $surahs = Surah::with(['reader', 'translations'])->when($request->surah, function ($q) use ($reader, $surah) {
            if (isset($surah) && $surah != '') {
                $q->whereTranslationLike('name', '%' . $surah . '%');
            }
        })->where('reader_id', $reader)->get();


        $view = View::make('website.quran.list_videos', ['surahs' => $surahs])->render();

        return response()->json([
            'view' => $view,
        ], 200);

    }//end of getSurah function

    public function show($id)
    {
        $surah = Surah::findOrFail($id);
        $surahs = Surah::get();

        return view('website.quran.show', compact('surah', 'surahs'));


    }//end of show function
}
