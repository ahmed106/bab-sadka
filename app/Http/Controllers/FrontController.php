<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsRequest;
use App\Models\City;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Dead;
use App\Models\PrayTime;
use App\Traits\prayTimes;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    use prayTimes;

    public function index(Request $request)
    {

        if (\session('dead')) {
            $dead_id = \session('dead.id');

            return redirect('/D/' . base64_encode($dead_id));
        }

        $pray_times = PrayTime::where('timezone', date_default_timezone_get())->first();
        foreach ($pray_times as $pray_time) {
            $pray_times['fajr'] = Carbon::create($pray_times['fajr'])->format('h:i A');
            $pray_times['dhuhr'] = Carbon::create($pray_times['dhuhr'])->format('h:i A');
            $pray_times['asr'] = Carbon::create($pray_times['asr'])->format('h:i A');
            $pray_times['maghrab'] = Carbon::create($pray_times['maghrab'])->format('h:i A');
            $pray_times['isha'] = Carbon::create($pray_times['isha'])->format('h:i A');
        }


        $countries = Country::all();
        $deads = Dead::all();

        return view('website.index', compact('countries', 'deads', 'pray_times'));
    }//end of index function

    public function getCity(Request $request)
    {
        $cities = City::where('country_id', $request->country_id)->get();
        return response()->json(['data' => $cities], 200);
    }//end of getCity function

    public function getDeads(Request $request)
    {

        $deads = Dead::Approved()->where('city_id', $request->city_id)->get();

        return response()->json(['data' => $deads], 200);
    }//end of getCity function

    public function contact(ContactUsRequest $request)
    {

        $data = $request->validated();

        ContactUs::create($data);
        return redirect()->back()->with('success', 'تم إرسال طلبك بنجاح');

    }//end of contact function

    public function getPrayTimes()
    {
        date_default_timezone_set(request()->timezone);
        $pray_times = PrayTime::where('timezone', date_default_timezone_get())->first();
        foreach ($pray_times as $pray_time) {
            $pray_times['fajr'] = Carbon::create($pray_times['fajr'])->format('h:i A');
            $pray_times['dhuhr'] = Carbon::create($pray_times['dhuhr'])->format('h:i A');
            $pray_times['asr'] = Carbon::create($pray_times['asr'])->format('h:i A');
            $pray_times['maghrab'] = Carbon::create($pray_times['maghrab'])->format('h:i A');
            $pray_times['isha'] = Carbon::create($pray_times['isha'])->format('h:i A');
        }

        $view = \Illuminate\Support\Facades\View::make('website.includes.pray_times', compact('pray_times'))->render();

        return response()->json(['data' => $view], 200);
    }
}
