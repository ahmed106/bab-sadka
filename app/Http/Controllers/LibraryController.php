<?php

namespace App\Http\Controllers;

use App\Models\Library;
use App\Models\LibraryCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class LibraryController extends Controller
{
    public function index()
    {
        $categories = LibraryCategory::query()->get();
        return view('website.libraries.index', compact('categories'));

    }//end of index function

    public function showCategory($id)
    {

        $books = LibraryCategory::with('translations', 'books')->findOrFail($id)->books;


        return view('website.libraries.books', compact('books'));


    }//end of showCategory function

    public function readBook($id)
    {
        $book = Library::findOrFail($id);

        return view('website.libraries.read', compact('book'));


    }//end of readBook function

    public function bookSearch(Request $request)
    {
        $book_name = $request->book_name;
        $cat_id = $request->cat_id;
        $books = Library::with('translations')->when($request->book_name, function ($q) use ($book_name, $cat_id) {
            if (isset($book_name) && $book_name != '') {
                $q->whereTranslationLike('name', '%' . $book_name . '%');

            }

        })->whereCategoryId($cat_id)->get();
        $view = View::make('website.libraries.search', compact('books'))->render();

        return response()->json(['view' => $view], 200);

    }//end of bookSearch function
}
