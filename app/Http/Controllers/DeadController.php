<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Dead;
use App\Models\PrayTime;
use App\Traits\upload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class DeadController extends Controller
{
    use upload;

    public function create()
    {
        $countries = Country::query()->get();
        return view('website.dead.create', compact('countries'));


    }//end of create function

    public function store(Request $request)
    {

        $data = $request->except('_token');

        $validaor = Validator::make($request->all(), [
            'name' => 'required|string',
            'phone' => 'required|numeric',
            'death_date' => 'required|date',
            'country_id' => 'required|exists:countries,id',
            'city_id' => 'required|exists:cities,id',
            'email' => 'required|email',
            'photo' => 'sometimes|nullable|file|mimes:png,jpg,jpeg',
        ]);

        if ($validaor->fails()) {
            return response()->json(['error' => $validaor->errors()->first()], '401');
        }


        $data['slug'] = \Str::slug($data['name']);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'deads');
            $data['photo'] = $photo;
        }
        Dead::create($data);

        if (request()->ajax()) {

            return response()->json('success', '200');
        }
        return redirect('/')->with('success', 'تم إضافه متوفي بنجاح , من فضلك إنتظر موافقه الإداره');

    }//end of store function

    public function show($id)
    {

        $id = base64_decode($id);

        $dead = \DB::select('select name,id,photo from deads where id = "' . $id . '" limit 1');
        $dead = $dead[0];
        Session::forget('dead');
        $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
        Session::put(['dead' => $dead_session]);

        date_default_timezone_set('Asia/Riyadh');
        $pray_times = PrayTime::where('timezone', date_default_timezone_get())->first();


        //dd(date_default_timezone_get());


        foreach ($pray_times as $pray_time) {
            $pray_times['fajr'] = Carbon::create($pray_times['fajr'])->format('h:i A');
            $pray_times['dhuhr'] = Carbon::create($pray_times['dhuhr'])->format('h:i A');
            $pray_times['asr'] = Carbon::create($pray_times['asr'])->format('h:i A');
            $pray_times['maghrab'] = Carbon::create($pray_times['maghrab'])->format('h:i A');
            $pray_times['isha'] = Carbon::create($pray_times['isha'])->format('h:i A');
        }

        return view('website.dead.show', compact('dead', 'pray_times'));

    }//end of show function
}
