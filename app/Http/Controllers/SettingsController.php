<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Dead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{
    public function edit()
    {
        $deads = Dead::approved()->get();

        $countries = Country::query()->get();
        return view('website.settings.index', compact('deads', 'countries'));

    }//end of edit function

    public function store(Request $request)
    {
		
		
        $validator = Validator::make($request->all(), [
            'country' => 'required|exists:countries,id',
            'city' => 'required|exists:cities,id',
            'dead-name' => 'required|exists:deads,name'
        ]);

        if ($validator->fails()) {

            if ($request->ajax_type =="ajax") {

                return response()->json(['errors' => $validator->errors()->first()], 401);
            }

            return redirect()->back()->with('error', $validator->errors()->first());

        }

        $dead = Dead::where('name', $request['dead-name'])->first();
        $dead_id = base64_encode($dead->id);

        if ($request->ajax_type =="ajax") {
            $data = [
                'name' => $dead->name,
                'id' => $dead_id,
            ];

 		return response()->json(['data' => $data], 200);
          
        }else{
		
		return redirect('/D/' . $dead_id)->with('success', 'تم تعديل البيانات بنجاح');
	
		}

    }//end of store function
}
