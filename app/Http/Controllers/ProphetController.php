<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use App\Models\ProphetStory;
use Illuminate\Support\Facades\Session;

class ProphetController extends Controller
{
    public function index()
    {


        if (\request('D')) {

            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }

        return view('website.prophets.index');

    }//end of index function

    public function read()
    {

        $prophets = ProphetStory::query()->with('translations')->get();
        return view('website.prophets.read', compact('prophets'));

    }//end of read function

    public function listen()
    {
        $prophets = ProphetStory::query()->get();
        return view('website.prophets.listen', compact('prophets'));

    }//end of listen function

    public function videos()
    {

        $paginator = 12;
        $videos_count = ProphetStory::query()->count();
        $pages = ceil($videos_count / $paginator);
        $prophets = ProphetStory::query()->withTranslation()->simplePaginate($paginator);
        return view('website.prophets.videos', compact('prophets', 'pages', 'paginator'));


    }//end of videos function

    public function show($id)
    {
        $prophet_story = ProphetStory::with('translations')->findOrFail($id);

        $stories = ProphetStory::get();

        return view('website.prophets.show', compact('prophet_story', 'stories'));

    }//end of show function
}
