<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth:admin', 'permission:read_roles'])->only('index');
        $this->middleware(['auth:admin', 'permission:create_roles'])->only('create');
        $this->middleware(['auth:admin', 'permission:update_roles'])->only('edit');
        $this->middleware(['auth:admin', 'permission:delete_roles'])->only('delete');

    }//end of __construct function

    public function index()
    {

        return view('dashboard.roles.index');
    }

    public function data()
    {

        $roles = Role::OrderBy('id', 'desc')->get();


        return DataTables::of($roles)
            ->addColumn('check_all', function ($raw) {

                return '<input name="items[]" value="' . $raw->id . '" class="checkedBtn" type="checkbox">';
            })
            ->addColumn('actions', function ($row) {
                return view('dashboard.roles.actions', compact('row'));
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
            'permissions' => 'required|array|min:1'
        ]);
        $display_name = ucwords(str_replace('_', ' ', $request->name));


        $role = Role::create([
            'name' => $request->name,
            'display_name' => $display_name,
            'description' => $display_name
        ]);

        $role->attachPermissions($request->permissions);


        return redirect()->route('roles.index')->with('success', 'data store successfully');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $role = Role::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }


        return view('dashboard.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $request->validate([
            'name' => ['required', Rule::unique('roles')->ignore($id)],
            'permissions' => 'required|array|min:1'
        ]);
        $display_name = ucwords(str_replace('_', ' ', $request->name));

        $role->update([
            'name' => $request->name,
            'display_name' => $display_name,
            'description' => $display_name
        ]);

        $role->syncPermissions($request->permissions);

        return redirect()->route('roles.index')->with('success', 'data updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return redirect(route('roles.index'))->with('success', 'data deleted  successfully');
    }

    public function bulkDelete(Request $request)
    {
        $ids = $request->items;

        Role::destroy($ids);

        return redirect()->route('roles.index')->with('success', 'data Deleted Successfully');


    }//end of bulkDelete function
}
