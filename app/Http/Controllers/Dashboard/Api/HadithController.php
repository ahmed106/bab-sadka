<?php

namespace App\Http\Controllers\Dashboard\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\HadithRequest;
use App\Models\Hadith;
use App\Services\HadithService;

class HadithController extends Controller
{


    public $service;

    public function __construct(HadithService $service)
    {

        $this->service = $service;

    }//end of __construct function


    public function store(HadithRequest $request)
    {
        $data = $this->service->handleUploads($request);
        Hadith::create($data);
        return redirect()->route('hadiths.index')->with('success', 'data stored successfully');
    }


    public function update(HadithRequest $request, $id)
    {
        $data = $this->service->handleUploads($request, $id);
        $hadith = Hadith::find($id);
        $hadith->update($data);

        return redirect()->route('hadiths.index')->with('success', 'data updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hadith = Hadith::find($id);
        $this->service->deleteFiles($hadith);
        $hadith->delete();
        return redirect()->back()->with('success', 'data deleted successfully');
    }

    public function bulkDelete()
    {
        $this->service->deleteItems(request()->items);
        Hadith::destroy(request()->items);

        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of bulkDelete function
}
