<?php

namespace App\Http\Controllers\Dashboard\Api;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{

    public function __construct()
    {

        $this->middleware(['auth:admin-api', 'permission:read_languages'])->only('index');
        $this->middleware(['auth:admin-api', 'permission:create_languages'])->only('create');
        $this->middleware(['auth:admin-api', 'permission:update_languages'])->only('edit');
        $this->middleware(['auth:admin-api', 'permission:delete_languages'])->only('delete');

    }//end of __construct function


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:languages,name',
            'locale' => 'required|unique:languages,locale',
            'country_abbreviation' => 'required|unique:languages,country_abbreviation'
        ]);
        $data = $request->except(['_token']);

        Language::create($data);
        return redirect()->route('languages.index')->with('success', 'data stored successfully');


    }//end of store function


    public function update(Request $request, $id)
    {

        $lang = Language::findOrFail($id);
        $request->validate([
            'name' => 'required|unique:languages,name,' . $id,
            'locale' => 'required|unique:languages,locale,' . $id,
            'country_abbreviation' => 'required|unique:languages,country_abbreviation,' . $id
        ]);
        $data = $request->except(['_token', '_method']);
        if (!isset($data['active'])) {
            $data['active'] = 0;

        }//end if statement

        $lang->update($data);
        return redirect()->route('languages.index')->with('success', 'data updated successfully');


    }//end of update function

    public function active(Request $request)
    {
        $lang = Language::find($request->id);
        $lang->update([
            'active' => $request->active,
        ]);

        return response()->json(['data' => 'data updated successfully'], 200);

    }//end of active function


    public function destroy($id)
    {

        $lang = Language::findOrFail($id);
        $lang->delete();
        return redirect()->route('languages.index')->with('success', 'data deleted successfully');

    }//end of delete function

    public function bulkDelete(Request $request)
    {

        Language::destroy($request->items);
        return redirect()->route('languages.index')->with('success', 'data deleted successfully');

    }//end of bulkDelete function
}
