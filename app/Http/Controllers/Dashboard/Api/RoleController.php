<?php

namespace App\Http\Controllers\Dashboard\Api;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth:admin-api', 'permission:read_roles'])->only('index');
        $this->middleware(['auth:admin-api', 'permission:create_roles'])->only('create');
        $this->middleware(['auth:admin-api', 'permission:update_roles'])->only('edit');
        $this->middleware(['auth:admin-api', 'permission:delete_roles'])->only('delete');

    }//end of __construct function


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles,name',
            'permissions' => 'required|array|min:1'
        ]);
        $display_name = ucwords(str_replace('_', ' ', $request->name));


        $role = Role::create([
            'name' => $request->name,
            'display_name' => $display_name,
            'description' => $display_name
        ]);

        $role->attachPermissions($request->permissions);


        return redirect()->route('roles.index')->with('success', 'data store successfully');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $request->validate([
            'name' => ['required', Rule::unique('roles')->ignore($id)],
            'permissions' => 'required|array|min:1'
        ]);
        $display_name = ucwords(str_replace('_', ' ', $request->name));

        $role->update([
            'name' => $request->name,
            'display_name' => $display_name,
            'description' => $display_name
        ]);

        $role->syncPermissions($request->permissions);

        return redirect()->route('roles.index')->with('success', 'data updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return redirect(route('roles.index'))->with('success', 'data deleted  successfully');
    }

    public function bulkDelete(Request $request)
    {
        $ids = $request->items;

        Role::destroy($ids);

        return redirect()->route('roles.index')->with('success', 'data Deleted Successfully');


    }//end of bulkDelete function
}
