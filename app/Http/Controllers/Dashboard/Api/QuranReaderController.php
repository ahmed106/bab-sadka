<?php

namespace App\Http\Controllers\Dashboard\Api;

use App\Http\Controllers\Controller;
use App\Models\QuranReader;
use App\Traits\ApiResponseHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class QuranReaderController extends Controller
{

    use ApiResponseHelper;

    public function store(Request $request)
    {

        foreach (activeLanguages() as $activeLanguage) {
            $validator = Validator::make($request->all(), [

                $activeLanguage->locale . '_name' => 'required|unique:quran_reader_translations,name|string|max:30',
            ]);
            if ($validator->fails()) {

                return $this->error($validator->errors()->first());
            }

        }
        $quran_reader = new QuranReader();
        $quran_reader->save();
        foreach (activeLanguages() as $activeLanguage) {

            $quran_reader->translation()->create([
                'name' => $request[$activeLanguage->locale . '_name'],
                'locale' => $activeLanguage->locale,
            ]);
        }
        return $this->setDate($quran_reader)->setSuccess('data stored successfully')->setCode(200)->send();

    }

    public function update(Request $request, $id)
    {
        $quran_reader = QuranReader::find($id);
        foreach (activeLanguages() as $activeLanguage) {

            $validator = Validator::make($request->all(), [

                $activeLanguage->locale . '_name' => ['required', 'string', 'max:30', 'unique:quran_reader_translations,name,' . $id . ',quran_reader_id'],
            ]);
            if ($validator->fails()) {

                return $this->error($validator->errors()->first());
            }

        }
        $quran_reader->translations()->delete();
        foreach (activeLanguages() as $activeLanguage) {
            $quran_reader->translation()->create([
                'name' => $request[$activeLanguage->locale . '_name'],
                'locale' => $activeLanguage->locale,
            ]);
        }

        return $this->setDate($quran_reader)->setSuccess('data updated successfully')->setCode(200)->send();
    }


    public function destroy($id)
    {
        $reader = QuranReader::find($id);
        if ($reader) {

            $reader->delete();
            return $this->setSuccess('data deleted successfully')->setCode(200)->send();
        }


        return $this->setError('quran reader not found ')->setStatus('error')->send();

    }

    public function bulkDelete()
    {
        QuranReader::destroy(\request()->items);
        toastr()->success('data deleted Successfully');
        return redirect()->back();
    }//end of bulck_delete function


}
