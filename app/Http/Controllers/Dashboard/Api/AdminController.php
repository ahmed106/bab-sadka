<?php

namespace App\Http\Controllers\Dashboard\Api;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Traits\upload;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    use upload;

    public function __construct()
    {
        $this->middleware(['auth:admin-api', 'permission:read_admins'])->only('index');
        $this->middleware(['auth:admin-api', 'permission:create_admins'])->only('create');
        $this->middleware(['auth:admin-api', 'permission:update_admins'])->only('edit');
        $this->middleware(['auth:admin-api', 'permission:delete_admins'])->only('delete');
    }


    public function store(Request $request)
    {
        $data = $request->except('_token', 'password', 'role', 'password_confirmation');


        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins,email',
            'password' => 'required|confirmed',
            'role' => 'integer|required',
        ]);
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }//end if statement
        if ($request->hasFile('photo')) {
            $name = $this->upload($request->photo, 'admins');
            $data['photo'] = $name;

        }//end if statement

        $admin = Admin::create($data);
        $admin->attachRole($request->role);
        return redirect()->route('admins.index')->with('success', 'data stored successfully');


    }


    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $data = $request->except(['_token', '_method', 'role']);


        if ($request->password != null) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data['password'] = $admin->password;
        }
        if ($request->hasFile('photo')) {

            $this->deleteoldPhoto($admin->photo, 'admins');
            $name = $this->upload($request->photo, 'admins');
            $data['photo'] = $name;

        }//end if statement

        $admin->update($data);

        $admin->syncRoles([$request->role]);

        return redirect()->route('admins.index')->with('success', 'data updated successfully');
    }


    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $this->deleteoldPhoto($admin->photo, 'admins');
        $admin->delete();
        return redirect()->route('admins.index')->with('success', 'data deleted successfully');
    }

    public function bulkDelete(Request $request)
    {

        foreach ($request->items as $item) {
            $admin = Admin::find($item);
            $this->deleteoldPhoto($admin->photo, 'admins');
        }
        Admin::destroy($request->items);

        return redirect()->route('admins.index')->with('success', 'data deleted successfully');


    }//end of bulkDelete function
}
