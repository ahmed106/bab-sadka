<?php

namespace App\Http\Controllers\Dashboard\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SurahRequest;
use App\Models\Surah;
use App\Services\SurahService;

class SurahController extends Controller
{

    public $service;

    public function __construct(SurahService $service)
    {
        $this->service = $service;
    }


    public function store(SurahRequest $request)
    {
        $data = $this->service->handleUploads($request);
        Surah::create($data);
        return redirect()->route('surahs.index')->with('success', 'data stored successfully');

    }//end of store function


    public function update(SurahRequest $request, $id)
    {
        $data = $this->service->handleUploads($request, $id);
        $surah = Surah::find($id);

        $surah->update($data);
        return redirect()->route('surahs.index')->with('success', 'data stored successfully');

    }//end of update function

    public function destroy($id)
    {
        $surah = Surah::find($id);
        $this->service->deleteFiles($surah);
        $surah->delete();
        return redirect()->route('surahs.index')->with('success', 'data delete successfully');
    }//end of destroy function

    public function bulkDelete()
    {
        $this->service->deleteItems(request()->items);
        Surah::destroy(request()->items);

        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of bulkDelete function

    public function getYoutubeVideo()
    {
        $video = getYoutubeId(\request()->video);
        return response()->json(['video' => $video], 200);
    }//end of getYoutubeVideo function


}
