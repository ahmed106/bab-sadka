<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsRequest;
use App\Models\ContactUs;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class ContactUsController extends Controller
{

    public function index()
    {

        return view('dashboard.contactus.index');
    }

    public function data()
    {
        $contactUs = ContactUs::query()->get();
        return DataTables::of($contactUs)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.contactus.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" name="items[]" class="checkedBtn" value="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function

    public function create()
    {
        return view('dashboard.contactus.create');
    }


    public function store(ContactUsRequest $request)
    {
        $data = $request->validated();
        ContactUs::create($data);
        return redirect()->route('contact-us.index')->with('success', 'data stored successfully');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $contactUs = ContactUs::query()->findOrFail(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        return view('dashboard.contactus.edit', compact('contactUs'));
    }

    public function update(ContactUsRequest $request, $id)
    {
        $contactUs = ContactUs::query()->findOrFail($id);
        $data = $request->validated();
        $contactUs->update($data);
        return redirect()->route('contact-us.index')->with('success', 'data updated successfully');
    }


    public function destroy($id)
    {
        $contactUs = ContactUs::query()->findOrFail($id);
        $contactUs->delete();
        return redirect()->back()->with('success', 'data deleted successfully');
    }

    public function bulkDelete()
    {
        ContactUs::destroy(request()->items);
        return redirect()->back()->with('success', 'data deleted successfully');
    }//end of bulkDelete function
}
