<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\VideoKidRequest;
use App\Models\VideoKid;
use App\Services\VideoKidService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class VideoKidController extends Controller
{

    public $service;

    public function __construct(VideoKidService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('dashboard.video_kids.index');
    }//end of index function

    public function data()
    {
        $videos = VideoKid::with('translations')->OrderBy('id', 'desc')->get();

        return DataTables::of($videos)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.video_kids.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input class="checkedBtn" type="checkbox" name="items[]" value="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);


    }//end of data function

    public function create()
    {
        return view('dashboard.video_kids.create');


    }//end of create function

    public function store(VideoKidRequest $request)
    {

        $data = $this->service->handleUploads($request);

        VideoKid::create($data);

        return redirect()->route('video-kids.index')->with('success', 'data stored successfully');
    }//end of store function

    public function edit($id)
    {

        try {
            $video = VideoKid::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        return view('dashboard.video_kids.edit', compact('video'));

    }//end of edit function

    public function update(VideoKidRequest $request, $id)
    {

        $data = $this->service->handleUploads($request, $id);
        $video = VideoKid::find($id);
        $video->update($data);

        return redirect()->route('video-kids.index')->with('success', 'data stored successfully');


    }//end of update function

    public function destroy($id)
    {

        $video = VideoKid::find($id);
        $this->service->deleteFiles($video);
        $video->delete();
        return redirect()->route('video-kids.index')->with('success', 'data deleted successfully');


    }//end of destroy function

    public function bulkDelete()
    {

        $this->service->deleteItems(request()->items);
        VideoKid::destroy(request()->items);
        return redirect()->route('video-kids.index')->with('success', 'data deleted successfully');


    }//end of delete function
}
