<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuranReaderRequest;
use App\Models\QuranReader;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\EncryptException;
use Yajra\DataTables\Facades\DataTables;

class QuranReaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('dashboard.quran_readers.index');
    }

    public function data()
    {
        $readers = QuranReader::OrderBy('id', 'desc')->get();

        return DataTables::make($readers)
            ->addColumn('check_all', function ($raw) {
                return '<input name="items[]" value="' . $raw->id . '" class="checkedBtn" type="checkbox">';
            })
            ->addColumn('actions', function ($raw) {
                return view('dashboard.quran_readers.actions', compact('raw'));
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);


    }//end of data function

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.quran_readers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuranReaderRequest $request)
    {


        QuranReader::create($request->validated());
        toastr()->success('data stored successfully');
        return redirect()->route('quran-readers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $id = decrypt($id);
        } catch (DecryptException $exception) {
           abort(404);
        }
        $reader = QuranReader::with('translation')->find($id);
        return view('dashboard.quran_readers.edit', compact('reader'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuranReaderRequest $request, $id)
    {
        $reader = QuranReader::find($id);
        $reader->update($request->validated());

        toastr()->success('data updated successfully');
        return redirect()->route('quran-readers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reader = QuranReader::find($id);

        $reader->delete();
        toastr()->success('data deleted Successfully');
        return redirect()->back();
    }

    public function bulkDelete()
    {
        QuranReader::destroy(\request()->items);
        toastr()->success('data deleted Successfully');
        return redirect()->back();
    }//end of bulck_delete function
}
