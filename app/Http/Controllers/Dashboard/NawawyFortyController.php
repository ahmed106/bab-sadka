<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\NawawyRequest;
use App\Models\NawawyForty;
use App\Services\NawawyService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class NawawyFortyController extends Controller
{

    protected $nawawyService;

    public function __construct(NawawyService $service)
    {

        $this->nawawyService = $service;

    }//end of __construct function

    public function index()
    {

        return view('dashboard.naway_forty.index');
    }

    public function data()
    {

        $nawawy = NawawyForty::with('translations')->OrderBy('id', 'desc')->get();

        return DataTables::of($nawawy)
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" class="checkedBtn" name="items[]" value="' . $raw->id . '">';
            })
            ->addColumn('actions', function ($raw) {
                return '<a class="btn btn-success" href="' . route('nawawy-forty.edit', encrypt($raw->id)) . '"><i class="ri-pencil-line mr-0"></i> تعديل</a>
                        <a onclick="return confirm(\'are you sure !\')" class="btn btn-danger" href="' . route('nawawy-forty.delete', $raw->id) . '"><i class="fa fa-trash mr-0"></i> حذف</a>';
            })
            ->rawColumns(['check_all' => 'check_all', 'actions' => 'actions'])
            ->make(true);

    }//end of data function

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.naway_forty.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NawawyRequest $request)
    {

        $data = $this->nawawyService->handleUploads($request);


        NawawyForty::create($data);

        return redirect()->route('nawawy-forty.index')->with('success', 'data created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $nawawy = NawawyForty::find(decrypt($id));
        }catch (DecryptException$exception){
            abort(404);
        }

        return view('dashboard.naway_forty.edit', compact('nawawy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NawawyRequest $request, $id)
    {
        $data = $this->nawawyService->handleUploads($request, $id);
        $nawawy = NawawyForty::find($id);
        $nawawy->update($data);

        return redirect()->route('nawawy-forty.index')->with('success', 'data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function delete($id)
    {
        $nawawy = NawawyForty::find($id);
        $this->nawawyService->deleteFiles($nawawy);
        $nawawy->delete();
        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of delete function

    public function destroy($id)
    {

    }

    public function bulkDelete()
    {
        $this->nawawyService->deleteItems(request()->items);

        NawawyForty::destroy(request()->items);

        return redirect()->back()->with('success', 'data deleted successfully');


    }//end of bulkDelete function
}
