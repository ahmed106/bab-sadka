<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CityRequest;
use App\Models\City;
use App\Models\Country;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.settings.cities.index');
    }

    public function data()
    {
        $cities = City::OrderBy('id', 'desc')->get();
        return DataTables::of($cities)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.settings.cities.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" class="checkedBtn" name="items[]" value="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::query()->with('translations')->get();

        return view('dashboard.settings.cities.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $data = $request->validated();
        City::create($data);

        return redirect()->route('cities.index')->with('success', 'data stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $city = City::query()->findOrFail(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        $countries = Country::query()->with('translations')->get();

        return view('dashboard.settings.cities.edit', compact('city', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        $city = City::query()->findOrFail($id);
        $data = $request->validated();
        $city->update($data);

        return redirect()->route('cities.index')->with('success', 'data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::query()->findOrFail($id);
        $city->delete();
        return redirect()->route('cities.index')->with('success', 'data deleted successfully');

    }

    public function bulkDelete()
    {
        City::destroy(request()->items);
        return redirect()->route('cities.index')->with('success', 'data deleted successfully');


    }//end of bulkDelete function
}
