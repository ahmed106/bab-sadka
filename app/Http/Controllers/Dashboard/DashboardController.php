<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Traits\prayTimes;

class DashboardController extends Controller
{

    use prayTimes;

    public function index()
    {
        return view('dashboard.index');

    }//end of home function

    public function prayTime($country = null)
    {
        $prayTimes = $this->prayTimes($country);

        dd($prayTimes);
    }//end of prayTime function


}
