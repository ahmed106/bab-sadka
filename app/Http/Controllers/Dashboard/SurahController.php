<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\SurahRequest;
use App\Models\QuranReader;
use App\Models\Surah;
use App\Services\SurahService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class SurahController extends Controller
{

    public $service;

    public function __construct(SurahService $service)
    {
        $this->service = $service;
    }

    public function index()
    {

        return view('dashboard.surahs.index');

    }//end of index function

    public function data()
    {
        $surahs = Surah::with('translations')->OrderBy('id', 'desc')->get();

        return DataTables::of($surahs)
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" value="' . $raw->id . '" name="items[]" class="checkedBtn">';
            })
            ->addColumn('reader', function ($raw) {
                return $raw->reader->name;
            })
            ->addColumn('actions', function ($raw) {
                return view('dashboard.surahs.actions', compact('raw'));
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function

    public function create()
    {
        $readers = QuranReader::all();
        return view('dashboard.surahs.create', compact('readers'));

    }//end of create function

    public function store(SurahRequest $request)
    {
        $data = $this->service->handleUploads($request);
        Surah::create($data);
        return redirect()->route('surahs.index')->with('success', 'data stored successfully');

    }//end of store function

    public function edit($id)
    {
        $readers = QuranReader::all();
        try {
            $surah = Surah::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        return view('dashboard.surahs.edit', compact('surah', 'readers'));
    }//end of edit function

    public function update(SurahRequest $request, $id)
    {
        $data = $this->service->handleUploads($request, $id);
        $surah = Surah::find($id);
        $surah->update($data);
        return redirect()->route('surahs.index')->with('success', 'data stored successfully');

    }//end of update function

    public function destroy($id)
    {
        $surah = Surah::find($id);
        $this->service->deleteFiles($surah);
        $surah->delete();
        return redirect()->route('surahs.index')->with('success', 'data delete successfully');
    }//end of destroy function

    public function bulkDelete()
    {
        $this->service->deleteItems(request()->items);
        Surah::destroy(request()->items);

        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of bulkDelete function

    public function getYoutubeVideo()
    {
        $video = getYoutubeId(\request()->video);
        return response()->json(['video' => $video], 200);
    }//end of getYoutubeVideo function


}
