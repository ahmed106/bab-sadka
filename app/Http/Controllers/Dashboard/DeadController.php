<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeadRequest;
use App\Models\Country;
use App\Models\Dead;
use App\Traits\upload;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DeadController extends Controller
{
    use upload;

    public function index()
    {
        return view('dashboard.dead.index');
    }

    public function data()
    {

        $deads = Dead::OrderBy('id', 'desc')->get();

        return DataTables::of($deads)
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" name="items[]" class="checkedBtn" value="' . $raw->id . '">';
            })
            ->addColumn('link', function ($raw) {
                return '<a href="' . url('/D/' . base64_encode($raw->id)) . '">' . url('/D/' . base64_encode($raw->id)) . '</a>';
            })
            ->addColumn('approval', function ($raw) {

                if ($raw->approval == 1) {
                    $checked = 'checked';
                    $approval = 0;
                } else {
                    $approval = 1;
                    $checked = '';
                }

                return '<input type="checkbox" class="approval" data-id=' . $raw->id . '  data-approval="' . $approval . '"  name="approval" ' . $checked . ' >';
            })
            ->addColumn('actions', function ($raw) {
                return view('dashboard.dead.actions', compact('raw'));
            })
            ->rawColumns(['check_all' => 'check_all', 'approval' => 'approval', 'link' => 'link'])
            ->make(true);
    }//end of data function


    public function create()
    {

        $countries = Country::query()->get();
        return view('dashboard.dead.create', compact('countries'));
    }


    public function store(DeadRequest $request)
    {
        $data = $request->validated();
        $data['slug'] = \Str::slug($data['name']);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'deads');
            $data['photo'] = $photo;
        }
        Dead::create($data);

        return redirect()->route('dead.index')->with('success', 'data stored successfully');
    }


    public function edit($id)
    {
        try {
            $dead = Dead::findOrFail(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        $countries = Country::query()->get();
        return view('dashboard.dead.edit', compact('dead', 'countries'));
    }


    public function update(DeadRequest $request, $id)
    {
        $data = $request->validated();
        $data['slug'] = \Str::slug($data['name']);
        $dead = Dead::find($id);
        if ($request->hasFile('photo')) {
            $this->deleteoldPhoto($dead->photo, 'deads');

            $photo = $this->upload($request->photo, 'deads');
            $data['photo'] = $photo;
        }
        $dead->update($data);

        return redirect()->route('dead.index')->with('success', 'data updated successfully');
    }


    public function destroy($id)
    {
        $dead = Dead::find($id);
        $this->deleteoldPhoto($dead->photo, 'deads');
        $dead->delete();

        return redirect()->route('dead.index')->with('success', 'data deleted successfully');

    }

    public function bulkDelete()
    {


        Dead::destroy(request()->items);
        return redirect()->route('dead.index')->with('success', 'data deleted successfully');

    }//end of bulkDelete function

    public function approval(Request $request)
    {


        $dead = Dead::find($request->id);
        if ($dead->approval == 1) {
            $dead->update([
                'approval' => 0
            ]);
        } else {
            $dead->update([
                'approval' => 1
            ]);
        }


        return response()->json(['data' => 'data updated successfully'], 200);


    }//end of approval function
}
