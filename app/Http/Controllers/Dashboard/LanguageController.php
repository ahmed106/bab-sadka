<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class LanguageController extends Controller
{

    public function __construct()
    {

        $this->middleware(['auth:admin', 'permission:read_languages'])->only('index');
        $this->middleware(['auth:admin', 'permission:create_languages'])->only('create');
        $this->middleware(['auth:admin', 'permission:update_languages'])->only('edit');
        $this->middleware(['auth:admin', 'permission:delete_languages'])->only('delete');

    }//end of __construct function

    public function index()
    {


        return view('dashboard.languages.index');

    }//end of  function

    public function data()
    {

        $languages = Language::all();

        return DataTables::of($languages)
            ->addColumn('check_all', function ($raw) {

                return '<input name="items[]" value="' . $raw->id . '" class="checkedBtn" type="checkbox">';
            })
            ->addColumn('active', function ($raw) {

                $checked = '';
                $active = 1;

                if ($raw->active == 1) {
                    $checked = 'checked';
                    $active = 0;
                }
                return '<input type="checkbox" style="width:24px; height:25px"   class="active_btn" ' . $checked . ' data-active="' . $active . '"  data-id="' . $raw->id . '">';
            })
            ->addColumn('actions', function ($raw) {
                return view('dashboard.languages.actions', compact('raw'));
            })
            ->rawColumns(['check_all' => 'check_all', 'active' => 'active'])
            ->make(true);
    }//end of data function

    public function create()
    {

        return view('dashboard.languages.create');

    }//end of create function

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:languages,name',
            'locale' => 'required|unique:languages,locale',
            'country_abbreviation' => 'required|unique:languages,country_abbreviation'
        ]);
        $data = $request->except(['_token']);

        Language::create($data);
        return redirect()->route('languages.index')->with('success', 'data stored successfully');


    }//end of store function

    public function edit($id)
    {

        try {
            $language = Language::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        return view('dashboard.languages.edit', compact('language'));

    }//end of edit function

    public function update(Request $request, $id)
    {

        $lang = Language::findOrFail($id);
        $request->validate([
            'name' => 'required|unique:languages,name,' . $id,
            'locale' => 'required|unique:languages,locale,' . $id,
            'country_abbreviation' => 'required|unique:languages,country_abbreviation,' . $id
        ]);
        $data = $request->except(['_token', '_method']);
        if (!isset($data['active'])) {
            $data['active'] = 0;

        }//end if statement

        $lang->update($data);
        return redirect()->route('languages.index')->with('success', 'data updated successfully');


    }//end of update function

    public function active(Request $request)
    {
        $lang = Language::find($request->id);
        $lang->update([
            'active' => $request->active,
        ]);

        return response()->json(['data' => 'data updated successfully'], 200);

    }//end of active function


    public function destroy($id)
    {

        $lang = Language::findOrFail($id);
        $lang->delete();
        return redirect()->route('languages.index')->with('success', 'data deleted successfully');

    }//end of delete function

    public function bulkDelete(Request $request)
    {

        Language::destroy($request->items);
        return redirect()->route('languages.index')->with('success', 'data deleted successfully');

    }//end of bulkDelete function
}
