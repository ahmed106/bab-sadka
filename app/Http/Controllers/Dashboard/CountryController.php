<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class CountryController extends Controller
{
    public function index()
    {
        return view('dashboard.settings.countries.index');
    }

    public function data()
    {

        $country = Country::OrderBy('id', 'desc')->get();
        return DataTables::of($country)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.settings.countries.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {

                return '<input type="checkbox" name="items[]" class="checkedBtn" value="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function

    public function create()
    {
        return view('dashboard.settings.countries.create');
    }

    public function store(CountryRequest $request)
    {
        $data = $request->validated();

        Country::create($data);

        return redirect()->route('countries.index')->with('success', 'data stored successfully');

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        try {
            $country = Country::query()->find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }

        return view('dashboard.settings.countries.edit', compact('country'));

    }

    public function update(CountryRequest $request, $id)
    {
        $country = Country::query()->find($id);
        $data = $request->validated();
        $country->update($data);
        return redirect()->route('countries.index')->with('success', 'data updated successfully');
    }

    public function destroy($id)
    {
        $country = Country::query()->find($id);
        $country->delete();
        return redirect()->back()->with('success', 'data deleted successfully');
    }

    public function bulkDelete()
    {
        Country::destroy(request()->items);
        return redirect()->back()->with('success', 'data deleted successfully');
    }//end of bulkDelete function
}
