<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Traits\upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SettingController extends Controller
{
    use upload;

    public function index()
    {

        $setting = Setting::first();
        return view('dashboard.settings.main_settings', compact('setting'));
    }

    public function store(Request $request)
    {


        $setting = Setting::first();
        $rules = [
            'logo' => 'sometimes|nullable|file|mimes:png,jpg,jpeg'
        ];

        foreach (activeLanguages() as $activeLanguage) {
            $rules += [$activeLanguage->locale . '.website_name' => 'required|string'];
            $rules += [$activeLanguage->locale . '.website_description' => 'required|string'];
            $rules += [$activeLanguage->locale . '.meta_title' => 'sometimes|nullable'];
            $rules += [$activeLanguage->locale . '.meta_description' => 'sometimes|nullable'];
            $rules += [$activeLanguage->locale . '.meta_keywords' => 'sometimes|nullable'];
        }

        $data = $request->validate($rules);
        if ($request->file('logo')) {

            $setting ? File::delete(public_path('images/settings/' . $setting->logo)) : '';
            $logo = $this->upload($request->file('logo'), 'settings');
            $data['logo'] = $logo;
        }


        if (!$setting) {
            Setting::create($data);

        } else {
            $setting->update($data);
        }


        return redirect()->route('settings.index')->with('success', 'data stored successfully');
    }//end of store function


}
