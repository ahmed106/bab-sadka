<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\LibraryCategoryRequest;
use App\Models\LibraryCategory;
use App\Services\LibraryCategoryService;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class LibraryCategoryController extends Controller
{

    public $service;

    public function __construct(LibraryCategoryService $service)
    {
        $this->service = $service;
    }

    public function index()
    {

        return view('dashboard.libraries.categories.index');

    }//end of index function

    public function data()
    {
        $category = LibraryCategory::with('translations')->OrderBy('id', 'desc')->get();

        return DataTables::of($category)
            ->addColumn('actions', function ($raw) {

                return view('dashboard.libraries.categories.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" class="checkedBtn" name="items[]" value="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);


    }//end of data function

    public function create()
    {
        return view('dashboard.libraries.categories.create');


    }//end of create function

    public function store(LibraryCategoryRequest $request)
    {

        $data = $this->service->handleUploads($request);
        LibraryCategory::create($data);
        return redirect()->route('library-categories.index')->with('success', 'data stored successfully');

    }//end of store function

    public function edit($id)
    {
        try {
            $category = LibraryCategory::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }

        return view('dashboard.libraries.categories.edit', compact('category'));


    }//end of edit function

    public function update(Request $request, $id)
    {
        $data = $this->service->handleUploads($request, $id);

        $category = LibraryCategory::find($id);
        $category->update($data);
        return redirect()->route('library-categories.index')->with('success', 'data updated successfully');


    }//end of update function

    public function destroy($id)
    {
        $cat = LibraryCategory::find($id);
        $this->service->deleteoldPhoto($cat->photo, 'library_categories');
        $cat->delete();
        return redirect()->back()->with('success', 'data deleted successfully');


    }//end of destroy function

    public function bulkDelete()
    {
        $this->service->deleteItems(\request()->items);
        LibraryCategory::destroy(\request()->items);
        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of bulkDelete function

}
