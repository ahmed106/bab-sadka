<?php

namespace App\Http\Controllers\Dashboard\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mews\Captcha\Facades\Captcha;

class AuthController extends Controller
{

    public function login()
    {

        return view('dashboard.auth.login');

    }//end of login function

    public function doLogin(Request $request)
    {

        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'captcha'=>'required|captcha'
        ],[
            'captcha.captcha'=>' الرمز خطأ'
        ],[
            'captcha'=>'الرمز'
        ]);
        if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/dashboard')->with('success', 'Welcome To Bab-sadka Dashboard');
        } else {

            return redirect()->back()->withInput()->with('error', 'Email or Password is in-valid');
        }


    }//end of doLogin function

    public function logout()
    {

        auth('admin')->logout();

        return redirect()->back();


    }//end of logout function

    public function reloadCaptcha(){
        return response()->json(['captcha'=>captcha_img('flat')]);
    } // end reloadCaptcha function
}
