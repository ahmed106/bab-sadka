<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\PrayRequest;
use App\Models\Pray;
use App\Services\PrayService;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class PrayController extends Controller
{

    protected $service;

    public function __construct(PrayService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('dashboard.prays.index');
    }

    public function data()
    {
        $prays = Pray::with('translations')->OrderBy('id', 'desc')->get();

        return DataTables::of($prays)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.prays.actions', compact('raw'));
            })
            ->addColumn('pray', function ($raw) {

                return strip_tags(Str::limit($raw->pray, 100));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" name="items[]" value="' . $raw->id . '" class="checkedBtn">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function


    public function create()
    {
        return view('dashboard.prays.create');
    }


    public function store(PrayRequest $request)
    {
        $data = $this->service->handleUploads($request);
        Pray::create($data);

        return redirect()->route('prays.index')->with('success', 'data stored successfully');
    }


    public function edit($id)
    {
        try {
            $pray = Pray::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }

        return view('dashboard.prays.edit', compact('pray'));
    }


    public function update(PrayRequest $request, $id)
    {
        $data = $this->service->handleUploads($request, $id);
        $pray = Pray::find($id);

        $pray->update($data);
        return redirect()->route('prays.index')->with('success', 'data updated successfully');
    }

    public function destroy($id)
    {

        $pray = Pray::find($id);
        $this->service->deleteFiles($pray);
        $pray->delete();
        return redirect()->back()->with('success', 'data updated successfully');
    }

    public function bulkDelete()
    {
        $this->service->deleteItems(request()->items);

        Pray::destroy(request()->items);
        return redirect()->route('prays.index')->with('success', 'data deleted successfully');
    }//end of bulkDelete function


}
