<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProphetRequest;
use App\Models\ProphetStory;
use App\Services\ProphetStoryService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\DataTables;

class ProphetStoryController extends Controller
{


    public $prophetStory;

    public function __construct(ProphetStoryService $prophetStory)
    {
        $this->prophetStory = $prophetStory;


    }//end of __construct function

    public function index()
    {
        return view('dashboard.prophet_stories.index');

    }//end of index function


    public function data()
    {

        $stories = ProphetStory::with('translations')->OrderBy('id', 'desc')->get();
        return DataTables::of($stories)
            ->addColumn('check_all', function ($raw) {

                return '<input type="checkbox" name="items[]" value="' . $raw->id . '" class="checkedBtn">';
            })
            ->addColumn('actions', function ($raw) {
                return
                    '<div class="d-flex align-items-center list-action">

                        <a class="btn btn-success" href="' . route('prophet-stories.edit', encrypt($raw->id)) . '"><i class="ri-pencil-line mr-0"></i> تعديل</a>
                        <a class="btn btn-danger" href="' . route('prophet-stories.delete', $raw->id) . '"><i class="fa fa-trash mr-0"></i> حذف</a>
                    </div>';


            })
            ->rawColumns(['check_all' => 'check_all', 'actions' => 'actions'])
            ->make(true);

    }//end of data function

    public function create()
    {
        return view('dashboard.prophet_stories.create');


    }//end of create function

    public function store(ProphetRequest $request)
    {

        $data = $this->prophetStory->handleUploads($request);

        ProphetStory::create($data);

        return redirect()->route('prophet-stories.index')->with('success', 'data stored successfully');

    }//end of store function

    public function edit($id)
    {

        try {
            $story = ProphetStory::query()->findOrFail(decrypt($id));

        }catch (DecryptException $exception){
            abort(404);
        }
        return view('dashboard.prophet_stories.edit', compact('story'));

    }//end of edit function

    public function update(ProphetRequest $request, $id)
    {
        $data = $this->prophetStory->handleUploads($request, $id);

        $story = ProphetStory::findOrFail($id);
        $story->update($data);

        return redirect()->route('prophet-stories.index')->with('success', 'data updated successfully');

    }//end of  function

    public function destroy($id)
    {
        dd($id);

    }//end of destroy function

    public function delete($id)
    {

        $story = ProphetStory::findOrFail($id);
        $this->prophetStory->deleteFiles($story);
        $story->delete();
        return redirect()->route('prophet-stories.index')->with('success', 'data deleted successfully');
    }

    public function bulkDelete()
    {
        $this->prophetStory->deleteItems(request()->items);
        ProphetStory::destroy(request()->items);
        return redirect()->route('prophet-stories.index')->with('success', 'data deleted successfully');

    }//end of bulkDelete function
}
