<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\HadithRequest;
use App\Models\Hadith;
use App\Services\HadithService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class HadithController extends Controller
{


    public $service;

    public function __construct(HadithService $service)
    {

        $this->service = $service;

    }//end of __construct function

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('dashboard.hadiths.index');
    }


    public function data()
    {

        $hadiths = Hadith::OrderBy('id', 'desc')->get();

        return DataTables::of($hadiths)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.hadiths.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" value="' . $raw->id . '" name="items[]" class="checkedBtn">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);

    }//end of data function

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.hadiths.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(HadithRequest $request)
    {
        $data = $this->service->handleUploads($request);
        Hadith::create($data);
        return redirect()->route('hadiths.index')->with('success', 'data stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $hadith = Hadith::findOrFail(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }

        return view('dashboard.hadiths.edit', compact('hadith'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(HadithRequest $request, $id)
    {
        $data = $this->service->handleUploads($request, $id);
        $hadith = Hadith::find($id);
        $hadith->update($data);

        return redirect()->route('hadiths.index')->with('success', 'data updated successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hadith = Hadith::find($id);
        $this->service->deleteFiles($hadith);
        $hadith->delete();
        return redirect()->back()->with('success', 'data deleted successfully');
    }

    public function bulkDelete()
    {
        $this->service->deleteItems(request()->items);
        Hadith::destroy(request()->items);

        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of bulkDelete function
}
