<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\HesnRequest;
use App\Models\Hesn;
use App\Services\HesnService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class HesnController extends Controller
{

    protected $hesnService;

    public function __construct(HesnService $hesnService)
    {
        $this->hesnService = $hesnService;

    }//end of __construct function

    public function index()
    {
        return view('dashboard/hesn.index');
    }


    public function data()
    {
        $hesn = Hesn::with('translations')->OrderBy('id', 'desc')->get();
        return DataTables::of($hesn)
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" value="' . $raw->id . '" name="items[]" class="checkedBtn">';
            })
            ->addColumn('actions', function ($raw) {
                return view('dashboard.hesn.actions', compact('raw'));
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);
    }


    public function create()
    {
        return view('dashboard.hesn.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(HesnRequest $request)
    {
        $data = $this->hesnService->handleUploads($request);
        Hesn::create($data);
        return redirect()->route('hesn.index')->with('success', 'data stored successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $hesn = Hesn::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }

        return view('dashboard.hesn.edit', compact('hesn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(HesnRequest $request, $id)
    {
        $data = $this->hesnService->handleUploads($request, $id);
        $hesn = Hesn::find($id);
        $hesn->update($data);

        return redirect()->route('hesn.index')->with('success', 'data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hesn = Hesn::find($id);
        $this->hesnService->deleteFiles($hesn);

        $hesn->delete();
        return redirect()->back()->with('success', 'data deleted successfully');
    }

    public function bulkDelete()
    {
        $this->hesnService->deleteItems(request()->items);
        Hesn::destroy(request()->items);
        return redirect()->back()->with('success', 'data deleted successfully');

    }//end of bulkDelete function
}
