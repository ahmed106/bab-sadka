<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrphanRequest;
use App\Models\City;
use App\Models\Country;
use App\Models\Orphan;
use App\Services\OrphanService;
use App\Traits\upload;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OrphanController extends Controller
{
    use upload;

    public $service;

    public function __construct(OrphanService $service)
    {

        $this->service = $service;

    }//end of __construct function

    public function index()
    {
        return view('dashboard.orphans.index');
    }

    public function data()
    {
        $orphans = Orphan::query()->OrderBy('id', 'desc')->get();

        return DataTables::of($orphans)
            ->addColumn('actions', function ($raw) {
                return view('dashboard.orphans.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" name="items[]" value="' . $raw->id . '" class="checkedBtn">';
            })
            ->addColumn('approval', function ($raw) {

                $checked = '';
                $active = 1;

                if ($raw->approval == 1) {
                    $checked = 'checked';
                    $active = 0;
                }
                return '<input type="checkbox" style="width:24px; height:25px"   class="active_btn" ' . $checked . ' data-active="' . $active . '"  data-id="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all', 'approval' => 'approval'])
            ->make(true);

    }//end of data function

    public function create()
    {

        $countries = Country::query()->with('translations')->get();

        return view('dashboard.orphans.create', compact('countries'));

    }//end of data function

    public function getCountryCities(Request $request)
    {
        $cities = City::whereCountryId($request->country_id)->get();

        return response()->json(['data' => $cities], 200);

    }//end of getCity function

    public function store(OrphanRequest $request)
    {
        $data = $this->service->handleUploads($request);

        Orphan::create($data);
        return redirect()->route('orphans.index')->with('success', 'data stored successfully');

    }//end of store function

    public function edit($id)
    {
        try {
            $orphan = Orphan::query()->findOrFail(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        $countries = Country::query()->with('translations')->get();
        return view('dashboard.orphans.edit', compact('orphan', 'countries'));


    }//end of edit function

    public function update(OrphanRequest $request, $id)
    {
        $orphan = Orphan::query()->findOrFail($id);
        $data = $this->service->handleUploads($request, $id);
        $orphan->update($data);
        return redirect()->route('orphans.index')->with('success', 'data updated successfully');

    }//end of update function

    public function destroy($id)
    {
        $orphan = Orphan::findOrFail($id);
        $this->service->deleteFiles($orphan);

        $orphan->delete();
        return redirect()->route('orphans.index')->with('success', 'data deleted successfully');

    }//end of destroy function

    public function bulkDelete()
    {

        $this->service->deleteItems(request()->items);
        Orphan::destroy(request()->items);
        return redirect()->route('orphans.index')->with('success', 'data deleted successfully');


    }//end of bulkDelete function

    public function approval(Request $request)
    {

        $orphan = Orphan::findOrFail($request->id);
        $orphan->update([
            'approval' => $request->active,
        ]);
        return response()->json(['data' => 'data updated successfully'], 200);

    }//end of approval function
}
