<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Role;
use App\Traits\upload;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    use upload;

    public function __construct()
    {
        $this->middleware(['auth:admin', 'permission:read_admins'])->only('index');
        $this->middleware(['auth:admin', 'permission:create_admins'])->only('create');
        $this->middleware(['auth:admin', 'permission:update_admins'])->only('edit');
        $this->middleware(['auth:admin', 'permission:delete_admins'])->only('delete');
    }

    public function index()
    {

        return view('dashboard.admins.index');
    }

    public function data()
    {
        $admins = Admin::whereRoleIs(['admin', 'owner'])->get();

        return DataTables::of($admins)
            ->addColumn('check_all', function ($raw) {

                return '<input name="items[]" value="' . $raw->id . '" class="checkedBtn" type="checkbox">';
            })
            ->addColumn('actions', function ($raw) {
                return view('dashboard.admins.actions', compact('raw'));
            })
            ->addColumn('photo', function ($raw) {

                return '<img src="' . $raw->image . '" style="width:70px;height:70px">';
            })
            ->rawColumns(['check_all' => 'check_all', 'photo' => 'photo'])
            ->make(true);
    }//end of data function

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();

        return view('dashboard.admins.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token', 'password', 'role', 'password_confirmation');


        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:admins,email',
            'password' => 'required|confirmed',
            'role' => 'integer|required',
        ]);
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }//end if statement
        if ($request->hasFile('photo')) {
            $name = $this->upload($request->photo, 'admins');
            $data['photo'] = $name;

        }//end if statement

        $admin = Admin::create($data);
        $admin->attachRole($request->role);
        return redirect()->route('admins.index')->with('success', 'data stored successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $admin = Admin::findOrFail(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        $roles = Role::get();
        return view('dashboard.admins.edit', compact('admin', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $data = $request->except(['_token', '_method', 'role']);


        if ($request->password != null) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data['password'] = $admin->password;
        }
        if ($request->hasFile('photo')) {

            $this->deleteoldPhoto($admin->photo, 'admins');
            $name = $this->upload($request->photo, 'admins');
            $data['photo'] = $name;

        }//end if statement

        $admin->update($data);

        $admin->syncRoles([$request->role]);

        return redirect()->route('admins.index')->with('success', 'data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $this->deleteoldPhoto($admin->photo, 'admins');
        $admin->delete();
        return redirect()->route('admins.index')->with('success', 'data deleted successfully');
    }

    public function bulkDelete(Request $request)
    {

        foreach ($request->items as $item) {
            $admin = Admin::find($item);
            $this->deleteoldPhoto($admin->photo, 'admins');
        }
        Admin::destroy($request->items);

        return redirect()->route('admins.index')->with('success', 'data deleted successfully');


    }//end of bulkDelete function
}
