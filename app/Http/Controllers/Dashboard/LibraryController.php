<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\LibraryRequest;
use App\Models\Library;
use App\Models\LibraryCategory;
use App\Services\LibraryService;
use Illuminate\Contracts\Encryption\DecryptException;
use Yajra\DataTables\Facades\DataTables;

class LibraryController extends Controller
{

    public $service;

    public function __construct(LibraryService $service)
    {
        $this->service = $service;
    }

    public function index()
    {

        return view('dashboard.libraries.libraries.index');

    }//end of index function

    public function data()
    {
        $library = Library::with('translations')->OrderBy('id', 'desc')->get();

        return DataTables::of($library)
            ->addColumn('actions', function ($raw) {

                return view('dashboard.libraries.libraries.actions', compact('raw'));
            })
            ->addColumn('check_all', function ($raw) {
                return '<input type="checkbox" class="checkedBtn" name="items[]" value="' . $raw->id . '">';
            })
            ->rawColumns(['check_all' => 'check_all'])
            ->make(true);


    }//end of data function

    public function create()
    {
        $categories = LibraryCategory::with('translations')->get();
        return view('dashboard.libraries.libraries.create', compact('categories'));


    }//end of create function

    public function store(LibraryRequest $request)
    {

        $data = $this->service->handleUploads($request);
        Library::create($data);

        return redirect()->route('libraries.index')->with('success', 'data stored successfully');

    }//end of store function

    public function edit($id)
    {
        try {
            $library = Library::find(decrypt($id));
        }catch (DecryptException $exception){
            abort(404);
        }
        $categories = LibraryCategory::with('translations')->get();
        return view('dashboard.libraries.libraries.edit', compact('library', 'categories'));


    }//end of edit function

    public function update(LibraryRequest $request, $id)
    {
        $library = Library::find($id);
        $data = $this->service->handleUploads($request, $id);
        $library->update($data);
        return redirect()->route('libraries.index')->with('success', 'data updated successfully');

    }//end of update function

    public function destroy($id)
    {
        $library = Library::find($id);
        $this->service->deleteFiles($library);
        $library->delete();
        return redirect()->route('libraries.index')->with('success', 'data deleted successfully');

    }//end of destroy function

    public function bulkDelete()
    {
        $this->service->deleteItems(request()->items);
        Library::destroy(request()->items);
        return redirect()->route('libraries.index')->with('success', 'data deleted successfully');

    }//end of bulkDelete function

    public function viewPdf($id)
    {
        $library = Library::find($id);
        return view('dashboard.libraries.libraries.view_pdf', compact('library'));
    }//end of viewPdf function
}
