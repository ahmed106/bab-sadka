<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use App\Models\Pray;
use Illuminate\Support\Facades\Session;

class PrayController extends Controller
{
    public function index()
    {

        if (\request('D')) {
            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }
        return view('website.pray.index');
    }//end of index function

    public function read()
    {
        $prays = Pray::query()->get();

        return view('website.pray.read', compact('prays'));

    }//end of read function

    public function listen()
    {
        $prays = Pray::query()->get();

        return view('website.pray.listen', compact('prays'));


    }//end of listen function

    public function videos()
    {
        $paginator = 12;
        $videos_count = Pray::query()->count();
        $pages = ceil($videos_count / $paginator);
        $prays = Pray::query()->simplePaginate($paginator);

        return view('website.pray.videos', compact('prays', 'pages'));


    }//end of videos function

    public function show($id)
    {

        $pray = Pray::findOrFail($id);
        $prays = Pray::get();
        return view('website.pray.show', compact('prays', 'pray'));

    }//end of show function
}

