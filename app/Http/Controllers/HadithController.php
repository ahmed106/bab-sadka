<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use App\Models\Hadith;
use App\Models\Hesn;
use App\Models\NawawyForty;
use App\Models\Pray;
use App\Models\ProphetStory;
use App\Models\Surah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class HadithController extends Controller
{
    public function index()
    {

        if (\request('D')) {
            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }
        return view('website.hadith.index');


    }//end of index function

    public function listen()
    {
        $hadith = Hadith::query()->with('translations')->get();

        return view('website.hadith.listen', compact('hadith'));

    }//end of listen function

    public function videos()
    {
        $paginator = 12;
        $videos_count = Hadith::query()->count();
        $pages = ceil($videos_count / $paginator);
        $hadith = Hadith::query()->simplePaginate($paginator);

        return view('website.hadith.videos', compact('hadith', 'pages'));

    }//end of videos function

    public function getAudio(Request $request)
    {

        $type = $request->class;

        switch ($type) {
            case 'hadiths':
                $model = Hadith::findOrFail($request->id);
                break;
            case 'surahs' :
                $model = Surah::findOrFail($request->id);
                break;
            case 'prophet_stories':
                $model = ProphetStory::findOrFail($request->id);
                break;
            case 'hesn':
                $model = Hesn::findOrFail($request->id);
                break;
            case 'nawawy_forty':

                $model = NawawyForty::findOrFail($request->id);
                break;
            case 'prays':

                $model = Pray::findOrFail($request->id);
                break;

        }


        $view = \View::make('website.hadith.audio', compact('model', 'type'))->render();
        $audio = $model->audio;

        return response()->json(['data' => $view, 'audio' => $audio], 200);
    }//end of getAudio function

    public function show($id)
    {
        $hadith = Hadith::findOrFail($id);
        $hadiths = Hadith::get();

        return view('website.hadith.show', compact('hadith', 'hadiths'));

    }//end of show function
}
