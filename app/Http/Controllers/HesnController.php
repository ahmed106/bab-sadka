<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use App\Models\Hesn;
use Illuminate\Support\Facades\Session;

class HesnController extends Controller
{
    public function index()
    {

        if (\request('D')) {
            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }
        return view('website.hesn.index');


    }//end of index function

    public function listen()
    {
        $hesns = Hesn::query()->get();

        return view('website.hesn.listen', compact('hesns'));


    }//end of listen function

    public function videos()
    {
        $paginator = 12;
        $videos_count = Hesn::query()->count();
        $pages = ceil($videos_count / $paginator);
        $hesns = Hesn::query()->simplePaginate($paginator);

        return view('website.hesn.videos', compact('hesns', 'pages'));


    }//end of videos function

    public function show($id)
    {

        $hesn = Hesn::findOrFail($id);
        $hesns = Hesn::get();
        return view('website.hesn.show', compact('hesns', 'hesn'));


    }//end of show function
}
