<?php

namespace App\Http\Controllers;

use App\Models\Dead;
use App\Models\NawawyForty;
use Illuminate\Support\Facades\Session;

class NwawiaController extends Controller
{
    public function index()
    {

        if (\request('D')) {
            Session::forget('dead');
            $dead_id = base64_decode(\request('D'));
            $dead = Dead::findOrFail($dead_id);
            $dead_session = ['name' => $dead->name, 'id' => $dead->id, 'photo' => $dead->photo];
            Session::put(['dead' => $dead_session]);
        }
        return view('website.nwawia.index');


    }//end of index function

    public function read()
    {

        $forty_nwawia = NawawyForty::query()->get();
        return view('website.nwawia.read', compact('forty_nwawia'));


    }//end of index function

    public function listen()
    {
        $forty_nwawia = NawawyForty::query()->get();
        return view('website.nwawia.listen', compact('forty_nwawia'));


    }//end of index function

    public function videos()
    {
        $paginator = 12;
        $videos_count = NawawyForty::query()->count();
        $pages = ceil($videos_count / $paginator);
        $forty_nwawia = NawawyForty::query()->simplePaginate($paginator);
        return view('website.nwawia.videos', compact('forty_nwawia', 'pages'));

    }//end of index function

    public function show($id)
    {

        $hadith = NawawyForty::findOrFail($id);
        $hadiths = NawawyForty::get();

        return view('website.nwawia.show', compact('hadith', 'hadiths'));
    }//end of show function

}
