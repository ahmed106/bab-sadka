<?php

namespace App\Http\Controllers;

use App\Models\VideoKid;

class KidvideoController extends Controller
{
    public function index()
    {

        $paginator = 12;
        $videos_count = VideoKid::query()->count();
        $pages = ceil($videos_count / $paginator);
        $videos = VideoKid::query()->simplePaginate($paginator);

        return view('website.kid_videos.index', compact('videos', 'pages'));

    }//end of index function

    public function show($id)
    {

        $kid_videos = VideoKid::query()->get();
        $video = VideoKid::findOrFail($id);

        return view('website.kid_videos.show', compact('video', 'kid_videos'));

    }//end of show function

}
