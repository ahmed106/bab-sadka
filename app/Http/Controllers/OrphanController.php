<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Country;
use App\Models\Orphan;
use App\Notifications\AddOrphan;
use App\Services\OrphanService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class OrphanController extends Controller
{
    public $service;

    public function __construct(OrphanService $service)
    {
        $this->service = $service;
    }

    public function create()
    {
        $countries = Country::query()->get();
        return view('website.orphan.create', compact('countries'));

    }//end of create function

    public function store(Request $request)
    {
        $this->validation($request);
        try {

            $data = $this->service->handleUploads($request);
            $orphan = Orphan::create($data);
            $admins = Admin::all();
            Notification::send($admins, new AddOrphan($orphan));
            return redirect('/')->with('success', 'data stored successfully');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }


    }//end of store function

    public function validation($request)
    {

        $data = $request->validate([
            'name' => 'required|string|min:4|unique:orphans,name',
            'id_number' => 'sometimes:nullable|min:11',
            'gender' => 'required|in:male,female',
            'birthdate' => 'required|date',
            'city_id' => 'required|exists:cities,id',
            'country_id' => 'required|exists:countries,id',
            'address' => 'required|string',
            'lost' => 'required|in:father,mother,both',
            'live_with' => 'required|string',
            'country_code' => 'required|starts_with:+20,+966',
            'phone' => 'required|numeric',
            'phone_owner' => 'required|string',
            'has_disease' => 'required|in:0,1',
            'disease_type' => 'required_if:has_disease,1|exclude_if:has_disease,0',
            'disease_cost' => 'required_if:has_disease,1|exclude_if:has_disease,0',
            'photo' => 'sometimes:nullable|file|mimes:png,jpg,jpeg,gif',
            'father_name' => 'required|string',
            'father_id' => 'required|integer|min:11',
            'father_death_date' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_reason' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_place' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_death_application' => 'required_if:lost,father|required_if:lost,both|exclude_if:lost,mother',
            'father_photo' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_education_level' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_job' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'father_salary' => 'required_if:lost,mother|exclude_if:lost,father|exclude_if:lost,both',
            'mother_name' => 'required|string',
            'mother_id' => 'required|integer|min:11',
            'mother_death_date' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_reason' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_place' => 'required_if:lost,mother|required_if:lost,both|exclude_if:lost,father',
            'mother_death_application' => 'required_if:lost,mother|exclude_if:lost,father',
            'mother_photo' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_education_level' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_job' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
            'mother_salary' => 'required_if:lost,father|exclude_if:lost,mother|exclude_if:lost,both',
        ]);
        return $data;

    }//end of validation function


}
