<?php

namespace App\Jobs;

use App\Events\PrayNotification;
use App\Models\PrayTime;
use App\Traits\prayTimes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetPrayTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use prayTimes;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $asr_time = PrayTime::first()->asr;

        event(new PrayNotification('حان الأن صلاه العصر'));

    }
}
