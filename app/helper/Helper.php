<?php


// get city name by country name
if (!function_exists('getCity')) {


    function getCity($country)
    {
        $countries = [
            'Afghanistan' => 'Kabul',
            'Albania' => 'Tirana',
            'Algeria' => 'Alger',
            'American Samoa' => 'Fagatogo',
            'Andorra' => 'Andorra la Vella',
            'Angola' => 'Luanda',
            'Anguilla' => 'The Valley',
            'Antigua and Barbuda' => "Saint John's",
            'Argentina' => 'Buenos Aires',
            'Armenia' => 'Yerevan',
            'Aruba' => 'Oranjestad',
            'Australia' => 'Canberra',
            'Austria' => 'Wien',
            'Azerbaijan' => 'Baku',
            'Bahamas' => 'Nassau',
            'Bahrain' => 'al-Manama',
            'Bangladesh' => 'Dhaka',
            'Barbados' => 'Bridgetown',
            'Belarus' => 'Minsk',
            'Belgium' => 'Bruxelles',
            'Belize' => 'Belmopan',
            'Benin' => 'Porto-Novo',
            'Bermuda' => 'Hamilton',
            'Bhutan' => 'Thimphu',
            'Bolivia' => 'La Paz',
            'Bosnia and Herzegovina' => 'Sarajevo',
            'Botswana' => 'Gaborone',
            'Brazil' => 'Brasília',
            'Brunei' => 'Bandar Seri Begawan',
            'Bulgaria' => 'Sofia',
            'Burkina Faso' => 'Ouagadougou',
            'Burundi' => 'Bujumbura',
            'Cambodia' => 'Phnom Penh',
            'Cameroon' => 'Yaound',
            'Canada' => 'Ottawa',
            'Cape Verde' => 'Praia',
            'Cayman Islands' => 'George Town',
            'Central African Republic' => 'Bangui',
            'Chad' => "N'Djam",
            'Chile' => 'Santiago de Chile',
            'China' => 'Peking',
            'Christmas Island' => 'Flying Fish Cove',
            'Colombia' => 'Santaf',
            'Comoros' => 'Moroni',
            'Congo' => 'Brazzaville',
            'Cook Islands' => 'Avarua',
            'Costa Rica' => 'San Jos',
            'Croatia' => 'Zagreb',
            'Cuba' => 'La Habana',
            'Cyprus' => 'Nicosia',
            'Czech Republic' => 'Praha',
            'Denmark' => 'Copenhagen',
            'Djibouti' => 'Djibouti',
            'Dominica' => 'Roseau',
            'Dominican Republic' => 'Santo Domingo de Guzm',
            'East Timor' => 'Dili',
            'Ecuador' => 'Quito',
            'Egypt' => 'Cairo',
            'El Salvador' => 'San Salvador',
            'England' => 'London',
            'Equatorial Guinea' => 'Malabo',
            'Eritrea' => 'Asmara',
            'Estonia' => 'Tallinn',
            'Ethiopia' => 'Addis Abeba',
            'Falkland Islands' => 'Stanley',
            'Faroe Islands' => 'Tórshavn',
            'France' => 'Paris',
            'Germany' => 'Berlin',
            'Greece' => 'Athenai',
            'Iraq' => 'Baghdad',
            'Italy' => 'Roma',
            'Japan' => 'Tokyo',
            'Jordan' => 'Amman',
            'Kuwait' => 'Kuwait',
            'Lebanon' => 'Beirut',
            'Palestine' => 'Gaza',
            'United Arab Emirates' => 'Abu Dhabi',
            'Saudi Arabia' => 'Riyadh',

        ];

        return $countries[$country];


    }//end of getCity function

}//end if statement


// get admin url
if (!function_exists('aurl')) {

    function aurl($url = null): string
    {
        return asset('assets/admin/assets/' . $url);

    }


}//end if statement

// get active class for aside menu

if (!function_exists('active')) {
    function active($url): array
    {
        if (request()->segment(2) === $url) {

            return ['active', 'show', 'true'];
        } elseif (request()->segment(3) === $url) {
            return ['active', 'show', 'true'];
        } else {
            return ['', '', ''];
        }


    }
}//end if statement

//  get Models for roles

if (!function_exists('models')) {
    function models(): array
    {
        return [
            'admins',
            'languages',
            'roles',
            'surahs',
            'quran_readers',
            'hadiths',
            'prophet_stories',
            'hesns',
            'nawawy_forty',
            'video_kids',
            'library_categories',
            'libraries',
            'prays',
            'countries',
            'cities',
            'orphans',
            'contactUs',
            'dead'


        ];
    }
}//end if statement

// get maps for roles

if (!function_exists('maps')) {

    function maps(): array
    {
        return ['create', 'read', 'update', 'delete'];
    }
}//end if statement


// get active languages
if (!function_exists('activeLanguages')) {
    function activeLanguages()
    {
        $languages = \App\Models\Language::where('active', 1)->get();
        return $languages;
    }
}//end if statement

// end active languages


if (!function_exists('getYoutubeId')) {
    function getYoutubeId($url)
    {
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        if (isset($match[1])) {
            return 'https://www.youtube.com/embed/' . $match[1];
        } else {
            return null;
        }
    }
}  //end of getYoutubeId function


