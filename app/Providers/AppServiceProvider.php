<?php

namespace App\Providers;

use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        if(Schema::hasTable('languages')&& Schema::hasTable('settings')){
            $languages = \App\Models\Language::where('active', 1)->get();
            $settings = Setting::with('translations')->first();
            View::composer('*', function ($view) use ($languages,$settings) {

                $view->with(['languages' => $languages]);
                $view->with(['settings' => $settings]);
            });

        }

        Schema::defaultStringLength(191);

    }


}
