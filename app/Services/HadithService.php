<?php

namespace App\Services;

use App\Models\Hadith;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class HadithService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $hadith = '';
        if ($id) {
            $hadith = Hadith::find($id);
        }
        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/hadiths/videos/' . $hadith->video)) : '';
            $path = $this->uploadVideo($request->video, 'hadiths/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/hadiths/videos/' . $hadith->video)) : '';
            $data['video'] = $request->video;
        } else {
            $data['video'] = $hadith->video;
        }

        if ($request->hasFile('audio')) {
            $id ? File::delete(public_path('uploads/hadiths/audios/' . $hadith->audio)) : '';
            $path = $this->uploadVideo($request->audio, 'hadiths/audios');
            $data['audio'] = $path;
        } else {
            $id ? $data['audio'] = $hadith->audio : '';
        }

        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/hadiths/' . $hadith->photo)) : '';
            $path = $this->upload($request->photo, 'hadiths');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $hadith->photo : '';
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($hadith)
    {

        File::delete(public_path('uploads/hadiths/videos/' . $hadith->video));
        File::delete(public_path('uploads/hadiths/audios/' . $hadith->audio));
        File::delete(public_path('images/hadiths/' . $hadith->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $hadith = Hadith::find($id);
            $this->deleteFiles($hadith);
        }

    }//end of deleteItems function
}
