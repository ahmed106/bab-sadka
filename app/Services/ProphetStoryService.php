<?php

namespace App\Services;

use App\Models\ProphetStory;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class ProphetStoryService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $story = '';
        if ($id) {
            $story = ProphetStory::find($id);
        }
        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/prophet_stories/videos/' . $story->video)) : '';
            $path = $this->uploadVideo($request->video, 'prophet_stories/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/prophet_stories/videos/' . $story->video)) : '';
            $data['video'] = $request->video;
        } else {
            $data['video'] = $story->video;
        }

        if ($request->hasFile('audio')) {
            $id ? File::delete(public_path('uploads/prophet_stories/audios/' . $story->audio)) : '';
            $path = $this->uploadVideo($request->audio, 'prophet_stories/audios');
            $data['audio'] = $path;
        } else {
            $id ? $data['audio'] = $story->audio : '';
        }

        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/prophet_stories/' . $story->photo)) : '';
            $path = $this->upload($request->photo, 'prophet_stories');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $story->photo : '';
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($story)
    {

        File::delete(public_path('uploads/prophet_stories/videos/' . $story->video));
        File::delete(public_path('uploads/prophet_stories/audios/' . $story->audio));
        File::delete(public_path('images/prophet_stories/' . $story->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $story = ProphetStory::find($id);
            $this->deleteFiles($story);
        }

    }//end of deleteItems function
}
