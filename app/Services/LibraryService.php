<?php

namespace App\Services;

use App\Models\Library;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class LibraryService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $library = '';
        if ($id) {
            $library = Library::find($id);
        }
        if ($request->hasFile('photo')) {
            $id ? $this->deleteoldPhoto($library->photo, 'libraries') : '';
            $path = $this->upload($request->photo, 'libraries');
            $data['photo'] = $path;
        } else {
            $data['photo'] = $library->photo;
        }

        if ($request->hasFile('pdf')) {

            $id ? $this->deleteoldPdf($library->pdf, 'libraries/pdf') : '';
            $path = $this->uploadPdf($request->pdf, 'libraries/pdf');
            $data['pdf'] = $path;
        } else {
            $data['pdf'] = $library->pdf;
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($library)
    {

        File::delete(public_path('images/libraries/' . $library->photo));
        File::delete(public_path('uploads/libraries/pdf/' . $library->pdf));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $library = Library::find($id);
            $this->deleteFiles($library);
        }

    }//end of deleteItems function
}
