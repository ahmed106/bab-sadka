<?php

namespace App\Services;

use App\Models\LibraryCategory;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class LibraryCategoryService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $category = '';
        if ($id) {
            $category = LibraryCategory::find($id);
        }
        if ($request->hasFile('photo')) {
            $id ? $this->deleteoldPhoto($category->photo, 'library_categories') : '';
            $path = $this->upload($request->photo, 'library_categories');
            $data['photo'] = $path;
        } else {
            $data['photo'] = $category->photo;
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($video_kids)
    {

        File::delete(public_path('images/library_categories/' . $video_kids->video));
        File::delete(public_path('images/library_categories/' . $video_kids->audio));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $hadith = LibraryCategory::find($id);
            $this->deleteoldPhoto($hadith->photo, 'library_categories');
        }

    }//end of deleteItems function
}
