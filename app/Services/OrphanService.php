<?php

namespace App\Services;

use App\Models\Orphan;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class OrphanService
{
    use upload;

    public function handleUploads($request, $id = null)
    {
        $data = $request->except('_token', '_method');
        $orphan = '';
        if ($id) {
            $orphan = Orphan::find($id);
        }
        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/orphans/' . $orphan->photo)) : '';
            $path = $this->upload($request->photo, 'orphans');
            $data['photo'] = $path;
        } elseif ($request->photo !== null) {
            $data['photo'] = $request->photo;
        } else {
            $data['photo'] = $orphan->photo;
        }

        if ($request->hasFile('father_death_application')) {
            $id ? File::delete(public_path('images/orphans/' . $orphan->father_death_application)) : '';
            $id ? File::delete(public_path('images/orphans/' . $orphan->mother_death_application)) : '';
            $path = $this->upload($request->father_death_application, 'orphans');
            $data['father_death_application'] = $path;
        } elseif ($request->father_death_application !== null && $id != null) {
            $data['father_death_application'] = $request->father_death_application;
        }

        if ($request->hasFile('mother_death_application')) {

            $id ? File::delete(public_path('images/orphans/' . $orphan->mother_death_application)) : '';
            $id ? File::delete(public_path('images/orphans/' . $orphan->father_death_application)) : '';
            $path = $this->upload($request->mother_death_application, 'orphans');
            $data['mother_death_application'] = $path;
        } elseif ($request->mother_death_application !== null && $id != null) {
            $data['mother_death_application'] = $request->mother_death_application;
        }

        if ($request->hasFile('mother_photo')) {
            $id ? File::delete(public_path('images/orphans/' . $orphan->mother_photo)) : '';
            $id ? File::delete(public_path('images/orphans/' . $orphan->father_photo)) : '';
            $path = $this->upload($request->mother_photo, 'orphans');
            $data['mother_photo'] = $path;
        } elseif ($request->mother_photo !== null && $id != null) {
            $data['mother_photo'] = $request->mother_photo;
        }
        if ($request->hasFile('father_photo')) {
            $id ? File::delete(public_path('images/orphans/' . $orphan->father_photo)) : '';
            $id ? File::delete(public_path('images/orphans/' . $orphan->mother_photo)) : '';
            $path = $this->upload($request->father_photo, 'orphans');
            $data['father_photo'] = $path;
        } elseif ($request->mother_photo !== null && $id != null) {
            $data['father_photo'] = $request->father_photo;
        }

        if ($request->lost == 'father') {
            $data['mother_death_application'] = null;
            $data['mother_death_date'] = null;
            $data['mother_death_reason'] = null;
            $data['mother_death_place'] = null;
            $data['father_photo'] = null;
            $data['father_job'] = null;
            $data['father_salary'] = null;
            $data['father_education_level'] = null;

        } elseif ($request->lost == 'mother') {
            $data['father_death_application'] = null;
            $data['father_death_date'] = null;
            $data['father_death_reason'] = null;
            $data['father_death_place'] = null;
            $data['mother_photo'] = null;
            $data['mother_job'] = null;
            $data['mother_salary'] = null;
            $data['mother_education_level'] = null;

        } else {
            $data['father_photo'] = null;
            $data['father_job'] = null;
            $data['father_salary'] = null;
            $data['mother_photo'] = null;
            $data['mother_job'] = null;
            $data['mother_salary'] = null;
            $data['father_education_level'] = null;
            $data['mother_education_level'] = null;
        }
        if (!$request->has_disease) {
            $data['disease_type'] = null;
            $data['disease_cost'] = null;
        }

        return $data;
    }//end of  function

    public function deleteFiles($orphan)
    {

        File::delete(public_path('images/orphans/' . $orphan->photo));
        File::delete(public_path('images/orphans/' . $orphan->mother_death_application));
        File::delete(public_path('images/orphans/' . $orphan->father_death_application));
        File::delete(public_path('images/orphans/' . $orphan->father_photo));
        File::delete(public_path('images/orphans/' . $orphan->mother_photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {

        foreach ($items as $id) {
            $orphan = Orphan::find($id);
            $this->deleteFiles($orphan);
        }

    }//end of deleteItems function
}
