<?php

namespace App\Services;

use App\Models\VideoKid;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class VideoKidService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $video = '';
        if ($id) {
            $video = VideoKid::find($id);
        }
        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/video_kids/videos/' . $video->video)) : '';
            $path = $this->uploadVideo($request->video, 'video_kids/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/video_kids/videos/' . $video->video)) : '';
            $data['video'] = $request->video;
        } else {
            $data['video'] = $video->video;
        }

        if ($request->hasFile('photo')) {

            $id ? File::delete(public_path('images/video_kids/' . $video->photo)) : '';
            $path = $this->upload($request->photo, 'video_kids');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $video->photo : '';
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($video_kids)
    {

        File::delete(public_path('uploads/video_kids/videos/' . $video_kids->video));
        File::delete(public_path('uploads/video_kids/audios/' . $video_kids->audio));
        File::delete(public_path('images/video_kids/' . $video_kids->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $video = VideoKid::find($id);
            $this->deleteFiles($video);
        }

    }//end of deleteItems function
}
