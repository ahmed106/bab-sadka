<?php

namespace App\Services;

use App\Models\Hesn;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class HesnService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $hesn = '';
        if ($id) {
            $hesn = Hesn::find($id);
        }
        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/hesn/videos/' . $hesn->video)) : '';
            $path = $this->uploadVideo($request->video, 'hesn/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/hesn/videos/' . $hesn->video)) : '';
            $data['video'] = $request->video;
        } else {
            $data['video'] = $hesn->video;
        }

        if ($request->hasFile('audio')) {
            $id ? File::delete(public_path('uploads/hesn/audios/' . $hesn->audio)) : '';
            $path = $this->uploadVideo($request->audio, 'hesn/audios');
            $data['audio'] = $path;
        } else {
            $id ? $data['audio'] = $hesn->audio : '';
        }

        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/hesns/' . $hesn->photo)) : '';
            $path = $this->upload($request->photo, 'hesns');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $hesn->photo : '';
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($hesn)
    {

        File::delete(public_path('uploads/hesn/videos/' . $hesn->video));
        File::delete(public_path('uploads/hesn/audios/' . $hesn->audio));
        File::delete(public_path('images/hesns/' . $hesn->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $hesn = Hesn::find($id);
            $this->deleteFiles($hesn);
        }

    }//end of deleteItems function
}
