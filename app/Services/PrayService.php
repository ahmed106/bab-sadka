<?php

namespace App\Services;

use App\Models\Pray;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class PrayService
{
    use upload;

    public function handleUploads($request, $id = null)
    {

        $data = $request->except('_token', '_method');
        $pray = '';
        if ($id) {
            $pray = Pray::find($id);
        }
        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/prays/videos/' . $pray->video)) : '';
            $path = $this->uploadVideo($request->video, 'prays/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/prays/videos/' . $pray->video)) : '';
            $data['video'] = $request->video;
        } else {
            $data['video'] = $pray->video;
        }

        if ($request->hasFile('audio')) {
            $id ? File::delete(public_path('uploads/prays/audios/' . $pray->audio)) : '';
            $path = $this->uploadVideo($request->audio, 'prays/audios');
            $data['audio'] = $path;
        } else {
            $id ? $data['audio'] = $pray->audio : '';
        }

        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/prays/' . $pray->photo)) : '';
            $path = $this->upload($request->photo, 'prays');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $pray->photo : '';
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($pray)
    {

        File::delete(public_path('uploads/prays/videos/' . $pray->video));
        File::delete(public_path('uploads/prays/audios/' . $pray->audio));
        File::delete(public_path('images/prays/' . $pray->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $pray = Pray::find($id);
            $this->deleteFiles($pray);
        }

    }//end of deleteItems function
}
