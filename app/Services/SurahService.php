<?php

namespace App\Services;

use App\Models\Surah;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class SurahService
{
    use upload;

    public function handleUploads($request, $id = null)
    {
        $data = $request->except('_token', '_method');
        $surah = '';
        if ($id) {
            $surah = Surah::find($id);
        }


        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/surahs/videos/' . $surah->video)) : '';
            $path = $this->uploadVideo($request->video, 'surahs/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/surahs/videos/' . $surah->video)) : '';
            $data['video'] = $request->video;
        } else {
            $data['video'] = $surah ? $surah->video : '';
        }

        if ($request->hasFile('audio')) {
            $id ? File::delete(public_path('uploads/surahs/audios/' . $surah->audio)) : '';
            $path = $this->uploadVideo($request->audio, 'surahs/audios');
            $data['audio'] = $path;
        } else {
            $id ? $data['audio'] = $surah->audio : '';
        }

        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/surahs/' . $surah->photo)) : '';
            $path = $this->upload($request->photo, 'surahs');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $surah->photo : '';
        }


        return $data;
    }//end of  function

    public function deleteFiles($surah)
    {

        File::delete(public_path('uploads/surahs/videos/' . $surah->video));
        File::delete(public_path('uploads/surahs/audios/' . $surah->audio));
        File::delete(public_path('images/surahs/' . $surah->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {

        foreach ($items as $id) {
            $surah = Surah::find($id);
            $this->deleteFiles($surah);
        }

    }//end of deleteItems function
}
