<?php

namespace App\Services;

use App\Models\NawawyForty;
use App\Traits\upload;
use Illuminate\Support\Facades\File;

class NawawyService
{
    use upload;

    public function handleUploads($request, $id = null)
    {


        $data = $request->except('_token', '_method');
        $nawawy = '';
        if ($id) {
            $nawawy = NawawyForty::find($id);
        }

        if ($request->hasFile('video')) {
            $id ? File::delete(public_path('uploads/nawawy_forty/videos/' . $nawawy->video)) : '';
            $path = $this->uploadVideo($request->video, 'nawawy_forty/videos');
            $data['video'] = $path;
        } elseif ($request->video !== null) {
            $id ? File::delete(public_path('uploads/nawawy_forty/videos/' . $nawawy->video)) : '';
            $data['video'] = $request->video;
        } else {
            $id ? $data['video'] = $nawawy->video : '';
        }


        if ($request->hasFile('audio')) {
            $id ? File::delete(public_path('uploads/nawawy_forty/audios/' . $nawawy->audio)) : '';
            $path = $this->uploadVideo($request->audio, 'nawawy_forty/audios');

            $data['audio'] = $path;
        } else {
            $id ? $data['audio'] = $nawawy->audio : '';
        }

        if ($request->hasFile('photo')) {
            $id ? File::delete(public_path('images/nawawy_forties/' . $nawawy->photo)) : '';
            $path = $this->upload($request->photo, 'nawawy_forties');
            $data['photo'] = $path;
        } else {
            $id ? $data['photo'] = $nawawy->photo : '';
        }


        unset($data['id']);
        return $data;
    }//end of  function

    public function deleteFiles($nawawy)
    {

        File::delete(public_path('uploads/nawawy_forty/videos/' . $nawawy->video));
        File::delete(public_path('uploads/nawawy_forty/audios/' . $nawawy->audio));
        File::delete(public_path('images/nawawy_forties/' . $nawawy->photo));

    }//end of deleteFiles function

    public function deleteItems($items)
    {
        foreach ($items as $id) {
            $nawawy = NawawyForty::find($id);
            $this->deleteFiles($nawawy);
        }

    }//end of deleteItems function
}
