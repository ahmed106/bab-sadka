<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;

trait upload
{
    public function upload($image, $file)
    {
        $image_name = random_int(1000, 10000) . '_' . $image->getClientOriginalName();
        $path = public_path('images/' . $file . '/' . $image_name);

        if (!file_exists(public_path('images/' . $file))) {
            mkdir(public_path('images/' . $file), 0777, true);
        }//end if statement


        $image->move(public_path('images/' . $file), $image_name);
//        $image = Image::make($image);
//
//        $image->save($path);

        return $image_name;

    }//end of upload function

    public function deleteoldPhoto($image, $file)
    {
        File::delete(public_path('images/' . $file . '/' . $image));
    }//end of deleteoldPhot function

    public function deleteoldPdf($image, $file)
    {
        File::delete(public_path('uploads/' . $file . '/' . $image));


    }//end of deleteoldPhot function

    public function uploadVideo($video, $file)
    {

        $video_name = random_int(1000, 10000) . '_' . $video->getClientOriginalName();


        if (!file_exists(public_path('uploads/' . $file))) {
            mkdir(public_path('uploads/' . $file), 077, true);
        }//end if statement

        $video->move(public_path('uploads/' . $file), $video_name);

        return $video_name;

    }//end of uploadVideo function

    public function uploadPdf($pdf, $file)
    {
        $pdf_name = random_int(1000, 10000) . '_' . $pdf->getClientOriginalName();


        if (!file_exists(public_path('uploads/' . $file))) {
            mkdir(public_path('uploads/' . $file), 077, true);
        }//end if statement

        $pdf->move(public_path('uploads/' . $file), $pdf_name);
        return $pdf_name;
    }//end of uploadPdf function
}
