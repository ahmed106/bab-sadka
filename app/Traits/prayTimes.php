<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

trait prayTimes
{

    public function prayTimes($country = null, $city = null)
    {
        $country = $country ?? 'Egypt';
        $city = $city ?? 'Cairo';
        $apiURL = 'http://api.aladhan.com/v1/timingsByCity?city=' . $city . '&country=' . $country . '';
        $response = Http::get($apiURL);
        $responseBody = json_decode($response->getBody(), true);

        $data['fajr'] = Carbon::create($responseBody['data']['timings']['Fajr'])->toTimeString();
        $data['dhuhr'] = Carbon::create($responseBody['data']['timings']['Dhuhr'])->toTimeString();
        $data['asr'] = Carbon::create($responseBody['data']['timings']['Asr'])->toTimeString();
        $data['maghrab'] = Carbon::create($responseBody['data']['timings']['Maghrib'])->toTimeString();
        $data['isha'] = Carbon::create($responseBody['data']['timings']['Isha'])->toTimeString();

        return $data;
    }//end of prayTimes function
}
