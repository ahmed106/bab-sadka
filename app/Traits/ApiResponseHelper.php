<?php

namespace App\Traits;

trait ApiResponseHelper
{
    protected $body;

    public function setCode($code)
    {
        $this->body['code'] = $code;

        return $this;


    }//end of setCode function

    public function setDate($data)
    {
        $this->body['data'] = $data;
        return $this;

    }//end of setDate function

    public function send()
    {
        return response()->json($this->body, 200);
    }//end of send function

    public function setError($error)
    {
        $this->body['error'] = $error;
        return $this;
    }//end of setError function

    public function setSuccess($message)
    {
        $this->body['message'] = $message;
        return $this;

    }//end of setSuccess function

    public function setStatus($status)
    {
        $this->body['status'] = $status;
        return $this;
    }//end of setStatus function

    public function error($error)
    {

        $this->body['error'] = $error;
        $this->body['status'] = 'error';

        return response()->json($this->body, 402);

    }//end of error function


}

