<?php

use Illuminate\Support\Facades\Route;

// routes for guest middleware  and guard admin
Route::group(['prefix' => 'dashboard', 'middleware' => 'guest:admin'], function () {
    Route::get('/login', 'Auth\AuthController@login')->name('admin.login');
    Route::post('login', 'Auth\AuthController@doLogin')->name('admin.doLogin');
    Route::get('reloadCaptcha', 'Auth\AuthController@reloadCaptcha')->name('reloadCaptcha');
});

// routes for auth middleware  and guard admin

Route::group(
    ['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'auth:admin']], function () {

    Route::group(['prefix' => 'dashboard'], function () {

        Route::get('/', 'DashboardController@index')->name('dashboard');
        Route::get('/logout', 'Auth\AuthController@logout')->name('admin.logout');
        Route::get('/praytime', 'DashboardController@prayTime')->name('admin.home');


        /*roles routes*/
        Route::get('/roles/data', 'RoleController@data')->name('roles.data');
        Route::post('roles/bulk-delete', 'RoleController@bulkDelete')->name('roles.bulkDelete');
        Route::resource('roles', 'RoleController')->except('show');
        /*end roles routes*/


        /* languages routes*/
        Route::get('/languages/data', 'LanguageController@data')->name('languages.data');
        Route::post('languages/bulk-delete', 'LanguageController@bulkDelete')->name('languages.bulkDelete');
        Route::post('languages/active', 'LanguageController@active')->name('languages.active');
        Route::resource('languages', 'LanguageController')->except('show');
        /* end languages routes */


        /* admins routes*/
        Route::get('/admins/data', 'AdminController@data')->name('admins.data');
        Route::post('admins/bulk-delete', 'AdminController@bulkDelete')->name('admins.bulkDelete');
        Route::resource('admins', 'AdminController')->except('show');
        /* end admins routes */

        /*quran readers*/
        Route::get('/quran-readers/data', 'QuranReaderController@data')->name('quran-readers.data');
        Route::post('quran-readers/bulk-delete', 'QuranReaderController@bulkDelete')->name('quran-readers.bulkDelete');
        Route::resource('quran-readers', 'QuranReaderController');
        /*end quran readers*/

        /*surah*/
        Route::get('surahs/data', 'SurahController@data')->name('surahs.data');
        Route::get('surahs/youtube-video', 'SurahController@getYoutubeVideo')->name('surahs.getVideo');
        Route::post('surahs/bulk-delete', 'SurahController@bulkDelete')->name('surahs.bulkDelete');
        Route::resource('surahs', 'SurahController');
        /*end surah*/

        /*hadith*/
        Route::get('hadiths/data', 'HadithController@data')->name('hadiths.data');
        Route::post('hadiths/bulk-delete', 'HadithController@bulkDelete')->name('hadiths.bulkDelete');
        Route::resource('hadiths', 'HadithController');
        /*end hadith*/

        /*prophet stories*/
        Route::get('prophet-stories/data', 'ProphetStoryController@data')->name('prophet-stories.data');
        Route::post('prophet-stories/bulk-delete', 'ProphetStoryController@bulkDelete')->name('prophet-stories.bulkDelete');
        Route::get('prophet-stories/delete/{id}', 'ProphetStoryController@delete')->name('prophet-stories.delete');
        Route::resource('prophet-stories', 'ProphetStoryController');
        /*prophet stories*/

        /*Hesn El muslim*/
        Route::get('hesn/data', 'HesnController@data')->name('hesn.data');
        Route::post('hesn/bulk-delete', 'HesnController@bulkDelete')->name('hesn.bulkDelete');
        Route::resource('hesn', 'HesnController');
        /*end  Hesn El muslim*/

        /* An Nawawy Forty*/
        Route::get('nawawy-forty/data', 'NawawyFortyController@data')->name('nawawy-forty.data');
        Route::post('nawawy-forty/bulk-delete', 'NawawyFortyController@bulkDelete')->name('nawawy-forty.bulkDelete');
        Route::get('nawawy-forty/delete/{id}', 'NawawyFortyController@delete')->name('nawawy-forty.delete');
        Route::resource('nawawy-forty', 'NawawyFortyController');
        /*An Nawawy Forty*/

        /* Video Kids*/
        Route::get('video-kids/data', 'VideoKidController@data')->name('video-kids.data');
        Route::post('video-kids/bulk-delete', 'VideoKidController@bulkDelete')->name('video-kids.bulkDelete');
        Route::resource('video-kids', 'VideoKidController');
        /*Video Kids*/


        /*libraries*/

        // library categories
        Route::get('library-categories/data', 'LibraryCategoryController@data')->name('library-categories.data');
        Route::post('library-categories/bulk-delete', 'LibraryCategoryController@bulkDelete')->name('library-categories.bulkDelete');
        Route::resource('library-categories', 'LibraryCategoryController');
        // end library categories

        // libraries
        Route::get('libraries/data', 'LibraryController@data')->name('libraries.data');
        Route::post('libraries/bulk-delete', 'LibraryController@bulkDelete')->name('libraries.bulkDelete');
        Route::get('/libraries/view-pdf/{id}', 'LibraryController@viewPdf')->name('libraries.viewPdf');
        Route::resource('libraries', 'LibraryController');
        // end libraries

        /*end libraries*/


        /* pray*/
        Route::get('prays/data', 'PrayController@data')->name('prays.data');
        Route::post('prays/bulk-delete', 'PrayController@bulkDelete')->name('prays.bulkDelete');
        Route::get('prays/delete/{id}', 'PrayController@delete')->name('prays.delete');
        Route::resource('prays', 'PrayController');
        /*end pray*/


        /* Orphan*/
        Route::get('orphans/data', 'OrphanController@data')->name('orphans.data');
        Route::post('orphans/bulk-delete', 'OrphanController@bulkDelete')->name('orphans.bulkDelete');
        Route::get('orphans/delete/{id}', 'OrphanController@delete')->name('orphans.delete');
        Route::get('orphans/get-city', 'OrphanController@getCountryCities')->name('getCountryCities');
        Route::post('orphans/approval', 'OrphanController@approval')->name('orphans.approval');
        Route::resource('orphans', 'OrphanController');
        /*end Orphan*/


        /* countries*/
        Route::get('countries/data', 'CountryController@data')->name('countries.data');
        Route::post('countries/bulk-delete', 'CountryController@bulkDelete')->name('countries.bulkDelete');
        Route::get('countries/delete/{id}', 'CountryController@delete')->name('countries.delete');
        Route::resource('countries', 'CountryController');
        /*end countries*/


        /* cities*/
        Route::get('cities/data', 'CityController@data')->name('cities.data');
        Route::post('cities/bulk-delete', 'CityController@bulkDelete')->name('cities.bulkDelete');
        Route::get('cities/delete/{id}', 'CityController@delete')->name('cities.delete');
        Route::resource('cities', 'CityController');
        /*end cities*/

        /* contact us*/
        Route::get('contact-us/data', 'ContactUsController@data')->name('contact-us.data');
        Route::post('contact-us/bulk-delete', 'ContactUsController@bulkDelete')->name('contact-us.bulkDelete');
        Route::get('contact-us/delete/{id}', 'ContactUsController@delete')->name('contact-us.delete');
        Route::resource('contact-us', 'ContactUsController');
        /*end contact us*/


        /* contact us*/
        Route::get('dead/data', 'DeadController@data')->name('dead.data');
        Route::post('dead/bulk-delete', 'DeadController@bulkDelete')->name('dead.bulkDelete');
        Route::get('dead/delete/{id}', 'DeadController@delete')->name('dead.delete');
        Route::post('dead/approval', 'DeadController@approval')->name('dead.approval');
        Route::resource('dead', 'DeadController');
        /*end contact us*/

        /*settings*/
        Route::resource('settings', 'SettingController');
        /*end settings*/
    });
});


