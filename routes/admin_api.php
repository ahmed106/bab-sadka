<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'guest:admin-api'], function () {

    Route::post('login', 'Auth\AuthenticationController@login');
});

Route::group(['middleware' => 'auth:admin-api'], function () {

    Route::post('logout', 'Auth\AuthenticationController@logout');

    Route::post('quran-readers', 'QuranReaderController@store');
    Route::put('quran-readers/{id}', 'QuranReaderController@update');
    Route::delete('quran-readers/{id}', 'QuranReaderController@destroy');

});

