<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create asidsomething great!
|
*/


Route::get('/', 'FrontController@index');
//Route::view('/website', 'website.index2');
//  Quran //
Route::get('/quran', 'QuranController@index')->name('website.quran.index');
Route::get('/quran/read', 'QuranController@read');
Route::get('/quran/listen/{reader_id?}/', 'QuranController@listen')->name('readerSearch');
Route::get('quran/get-surah', 'QuranController@getSurah')->name('quran.getSurah');
Route::get('quran/get-video', 'QuranController@getVideos')->name('quran.getVideos');
Route::get('/quran/videos', 'QuranController@videos')->name('quran.videos');
Route::get('/quran-videos/{id}', 'QuranController@show');
// end Quran

// hadith
Route::get('/hadith', 'HadithController@index');
Route::get('/hadith/listen', 'HadithController@listen')->name('hadith.listen');
Route::get('/hadith/videos', 'HadithController@videos')->name('hadith.videos');
Route::get('/hadith/getAudio', 'HadithController@getAudio')->name('hadith.getAudio');
Route::get('/hadith-videos/{id}', 'HadithController@show');
//end hadith

// prophet's stories
Route::get('/prophets-stories', 'ProphetController@index');
Route::get('/prophets-stories/read', 'ProphetController@read')->name('prophet.read');
Route::get('/prophets-stories/listen', 'ProphetController@listen')->name('prophet.listen');
Route::get('/prophets-stories/videos', 'ProphetController@videos')->name('prophet.videos');
Route::get('/prophets-stories-videos/{id}', 'ProphetController@show');
// end prophet's story

// Hesn el-muslim
Route::get('/hesn-elmuslim', 'HesnController@index');
Route::get('/hesn-elmuslim/listen', 'HesnController@listen');
Route::get('/hesn-elmuslim/videos', 'HesnController@videos');
Route::get('/hesn-elmuslim-videos/{id}', 'HesnController@show');
// end Hesn el-muslim


// Arbaon Nwawia
Route::get('arboon-nwawia', 'NwawiaController@index');
Route::get('/arboon-nwawia/read', 'NwawiaController@read');
Route::get('/arboon-nwawia/listen', 'NwawiaController@listen');
Route::get('/arboon-nwawia/videos', 'NwawiaController@videos');
Route::get('/arboon-nwawia-videos/{id}', 'NwawiaController@show');

// end Arbaon Nwawia

// kids videos
Route::get('/kid-videos', 'KidvideoController@index');
Route::get('/kid-videos/{id}', 'KidvideoController@show');
// end kids videos

// masbaha
Route::get('/masbaha', 'MasbahaController@index');
// end masbaha

// libraries
Route::get('/libraries', 'LibraryController@index');
Route::get('/libraries/{id}', 'LibraryController@showCategory')->name('libraries.showCategory');
Route::get('/libraries/book/{id}', 'LibraryController@readBook')->name('libraries.readBook');
Route::get('/book-search/', 'LibraryController@bookSearch')->name('libraries.books_search');
//end libraries

// pray for death
Route::get('/pray', 'PrayController@index');
Route::get('pray/read', 'PrayController@read');
Route::get('pray/listen', 'PrayController@listen');
Route::get('pray/videos', 'PrayController@videos');
Route::get('/pray-videos/{id}', 'PrayController@show');

// end pray for death

// add orphan
Route::get('/add-orphan', 'OrphanController@create');
Route::post('add-orphan', 'OrphanController@store')->name('website.storeOrphan');
//  add orphan

//settings
Route::get('/settings', 'SettingsController@edit');
Route::post('/settings/', 'SettingsController@store');

//end settings

// add dead
Route::get('/add-dead', 'DeadController@create');
Route::post('/add-dead', 'DeadController@store');
Route::get('/D/{id}', 'DeadController@show')->name('dead.show');
// end add dead

// share app

Route::view('share', 'website.pages.share');


// get city
Route::get('/get-city', 'FrontController@getCity')->name('website.getCity');
Route::get('/get-dead', 'FrontController@getDeads')->name('website.getDeads');
//end city

// get pray times
Route::get('getTimeZone', 'FrontController@getPrayTimes')->name('website.getPrayTimes');

// end pray times
//Route::get('/fire_event', function () {
//
//    event(new \App\Events\PrayNotification('welcome'));
//})->name('fire_event');

Route::view('/contact-us', 'website.pages.contact');
Route::post('contact-us', 'FrontController@contact')->name('website.contact');

Route::view('/about-us', 'website.pages.about');

Route::view('test-player', 'website.test_player');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

